/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#include <glib.h>

G_BEGIN_DECLS

/* What features does the plugin support */
enum {
	PLUGIN_READ_ONLY = 1,
	PLUGIN_READ_WRITE = 2,
	PLUGIN_ADVANCED_SEARCH = 4
};

#define IS_READ_ONLY(X) ((X & PLUGIN_READ_ONLY) == PLUGIN_READ_ONLY)
#define IS_READ_WRITE(X) ((X & PLUGIN_READ_WRITE) == PLUGIN_READ_WRITE)
#define HAS_ADV_SEARCH(X) ((X & PLUGIN_ADVANCED_SEARCH) == PLUGIN_ADVANCED_SEARCH)

typedef enum {
	PLUGIN_CONFIG_EXTRA_ENTRY,
	PLUGIN_CONFIG_EXTRA_CHECKBOX,
	PLUGIN_CONFIG_EXTRA_SPINBUTTON
} ExtraConfigType;

typedef struct {
	ExtraConfigType	type;
	gchar*			label;
	gchar*			tooltip;
	union {
		gchar*		entry;
		gboolean	check_btn;
		gint		spin_btn;
	} current_value;
	union {
		gchar*		entry;
		gboolean	check_btn;
		gint		spin_btn;
	} preset_value;
} ExtraConfig;

typedef enum {
	/*
	 * Address books configured with this plugin only requires
	 * the following input to function
	 * Name: Address book name (Name must be uniq within this plugin)
	 * (URI|URL): Reference to storage (URI => File in local file system,
	 * URL => TCP based reference). If 'plugin_file_filter' returns NULL
	 * then this plugin uses URL type storage reference.
	 * 
	 * Optional
	 * - Username
	 * - Password
	 */
	PLUGIN_TYPE_SIMPLE,
	/*
	 * Address books configured with this plugin requires additional
	 * input to function in which case the function 'plugin_extra_config'
	 * MUST return a GSList of ExtraConfig
	 */
	PLUGIN_TYPE_ADVANCED
} PluginType;

typedef struct {
	guint			support; /* One or more of the plugin features or'ed */
	const gchar*	subtype;
} PluginFeature;

typedef enum {
	ATTRIB_TYPE_INT,
	ATTRIB_TYPE_BOOLEAN,
	ATTRIB_TYPE_CHAR,
	ATTRIB_TYPE_STRING
} AttribType;

typedef union {
		gint		number;
		gboolean	boolean;
		gchar		character;
		gchar*		string;
} Value;

typedef struct {
	AttribType	type;
	gchar*		attrib_name;
	Value 		value;
} AttribDef;

typedef struct {
	gchar*	alias;
	gchar*	email;
	gchar*	remarks;
} Email;

typedef struct {
	GHashTable*	data; /* Other contact data except emails is expected to be in a GHashTable */
	GSList*		emails; /* emails is expected te be a list of Email */
} Contact;

typedef struct {
	gchar*		abook_name;
	gchar*		URL;
	gchar*		username;
	gchar*		password;
	GList*		contacts; /* contacts is expected to be a list of Contact */
	gboolean	dirty;
	gulong		next_uid;
	gboolean	open;
	GSList*		extra_config; /* List of ExtraConfig */
} AddressBook;

typedef struct {
	gchar*	group;
	GSList*	books;
} ConfiguredBooks;

typedef struct {
	gchar*	group;
	GSList*	books;
} ClosedBooks;

typedef struct {
	gchar*				comment;
	gchar*				path;
	ConfiguredBooks*	configured_books;
	ClosedBooks*		closed_books;
	GKeyFile*			key_file;
} ConfigFile;

/**
 * Starndard attributes which any plugin must support
 * cn, first-name, last-name, nick-name, email
 * If plugin supports saving a picture of the contact
 * the attribute must be named: image
 */
extern const gchar *standard_attribs[];


/* Functions which must be implemented by any plugin */
gboolean plugin_init(gpointer self, gchar** error);
void plugin_reset(gchar** error);
gboolean plugin_done(void);
const PluginFeature* plugin_provides(void);
const gchar* plugin_name(void);
const gchar* plugin_desc(void);
const gchar* plugin_version(void);
PluginType plugin_type(void);
const gchar* plugin_file_filter(void);
const gchar* plugin_license(void);
gboolean plugin_need_credentials(void);
gchar* plugin_default_url(const gchar* name);
GSList* plugin_extra_config(void);

/* 
 * Returning NULL means list of supported attributes are infinite
 * Returning an empty list means that no more supported attributes
 * are available for activation
 */
GSList* plugin_remaining_attribs(void);
GSList* plugin_inactive_attribs(void);

gboolean plugin_abook_open(AddressBook* abook, gchar** error);
gboolean plugin_abook_close(AddressBook* abook, gchar** error);
gboolean plugin_abook_delete(AddressBook* abook, gchar** error);
/* If new is not null an update of an existing config will be done */
gboolean plugin_abook_set_config(AddressBook* old, AddressBook* new, gchar** error);

/* Return a list af AttribDef's attrib_name*/
GSList* plugin_attrib_list(void);
void plugin_attribs_set(GHashTable* attributes);

/* Return a list of open address books */
GSList* plugin_addrbook_all_get(void);
/* Return a list of closed address books */
GList* plugin_closed_books_get(void);
/* Return a list of all address book names */
GSList* plugin_addrbook_names_all(void);

/* Commit all changes */
gboolean plugin_commit_all(gchar** error);

/* If plugin is read-only these functions must be implemented by plugin */
GSList* plugin_get_contact(
			AddressBook* abook, const gchar* search_token, gchar** error);

/* If plugin is read-write these functions must be implemented additionaly */
gboolean plugin_set_contact(
			AddressBook* abook, const Contact* contact, gchar** error);
gboolean plugin_delete_contact(
			AddressBook* abook, const Contact* contact, gchar** error);
gboolean plugin_update_contact(
			AddressBook* abook, const Contact* contact, gchar** error);

/* If plugin supports advanced search these functions must be implemented additionaly */
GSList* plugin_search_contact(AddressBook* abook,
							  const Contact* search_tokens,
							  gboolean is_and,
							  gchar** error);

G_END_DECLS

#endif
