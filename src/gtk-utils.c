/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "gtk-utils.h"
#include "utils.h"
#include "plugin-loader.h"
#include "plugin.h"
#include "mainwindow.h"

typedef struct {
	GHashTable*	hash_table;
	gchar*		error;
} CallbackData;

typedef struct {
	GtkWidget*	parent;
	GtkWidget*	vbox;
	GSList*		remaining_attribs;
} ButtonCB;


static void callback_data_free(CallbackData* data) {
	hash_table_free(&data->hash_table);
	g_free(data->error);
	g_free(data);
}

static void collect_attribs_cb(GtkWidget* widget, gpointer data) {
	CallbackData* callback_data = (CallbackData *) data;
	GtkWidget* btn;
	const gchar* label;
	gboolean use;
	AttribDef* attr;
	
	btn = gtk_bin_get_child(GTK_BIN(widget));
	if (btn) {
		use = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(btn));
		if (use) {
			label = gtk_button_get_label(GTK_BUTTON(btn));
			attr = g_new0(AttribDef, 1);
			attr->attrib_name = g_strdup(label);
			attr->type = ATTRIB_TYPE_STRING;
			g_hash_table_replace(
				callback_data->hash_table, g_strdup(label), attr);
		}
	}
}

void show_message(GtkWidget* parent, MessageType type, const gchar* format, ...) {
	va_list args;
	gchar buf[BUFSIZ];
	GtkMessageType msg_type = 0;
	GtkWidget* dialog;
	
	switch (type) {
		case GTK_UTIL_MESSAGE_INFO: msg_type = GTK_MESSAGE_INFO; break;
		case GTK_UTIL_MESSAGE_WARNING: msg_type = GTK_MESSAGE_WARNING; break;
		case GTK_UTIL_MESSAGE_ERROR: msg_type = GTK_MESSAGE_ERROR; break;
	}
	
	va_start(args, format);
	g_vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	debug_print("%s\n", buf);
	dialog = gtk_message_dialog_new(
		GTK_WINDOW(parent), 0, msg_type, GTK_BUTTONS_OK, buf, NULL);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

gboolean show_question(GtkWidget* parent, const gchar* format, ...) {
	va_list args;
	gchar buf[BUFSIZ];
	GtkWidget* dialog;
	gboolean reply;
	
	va_start(args, format);
	g_vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	debug_print("%s\n", buf);
	dialog = gtk_message_dialog_new(
		GTK_WINDOW(parent), 0, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, buf, NULL);
	gint r = gtk_dialog_run(GTK_DIALOG(dialog));
	switch (r) {
		case GTK_RESPONSE_YES:
			reply = TRUE;
			break;
		default:
			reply = FALSE;
			break;
	}
	gtk_widget_destroy(dialog);
	
	return reply;
}

gboolean show_input(GtkWidget* parent, const gchar* title, gchar** reply,
					const gchar* format, ...) {
	va_list args;
	gchar buf[BUFSIZ];
	GtkWidget *dialog, *label, *entry;
	GtkBox* vbox;
	
	if (reply == NULL)
		return FALSE;

	va_start(args, format);
	g_vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);
	
	label = gtk_label_new(buf);
	entry = gtk_entry_new();

	dialog = gtk_dialog_new_with_buttons (
								title, GTK_WINDOW(parent),
								GTK_DIALOG_MODAL,
								GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
								GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
								NULL);

	vbox = GTK_BOX(GTK_DIALOG(dialog)->vbox);
	gtk_box_pack_start(vbox, label, FALSE, TRUE, 0);
	gtk_box_pack_start(vbox, entry, TRUE, TRUE, 0);
	dialog_set_focus(GTK_DIALOG(dialog), GTK_STOCK_CANCEL);
	gtk_widget_show_all(GTK_WIDGET(vbox));
	
	gint r = gtk_dialog_run(GTK_DIALOG(dialog));
	switch (r) {
		case GTK_RESPONSE_ACCEPT:
			*reply = gtk_editable_get_chars(GTK_EDITABLE(entry), 0, -1);
			break;
		default:
			*reply = NULL;
			break;
	}
	gtk_widget_destroy(dialog);
	
	return (*reply) ? TRUE : FALSE;
}

GHashTable* get_attrib_list(GtkWidget* parent, 
							AttribContainer* attrib_lists,
							const gchar* info_text, gboolean can_delete,
							gchar** error, AddAttribCallback callback) {
	GtkWidget *header, *dialog, *window, *vbox1, *attrib_box,
			  *frame1, *frame2, *hbtnbox, *add_attrib_btn;
	GHashTable*	hash_table = NULL;
	CallbackData* callback_data;
	ButtonCB* button_cb;
	GSList* cur;
	
	if (! attrib_lists)
		return NULL;

	callback_data = g_new0(CallbackData, 1);
	callback_data->hash_table = hash_table_new();
	callback_data->error = NULL;
	
	vbox1 = gtk_vbox_new(FALSE, 2);
	
	header = gtk_label_new(info_text);
	frame1 = gtk_frame_new(NULL);
	gtk_container_add(GTK_CONTAINER(frame1), header);
	gtk_box_pack_start(GTK_BOX(vbox1), frame1, FALSE, TRUE, 0);
	
	frame2 = gtk_frame_new("Select attributes");
	window = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(frame2), window);
	gtk_scrolled_window_set_policy(
            GTK_SCROLLED_WINDOW(window),
            GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	attrib_box = gtk_vbox_new(FALSE, 2);
	gtk_scrolled_window_add_with_viewport(
			GTK_SCROLLED_WINDOW(window), attrib_box);
	
	for (cur = attrib_lists->existing_attribs; cur; cur = g_slist_next(cur)) {
		AttribDef* attr = (AttribDef *) cur->data;
		GtkWidget* check_btn = gtk_check_button_new_with_label(attr->attrib_name);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_btn), TRUE);
		gtk_widget_set_sensitive(check_btn, can_delete);
		GtkWidget* frame = gtk_frame_new(NULL);
		gtk_container_add(GTK_CONTAINER(frame), check_btn);
		gtk_box_pack_start(GTK_BOX(attrib_box), frame, FALSE, TRUE, 0);
	}
	
	for (cur = attrib_lists->inactive_attribs; cur; cur = g_slist_next(cur)) {
		AttribDef* attr = (AttribDef *) cur->data;
		GtkWidget* check_btn = gtk_check_button_new_with_label(attr->attrib_name);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_btn), FALSE);
		gtk_widget_set_sensitive(check_btn, can_delete);
		GtkWidget* frame = gtk_frame_new(NULL);
		gtk_container_add(GTK_CONTAINER(frame), check_btn);
		gtk_box_pack_start(GTK_BOX(attrib_box), frame, FALSE, TRUE, 0);
	}
	
	gtk_box_pack_start(GTK_BOX(vbox1), frame2, TRUE, TRUE, 0);

	hbtnbox = gtk_hbutton_box_new();
	gtk_button_box_set_layout(GTK_BUTTON_BOX(hbtnbox), GTK_BUTTONBOX_CENTER);
	add_attrib_btn = gtk_button_new_with_mnemonic("_Add attribute");
	gtk_box_pack_start_defaults(GTK_BOX(hbtnbox), add_attrib_btn);
	button_cb = g_new0(ButtonCB, 1);
	button_cb->parent = parent;
	button_cb->vbox = attrib_box;
	button_cb->remaining_attribs = attrib_lists->remaining_attribs;
	g_signal_connect(
			add_attrib_btn, "clicked", G_CALLBACK(callback), button_cb);
	gtk_box_pack_start(GTK_BOX(vbox1), hbtnbox, FALSE, TRUE, 0);
	
	gtk_widget_show_all(vbox1);
	
	dialog = gtk_dialog_new_with_buttons(
			"Define supported attributes", GTK_WINDOW(parent), 0,
			GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
			GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
			NULL);
	gtk_window_set_default_size(GTK_WINDOW(dialog), 300, 300);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_REJECT);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), vbox1);
	dialog_set_focus(GTK_DIALOG(dialog), GTK_STOCK_CANCEL);
	
	gint reply = gtk_dialog_run(GTK_DIALOG(dialog));
	switch (reply) {
		case GTK_RESPONSE_ACCEPT:
			gtk_container_foreach(GTK_CONTAINER(attrib_box),
								  collect_attribs_cb, callback_data);
			if (callback_data->error) {
				*error = g_strdup(callback_data->error);
			}
			else {
				hash_table = hash_table_copy(callback_data->hash_table);
			}
			break;
		default:
			for (cur = attrib_lists->existing_attribs; cur; cur = g_slist_next(cur)) {
				AttribDef* attr = (AttribDef *) cur->data;
				g_hash_table_replace(callback_data->hash_table,
					g_strdup(attr->attrib_name), attrib_def_copy(attr));
			}
			hash_table = hash_table_copy(callback_data->hash_table);
			break;
	}
	gtk_widget_destroy(dialog);
	g_free(button_cb);
	callback_data_free(callback_data);

	return hash_table;
}

void add_widget_attrib_list(GtkWidget* vbox, GtkWidget* check_btn) {
		GtkWidget* frame = gtk_frame_new(NULL);
		gtk_widget_show(frame);
		gtk_container_add(GTK_CONTAINER(frame), check_btn);
		gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 0);
		gtk_box_reorder_child(GTK_BOX(vbox), frame, 0);
}

void add_attrib_btn_cb(GtkWidget* widget, gpointer data) {
	ButtonCB* button_cb = (ButtonCB *) data;
	gchar* reply = NULL;
	GSList *cur, *choices = NULL;
	
	if (button_cb->remaining_attribs && ! button_cb->remaining_attribs->data) {
		show_message(button_cb->parent, GTK_MESSAGE_INFO,
			"Plugin does not support user defined attributes");
	}
	else if (button_cb->remaining_attribs) {
		for (cur = button_cb->remaining_attribs; cur; cur = g_slist_next(cur)) {
			AttribDef* attr = (AttribDef *) cur->data;
			choices = g_slist_prepend(choices, g_strdup(attr->attrib_name));
			if (show_choice_list(button_cb->parent, "Choice new attribute",
				choices, &reply)) {
			}
			gslist_free(&choices, g_free);
		}
		
	}
	else {
		if (show_input(
			button_cb->parent, "Define new attribute",
					&reply, "Enter name for new attribute")) {
			GtkWidget* check_btn = gtk_check_button_new_with_label(reply);
			g_free(reply);
			gtk_widget_show(check_btn);
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_btn), TRUE);
			add_widget_attrib_list(button_cb->vbox, check_btn);
		}
	}
}

GtkWidget* create_combo_box_text(GSList* items) {
	GtkWidget* combobox = NULL;
	GSList* cur;
	
#if GTK_MINOR_VERSION < 24
	combobox = gtk_combo_box_new();
	for (cur = items; cur; cur = g_slist_next(cur)) {
		gchar* name = (gchar *) cur->data;
		gtk_combo_box_append_text(
			GTK_COMBO_BOX(combobox), name);
	}
#else
	combobox = gtk_combo_box_text_new();
	for (cur = items; cur; cur = g_slist_next(cur)) {
		gchar* name = (gchar *) cur->data;
		gtk_combo_box_text_append_text(
			GTK_COMBO_BOX_TEXT(combobox), name);
	}
#endif
	
	return combobox;
}

gchar* combo_box_text_get_active(GtkWidget* combobox) {
	gchar* text = NULL;
	
	if (! GTK_IS_COMBO_BOX(combobox))
		return text;	

#if GTK_MINOR_VERSION < 24
	text = gtk_combo_box_get_active_text(GTK_COMBO_BOX(combobox));
#else
	text = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(combobox));
#endif

	return text;
}

gboolean show_choice_list(GtkWidget* parent, 
						  const gchar* title,
						  GSList* choices,
						  gchar** response) {
	gboolean result = FALSE;
	GtkWidget *combobox, *dialog;
	
	if (response == NULL)
		return result;

	combobox = create_combo_box_text(choices);
			
	gtk_combo_box_set_active(GTK_COMBO_BOX(combobox), 0);
	gtk_widget_show(combobox);

	dialog = gtk_dialog_new_with_buttons (
		title, GTK_WINDOW(parent),
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT, NULL);
	
	gtk_window_set_default_size(GTK_WINDOW(dialog), 300, 50);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_REJECT);
	dialog_set_focus(GTK_DIALOG(dialog), GTK_STOCK_CANCEL);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), combobox);
	
	gint reply = gtk_dialog_run(GTK_DIALOG(dialog));
	switch (reply) {
		case GTK_RESPONSE_ACCEPT:
			*response = combo_box_text_get_active(combobox);
			result = TRUE;
			break;
		default:
			break;
	}
	gtk_widget_destroy(dialog);
	
	return result;
}

GtkTreeIter* set_selection_combobox(GtkWidget* parent, 
									const gchar* title,
					   				GtkTreeModel* model,
					   				guint column) {
	GtkWidget *combobox, *dialog;
	GtkTreeIter* iter = NULL;
	GtkCellRenderer* cell;
	
	if (model == NULL)
		return iter;

	combobox = gtk_combo_box_new_with_model(model);

    cell = gtk_cell_renderer_text_new();
	gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combobox), cell, TRUE);
	gtk_cell_layout_set_attributes(
			GTK_CELL_LAYOUT(combobox), cell, "text", column, NULL);

	gtk_combo_box_set_active(GTK_COMBO_BOX(combobox), 0);
	gtk_widget_show(combobox);

	dialog = gtk_dialog_new_with_buttons (
		title, GTK_WINDOW(parent),
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT, NULL);
	
	gtk_window_set_default_size(GTK_WINDOW(dialog), 300, 50);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_REJECT);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), combobox);
	dialog_set_focus(GTK_DIALOG(dialog), GTK_STOCK_CANCEL);
	
	gint reply = gtk_dialog_run(GTK_DIALOG(dialog));
	switch (reply) {
		case GTK_RESPONSE_ACCEPT:
			iter = g_new0(GtkTreeIter, 1);
			gtk_combo_box_get_active_iter(GTK_COMBO_BOX(combobox), iter);
			break;
		default:
			break;
	}
	gtk_widget_destroy(dialog);

	return iter;
}

void row_data_free(gpointer row) {
	RowData* data = (RowData *) row;

	cm_return_if_fail(data != NULL);
	
	data->plugin = NULL;
	data->abook = NULL;
	g_free(data->name);
}

void dialog_set_focus(GtkDialog* dialog, const gchar* button_label) {
	GList *children, *cur;
	
	cm_return_if_fail(dialog != NULL);
	cm_return_if_fail(GTK_IS_DIALOG(dialog));
	
	children = gtk_container_get_children(
		GTK_CONTAINER(GTK_DIALOG(dialog)->action_area));
	for (cur = children; cur; cur = g_list_next(cur)) {
		GtkWidget* widget = (GtkWidget *) cur->data;
		if (strcmp(G_OBJECT_TYPE_NAME(widget), "GtkButton") == 0) {
			const gchar* label = gtk_button_get_label(GTK_BUTTON(widget));
			if (label && strcmp(label, button_label) == 0) {
				gtk_widget_grab_focus(widget);
				break;
			}
		}
	}
}

AddressBook* get_selected_address_book(GtkTreeView* view) {
    GtkTreeIter   iter;
    GtkTreeModel* model;
    GtkTreeSelection* row;
	GValue value={0,};
	AddressBook* abook = NULL;

	cm_return_val_if_fail(view != NULL, NULL);
	
	row = gtk_tree_view_get_selection(view);
	if (row) {
		if (gtk_tree_selection_get_selected(row, &model, &iter)) {
			gtk_tree_model_get_value(model, &iter, BOOK_DATA_COLUMN, &value);
			abook = (AddressBook *) g_value_get_pointer(&value);
			g_value_unset(&value);
		}
	}
	
	return abook;
}

void set_selected_address_book(GtkTreeView* view, AddressBook* book) {
	if (! view || ! book)
		return;	
}

Plugin* get_selected_plugin(GtkTreeView* view) {
    GtkTreeIter   iter;
    GtkTreeModel* model;
    GtkTreeSelection* row;
	GValue value={0,};
	Plugin* plugin = NULL;

	cm_return_val_if_fail(view != NULL, NULL);
	
	row = gtk_tree_view_get_selection(view);
	if (row) {
		if (gtk_tree_selection_get_selected(row, &model, &iter)) {
			gtk_tree_model_get_value(model, &iter, BOOK_PLUGIN_COLUMN, &value);
			plugin = (Plugin *) g_value_get_pointer(&value);
			g_value_unset(&value);
		}
	}
	
	return plugin;
}

Contact* get_selected_contact(GtkTreeView* view) {
    GtkTreeIter   iter;
    GtkTreeModel* model;
    GtkTreeSelection* row;
	GValue value={0,};
	Contact* contact = NULL;

	cm_return_val_if_fail(view != NULL, NULL);

	row = gtk_tree_view_get_selection(view);
	if (row) {
		if (gtk_tree_selection_get_selected(row, &model, &iter)) {
			gtk_tree_model_get_value(model, &iter, CONTACT_DATA_COLUMN, &value);
			contact = (Contact *) g_value_get_pointer(&value);
			g_value_unset(&value);
		}
	}
	
	return contact;
}

void list_view_clear(GtkTreeView* view, MainWindow* mainwindow) {
    GtkListStore* store;
    GtkTreeModel* model;

	if (! view)
		return;
		
    model = gtk_tree_view_get_model(view);
    store = GTK_LIST_STORE(model);
	
   	gtk_list_store_clear(store);
	if (gtk_tree_model_get_n_columns(model) == CONTACT_N_COLUMNS &&
			mainwindow->compose_mode) {
		gtk_widget_set_sensitive(mainwindow->to_btn, FALSE);
		gtk_widget_set_sensitive(mainwindow->cc_btn, FALSE);
		gtk_widget_set_sensitive(mainwindow->bcc_btn, FALSE);
	}
}

void list_view_append_contact(GtkTreeView* view, Contact* contact) {
    GtkTreeIter   iter;
    GtkListStore* store;
    GtkTreeModel* model;
	gchar *cn, *first, *last, *mail = NULL;

	cm_return_if_fail(view != NULL);
	cm_return_if_fail(contact != NULL);

	cn = first = last = mail = NULL;
    model = gtk_tree_view_get_model(view);
    store = GTK_LIST_STORE(model);

	if (contact->emails) {
		Email* email = (Email *) contact->emails->data;
		mail = email->email;
	}
	extract_data(contact->data, "first-name", (void *) &first);
	extract_data(contact->data, "last-name", (void *) &last);
	extract_data(contact->data, "cn", (void *) &cn);

	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter,
			CONTACT_DISPLAYNAME_COLUMN, cn,
			CONTACT_FIRSTNAME_COLUMN, first,
			CONTACT_LASTNAME_COLUMN, last,
			CONTACT_EMAIL_COLUMN, mail,
			CONTACT_DATA_COLUMN, contact,
	-1);
	g_free(first);
	g_free(last);
	g_free(cn);
}
