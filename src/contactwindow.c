/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <stdlib.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "contactwindow.h"
#include "plugin.h"
#include "plugin-loader.h"
#include "utils.h"
#include "gtk-utils.h"
#include "mainwindow.h"
#include "callbacks.h"
#include "extension-loader.h"

#define CONTACT_WINDOW_WIDTH 480
#define CONTACT_WINDOW_HEIGHT 400
#define PHOTO_WIDTH 140
#define PHOTO_HEIGHT 140

typedef enum {
	WINDOW_ALIAS,
	WINDOW_EMAIL,
	WINDOW_REMARKS,
	WINDOW_N_COLUMNS
} WindowColumn;

typedef struct {
	gchar* key;
	Contact* contact;
	GtkWidget* entry;
} ContactEntryData;

typedef struct {
	MainWindow*			main;
	AddressBook* 		abook;
	Plugin*				plugin;
	Contact*			contact;
	Contact*			old;
	GtkWidget*			window;
	GtkWidget*			tabs;
	GtkWidget*			email_list;
	GtkWidget*			image;
	ContactEntryData**	data;
	gboolean			new_row;
	gboolean			email_dirty;
} ContactWindow;

static gint contact_entry_data_size = 0;
static gboolean contact_is_new = FALSE;

static void email_list_add(GtkTreeView* view, ContactWindow* cw);
static gchar* hash_table_get_entry(GHashTable* hash, const gchar* key);

static void contact_entry_data_free(ContactEntryData** data) {
	gint i;

	for (i = 0; i < contact_entry_data_size; i++) {
		ContactEntryData* d = data[i];
		if (d) {
			g_free(d->key);
			g_free(d);
		}
	}
	g_free(data);
	contact_entry_data_size = 0;
}

static gboolean entry_dirty(GHashTable* hash,
							const gchar* key,
							const gchar* new_data) {
	gchar* value = NULL;
	
	value = hash_table_get_entry(hash, key);	
	gboolean res = xor(value, new_data);
	
	g_free(value);
	
	return res;
}

static gboolean contact_dirty(ContactWindow* cw) {
	gboolean result = FALSE;
	gint i;
	ContactEntryData** data = cw->data;
	gchar *cur_image, *old_image;

	if (cw->email_dirty)
		return TRUE;
	
	if (cw->old) {
		extract_data(cw->contact->data, "image", (gpointer) &cur_image);
		extract_data(cw->old->data, "image", (gpointer) &old_image);
		if (utf8_collate(cur_image, old_image) != 0)
			result = TRUE;
		g_free(cur_image);
		g_free(old_image);
	}

	for (i = 0; !result && i < contact_entry_data_size; i++) {
		Contact* contact = data[i]->contact;
		if (contact && contact->data && data[i]->key && data[i]->entry) {
			const gchar* new_data =
				gtk_entry_get_text(GTK_ENTRY(data[i]->entry));
			result = entry_dirty(contact->data, data[i]->key, new_data);
		}
	}
	
	return result;
}

static void contact_update(ContactWindow* cw, Contact* contact) {
	gchar* err = NULL;
	GtkTreeIter iter;
	GtkTreeModel* model;
    GtkTreeSelection* row;

	if (contact) {
		if (contact_is_new) {
			cw->plugin->set_contact(cw->abook, contact, &err);
			if (cw->main->use_extensions) {
				debug_print("AFTER_INIT_CONTACT_HOOK\n");
				run_hook_callbacks(EXTENSION_AFTER_INIT_CONTACT_HOOK, contact);
			}
		}
		else {
			err = contact_write_to_backend(cw->plugin, cw->abook, cw->old, contact);
		}
		if (err) {
			show_message(cw->window, GTK_UTIL_MESSAGE_WARNING, "%s", err);
			g_free(err);
			err = NULL;
		}
		else {
			cw->email_dirty = FALSE;
			if (contact_is_new) {
				if (debug_get_mode()) {
					contact_dump(contact, stderr);
				}
				list_view_append_contact(
						GTK_TREE_VIEW(cw->main->contact_list), contact);
				cw->abook->contacts = g_list_prepend(cw->abook->contacts, contact);
				while (gtk_events_pending())
  					gtk_main_iteration();
				contact_is_new = FALSE;
				if (cw->old) {
					contact_free(cw->old);
					g_free(cw->old);
				}
			}
			else {
				if (debug_get_mode()) {
					contact_dump(contact, stderr);
				}
				row = gtk_tree_view_get_selection(
						GTK_TREE_VIEW(cw->main->contact_list));
				if (row) {
					if (gtk_tree_selection_get_selected(row, &model, &iter)) {
						gtk_list_store_set(GTK_LIST_STORE(model), &iter,
							CONTACT_DATA_COLUMN, contact, -1);
					}
				}
				if (cw->old) {
					contact_free(cw->old);
					g_free(cw->old);
					cw->old = NULL;
				}
			}
		}
	}
	else {
		show_message(cw->window, GTK_UTIL_MESSAGE_ERROR,
			_("[plugin_update_contact] Unresolved error"));
	}
}

static void save_contact(ContactWindow* cw, gchar** error) {
	gint i;
	gboolean changed = FALSE;
	Contact *contact, *cur = NULL;
	const gchar* new_data;
	void* value = NULL;
	AttribDef* attr;
	gchar* tmp = NULL;
	gboolean bool;
	gchar ch;
	gint num;
	gchar *cur_image, *old_image;
/*    GtkTreeModel* model;
    GtkTreeIter iter;
    GSList* new_list = NULL;*/
	
	contact = cw->contact;
		
	for (i = 0; i < contact_entry_data_size; i++) {
		cur = cw->data[i]->contact;
		if (cur && cur->data && cw->data[i]->key) {
			attr = g_hash_table_lookup(cur->data, cw->data[i]->key);
			if (attr) {
				AttribType type = get_data(attr, &value);
				switch (type) {
					case ATTRIB_TYPE_BOOLEAN:
						bool = *(gboolean *) value;
						tmp = (bool) ? g_strdup("true") : g_strdup("false");
						break;
					case ATTRIB_TYPE_CHAR:
						ch = *(gchar *) value;
						tmp = g_strdup_printf("%c", ch);
						break;
					case ATTRIB_TYPE_INT:
						num = *(gint *) value;
						tmp = g_strdup_printf("%i", num);
						break;
					case ATTRIB_TYPE_STRING:
						tmp = g_strdup((gchar *) value);
						break;
				}
				g_free(value);
			}
			if (cw->data[i]->entry)
				new_data = gtk_entry_get_text(GTK_ENTRY(cw->data[i]->entry));
			else
				new_data = NULL;
			if (xor(tmp, new_data)) {
				swap_data(contact->data, (gpointer) cw->data[i]->key, (gpointer) new_data);
				changed = TRUE;
			}
			g_free(tmp);
			tmp = NULL;
		}
	}

	if (cw->old) {
		extract_data(cw->contact->data, "image", (gpointer) &cur_image);
		extract_data(cw->old->data, "image", (gpointer) &old_image);
		if (utf8_collate(cur_image, old_image) != 0)
			changed = TRUE;
		g_free(cur_image);
		g_free(old_image);
	}

	if (cw->email_dirty)
		changed = TRUE;
/*	    model = gtk_tree_view_get_model(GTK_TREE_VIEW(cw->email_list));
	 	if (gtk_tree_model_get_iter_first(model, &iter)) {
			do {
				Email* a = g_new0(Email, 1);
				gtk_tree_model_get(model, &iter, WINDOW_ALIAS, &a->alias,
					WINDOW_EMAIL, &a->email, WINDOW_REMARKS, &a->remarks, -1);
				new_list = g_slist_append(new_list, a);
			} while (gtk_tree_model_iter_next(model, &iter));
			gslist_free(&cw->contact->emails, email_free);
			cw->contact->emails = new_list;
		}
	}*/
	if (changed)
		contact_update(cw, cw->contact);
}

static gint delete_contact_window(GtkWidget* widget,
								  GdkEvent* event,
								  gpointer data) {
	ContactWindow* cw = (ContactWindow *) data;
	gchar* error = NULL;

	if (contact_dirty(cw)) {
		if (show_question(cw->window, _("Contact has unsaved data. Save?"))) {
			save_contact(cw, &error);
			if (error) {
				show_message(cw->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
				g_free(error);
			}
		}
	}	

	contact_entry_data_free(cw->data);
	
	abook_list_cursor_changed_cb(
			GTK_TREE_VIEW(cw->main->abook_list), cw->main);

	gtk_widget_destroy(cw->window);
	g_free(cw);
	cw = NULL;
	
    return FALSE;
}

static void quit_cb(GtkWidget* widget, gpointer data) {
	ContactWindow* cw = (ContactWindow *) data;
	
	delete_contact_window(widget, NULL, cw);
}

static void list_view_append_email(GtkTreeView* view, Email* email) {
    GtkTreeIter   iter;
    GtkListStore* store;
    GtkTreeModel* model;

    model = gtk_tree_view_get_model(view);
    store = GTK_LIST_STORE(model);

	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter,
		WINDOW_ALIAS, email->alias,
		WINDOW_EMAIL, email->email,
		WINDOW_REMARKS, email->remarks,
	-1);
}

#define LABEL_SIZE 10
static void email_add_cb(GtkWidget* widget, gpointer data) {
	ContactWindow* cw = (ContactWindow *) data;
	Contact* contact = NULL;
	Email* email;
	GtkWidget *dialog, *frame, *hbox, *label, *alias, *mail, *remark;
	
	email = g_new0(Email, 1);
	dialog = gtk_dialog_new_with_buttons("New email address",
		GTK_WINDOW(cw->window), GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT, NULL);
	gtk_window_set_default_size(GTK_WINDOW(dialog), 300, 50);
	
	frame = gtk_frame_new(NULL);
	label = gtk_label_new("Alias");
	gtk_label_set_width_chars(GTK_LABEL(label), LABEL_SIZE);
	alias = gtk_entry_new();
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), alias, TRUE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(frame), hbox);
	gtk_widget_show_all(frame);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), frame);
	
	frame = gtk_frame_new(NULL);
	label = gtk_label_new("Email");
	gtk_label_set_width_chars(GTK_LABEL(label), LABEL_SIZE);
	mail = gtk_entry_new();
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), mail, TRUE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(frame), hbox);
	gtk_widget_show_all(frame);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), frame);

	frame = gtk_frame_new(NULL);
	label = gtk_label_new("Remarks");
	gtk_label_set_width_chars(GTK_LABEL(label), LABEL_SIZE);
	remark = gtk_entry_new();
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), remark, TRUE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(frame), hbox);
	gtk_widget_show_all(frame);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), frame);

	gint result = gtk_dialog_run(GTK_DIALOG(dialog));
	switch (result) {
		case GTK_RESPONSE_ACCEPT:
			contact = cw->contact;
			if (contact) {
				email->alias = gtk_editable_get_chars(GTK_EDITABLE(alias), 0, -1);
				email->email = gtk_editable_get_chars(GTK_EDITABLE(mail), 0, -1);
				email->remarks = gtk_editable_get_chars(GTK_EDITABLE(remark), 0, -1);
				contact->emails = g_slist_append(contact->emails, email);
				cw->new_row = TRUE;
				cw->email_dirty = TRUE;
				list_view_append_email(GTK_TREE_VIEW(cw->email_list), email);
			}
			else
				email_free(email);
			break;
		default:
			email_free(email);
	}
	gtk_widget_destroy(dialog);
}

static void email_delete_cb(GtkWidget* widget, gpointer data) {
	ContactWindow* cw = (ContactWindow *) data;
    GtkTreeIter   iter;
    GtkTreeModel* model;
    GtkTreeSelection* row;
    gchar *alias = NULL, *email = NULL, *remark = NULL;

	row = gtk_tree_view_get_selection(GTK_TREE_VIEW(cw->email_list));
	model = gtk_tree_view_get_model(GTK_TREE_VIEW(cw->email_list));
	if (row) {
		if (gtk_tree_selection_get_selected(row, &model, &iter)) {
			gtk_tree_model_get(model, &iter,
			WINDOW_ALIAS, &alias,
			WINDOW_EMAIL, &email,
			WINDOW_REMARKS, &remark,
			-1);
			if (email) {
				gboolean r = show_question(cw->window,
					_("%s: Delete email address?"), email);
				if (r) {
					gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
					cw->email_dirty = TRUE;
				}
			}
		}
		else {
			show_message(cw->window, GTK_UTIL_MESSAGE_INFO,
				_("Highlight desired email address to delete"));
		}
	}
	else {
		show_message(cw->window, GTK_UTIL_MESSAGE_INFO,
			_("Highlight desired email address to delete"));
	}
}

static void contact_save_cb(GtkWidget* widget, gpointer data) {
	ContactWindow* cw = (ContactWindow *) data;
	const gchar* label = gtk_widget_get_name(widget);
	gchar* error = NULL;
	
	if (strcmp(label, GTK_STOCK_OK) == 0) {
		if (cw->main->use_extensions) {
			debug_print("BEFORE_ADD_CONTACT_HOOK\n");
			run_hook_callbacks(EXTENSION_BEFORE_ADD_CONTACT_HOOK, cw->contact);
		}
		save_contact(cw, &error);
		if (error) {
			show_message(cw->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
			g_free(error);
		}
		if (cw->main->use_extensions) {
			debug_print("AFTER_ADD_CONTACT_HOOK\n");
			run_hook_callbacks(EXTENSION_AFTER_ADD_CONTACT_HOOK, cw->contact);
		}
		quit_cb(widget, data);
	}
	else if (strcmp(label, GTK_STOCK_APPLY) == 0) {
		if (cw->main->use_extensions) {
			debug_print("AFTER_ADD_CONTACT_HOOK\n");
			run_hook_callbacks(EXTENSION_AFTER_ADD_CONTACT_HOOK, cw->contact);
		}
		save_contact(cw, &error);
		if (error) {
			show_message(cw->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
			g_free(error);
		}
		if (cw->main->use_extensions) {
			debug_print("AFTER_ADD_CONTACT_HOOK\n");
			run_hook_callbacks(EXTENSION_AFTER_ADD_CONTACT_HOOK, cw->contact);
		}
	}
	else {
		/* we should never end here */
	}
}

static void contact_set_attr(ContactWindow* cw,
							 guint row,
							 WindowColumn column,
							 const gchar* value) {
	Email* mail;
	Contact* contact = cw->contact;
	
	mail = (Email *) g_slist_nth_data(contact->emails, row);
	
	switch (column) {
		case WINDOW_ALIAS:
			g_free(mail->alias);
			mail->alias = g_strdup(value);
			break;
		case WINDOW_EMAIL:
			g_free(mail->email);
			mail->email = g_strdup(value);
			break;
    	case WINDOW_REMARKS:
			g_free(mail->remarks);
			mail->remarks = g_strdup(value);
			break;
		default:
			break;
	}
}

static gchar* hash_table_get_entry(GHashTable* hash, const gchar* key) {
	AttribDef* attr;
	gchar* value = NULL;
	
	attr = g_hash_table_lookup(hash, key);
	
	if (attr) {
		switch (attr->type) {
			case ATTRIB_TYPE_BOOLEAN:
				value = (attr->value.boolean) ? g_strdup("true") : g_strdup("false");
				break;
			case ATTRIB_TYPE_INT:
				value = g_new0(gchar, 5);
				sprintf(value, "%i", attr->value.number);
				break;
			case ATTRIB_TYPE_CHAR:
				value = g_new0(gchar, 1);
				sprintf(value, "%c", attr->value.character);
				break;
			case ATTRIB_TYPE_STRING:
				value = g_strdup(attr->value.string);
				break;
		}
	}
	return (value) ? value : g_strdup("");
}

static void contact_image_change(gpointer data) {
	ContactWindow* cw = (ContactWindow *) data;
	GtkWidget *dialog;
	gchar* file = NULL;
	GtkFileFilter* filter;
	gchar* home;
	GdkPixbuf* pixbuf;
	GError* err = NULL;
	
	debug_print("Address book: %s\n", cw->abook->abook_name);

	if (! attribute_supported(cw->plugin, "image")) {
		show_message(cw->window, GTK_UTIL_MESSAGE_INFO,
			_("Address book does not support saving images"));
		return;
	}
	
	filter = gtk_file_filter_new();
	gtk_file_filter_set_name(filter, "Image");
	gtk_file_filter_add_pixbuf_formats(filter);
	
	dialog = gtk_file_chooser_dialog_new("Select File",
					      GTK_WINDOW(cw->main->window),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					      NULL);
	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
	gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), FALSE);
	home = cw->plugin->default_url(NULL);
	if (! home)
		home = get_self_home();
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), home);
	g_free(home);
	
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));

		pixbuf = gdk_pixbuf_new_from_file_at_scale(
			file, PHOTO_WIDTH, PHOTO_HEIGHT, TRUE, &err);
		g_clear_error(&err);
		
		if (pixbuf) {
			gchar* image = base64_encode_data(file);
			swap_data(cw->contact->data, "image", image);
			g_free(image);
			gtk_image_clear(GTK_IMAGE(cw->image));
			gtk_image_set_from_pixbuf(GTK_IMAGE(cw->image), pixbuf);
			g_object_unref(pixbuf);
		}
		
		g_free(file);
	}

	//g_object_unref(filter);
	gtk_widget_destroy(dialog);
}

static void image_button_activate_cb(GtkMenuItem* widget, gpointer data) {
	debug_print("Image dialog activated\n");
	
	contact_image_change(data);
}

static gboolean image_button_pressed_cb(GtkWidget* widget,
										GdkEventButton* event,
										gpointer data) {
    debug_print("Button pressed: %d\n", event->button);
    if (event->button == 1 && event->type == GDK_BUTTON_PRESS) {
		contact_image_change(data);
		return TRUE;
    }
    return FALSE;
}

static void email_cell_edited_cb(GtkCellRendererText *cell,
							gchar *path_string,
							gchar *new_text,
							gpointer data) {
	ContactWindow* win = (ContactWindow *) data;
	GtkTreeIter iter;
	GtkTreeModel* model;
	GtkTreeViewColumn* column;
	GtkTreePath* path;
	guint col;
	
	debug_print("Path: %s - New text: %s\n", path_string, new_text);
	model = gtk_tree_view_get_model(GTK_TREE_VIEW(win->email_list));
	gtk_tree_model_get_iter_from_string(model, &iter, path_string);
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(win->email_list), &path, &column);

	gtk_tree_path_free(path);

	if (cell->text && new_text && strcmp(cell->text, new_text) == 0)
		return;
	
	for (col = WINDOW_ALIAS; col < WINDOW_N_COLUMNS; col++) {
		if (column == gtk_tree_view_get_column(
				GTK_TREE_VIEW(win->email_list),col))
			break;
	}

	if (! contact_is_new) {
		contact_set_attr(win, atoi(path_string), col, new_text);
	}
	
	win->email_dirty = TRUE;
	gtk_list_store_set(GTK_LIST_STORE(model), &iter, col, new_text, -1);
}

static ContactEntryData* get_contact_entry_data(
		ContactEntryData** list, const gchar* name)  {
	gint i;
	
	for (i = 0; i < contact_entry_data_size; i++) {
		if (strcmp(list[i]->key, name) == 0)
			return list[i];
	}
	
	return NULL;
}

static GtkWidget* create_entry_row(GHashTable* hash,
							 	   ContactEntryData* data,
							 	   const gchar* title,
							 	   gint title_size) {
	GtkWidget *frame = NULL, *label, *entry, *hbox;
	gchar* value;

	if (data) {
		frame = gtk_frame_new(NULL);
		gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_NONE);
	
		label = gtk_label_new(title);
		gtk_label_set_width_chars(GTK_LABEL(label), title_size);
		entry = gtk_entry_new();
		data->entry = entry;
	
		if (hash)
			value = hash_table_get_entry(hash, data->key);
		else
			value = g_strdup("");
	
		gtk_entry_set_text(GTK_ENTRY(entry), value);
		g_free(value);
		hbox = gtk_hbox_new(FALSE, 5);
		gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);
		gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
		gtk_container_add(GTK_CONTAINER(frame), hbox);
	}
	
	return frame;
}

static GtkWidget* extended_attributes(ContactWindow* cw) {
	GtkWidget *container, *row;
	Contact* contact = cw->contact;
	gint i;
	
	container = gtk_vbox_new(FALSE, 0);

	if (contact && contact->data) {
		for (i = 0; i < contact_entry_data_size; i++) {
			gchar* key = cw->data[i]->key;
			if (strcmp(key, "cn") == 0 ||
				strcmp(key, "first-name") == 0 ||
				strcmp(key, "last-name") == 0 ||
				strcmp(key, "nick-name") == 0 ||
				strcmp(key, "image") == 0 ||
				strcmp(key, "email") == 0)
				continue;
			cw->data[i]->contact = contact;
			row = create_entry_row(contact->data, cw->data[i], key, 20);
			gtk_box_pack_start(GTK_BOX(container), row, FALSE, TRUE, 0);
		}
	}

	return container;
}

static GtkWidget* standard_attributes(ContactWindow* cw) {
	GtkWidget *container, *row;
	Contact* contact = cw->contact;
	ContactEntryData* data;
	const gchar *locale;
	
	container = gtk_vbox_new(FALSE, 0);
	
	if (contact && contact->data) {
		data = get_contact_entry_data(cw->data, "cn");
		data->contact = contact;
		row = create_entry_row(contact->data, data, "Display name", 12);
		gtk_box_pack_start(GTK_BOX(container), row, FALSE, TRUE, 0);

	locale = conv_get_current_locale();
	if (locale &&
		(!g_ascii_strncasecmp(locale, "hu", 2) ||
		 !g_ascii_strncasecmp(locale, "ja", 2) ||
		 !g_ascii_strncasecmp(locale, "ko", 2) ||
		 !g_ascii_strncasecmp(locale, "vi", 2) ||
		 !g_ascii_strncasecmp(locale, "zh", 2))) {
		data = get_contact_entry_data(cw->data, "last-name");
		data->contact = contact;
		row = create_entry_row(contact->data, data, "Last name", 12);
		gtk_box_pack_start(GTK_BOX(container), row, FALSE, TRUE, 0);

		data = get_contact_entry_data(cw->data, "first-name");
		data->contact = contact;
		row = create_entry_row(contact->data, data, "First name", 12);
		gtk_box_pack_start(GTK_BOX(container), row, FALSE, TRUE, 0);
	} else {
		data = get_contact_entry_data(cw->data, "first-name");
		data->contact = contact;
		row = create_entry_row(contact->data, data, "First name", 12);
		gtk_box_pack_start(GTK_BOX(container), row, FALSE, TRUE, 0);

		data = get_contact_entry_data(cw->data, "last-name");
		data->contact = contact;
		row = create_entry_row(contact->data, data, "Last name", 12);
		gtk_box_pack_start(GTK_BOX(container), row, FALSE, TRUE, 0);
	}
		data = get_contact_entry_data(cw->data, "nick-name");
		data->contact = contact;
		row = create_entry_row(contact->data, data, "Nick name", 12);
		gtk_box_pack_start(GTK_BOX(container), row, FALSE, TRUE, 0);
	}
	return container;
}

static void email_list_add(GtkTreeView* view, ContactWindow* cw) {
	GSList* cur;
	Contact* contact = cw->contact;
	
	list_view_clear(view, cw->main);
	for (cur = contact->emails; cur; cur = g_slist_next(cur)) {
		Email* email = (Email *) cur->data;
		cw->new_row = TRUE;
		list_view_append_email(view, email);
	}
}

static gboolean contact_reorder_emails(GtkTreeModel *model,
								   GtkTreePath *path,
								   GtkTreeIter *iter,
								   gpointer data) {
	ContactWindow* cw = (ContactWindow *) data;
	Email* email = g_new0(Email, 1);
	
	gtk_tree_model_get(model, iter,
		WINDOW_ALIAS, &email->alias,
		WINDOW_EMAIL, &email->email,
		WINDOW_REMARKS, &email->remarks, -1);

	if (debug_get_mode()) {	
		gchar* s = gtk_tree_path_to_string(path);
		debug_print("Get row at: %s\n", s);
		g_free(s);
		email_dump(email, stderr);
	}

	cw->contact->emails = g_slist_append(cw->contact->emails, email);

	return FALSE;
}

static void row_deleted_cb(GtkTreeModel *tree_model,
                                                        GtkTreePath  *path,
                                                        gpointer      data) {
	ContactWindow* cw = (ContactWindow *) data;
	
	gchar* s = gtk_tree_path_to_string(path);
	debug_print("deleted: %s\n", s);
	g_free(s);
	gslist_free(&cw->contact->emails, email_free);
	cw->contact->emails = NULL;
	gtk_tree_model_foreach(tree_model, contact_reorder_emails, cw);
	cw->email_dirty = TRUE;
}

static gboolean contactwindow_key_press_event_cb(GtkWidget *widget,
									   			 GdkEventKey *event,
									   			 gpointer   user_data) {
	if (!event) return FALSE;
	
	switch (event->keyval) {
		case GDK_KEY_Escape:
			delete_contact_window(widget, (GdkEvent *) event, user_data);
		default:
			break;
	}
	
	return FALSE;
}

static void contact_widget(ContactWindow* cw) {
	GtkWidget *label, *vbox, *hbox, *win1, *win2, *frame,
			  *add_btn, *delete_btn, *cancel_btn, *ok_btn, *apply_btn,
			  *pix_event_box;
	GtkAccelGroup* accel;
    GtkCellRenderer *renderer;
    GtkTreeViewColumn *column;
    GtkListStore* list;
    gchar* photo = NULL;
    GError* err = NULL;
    GdkPixbuf* pixbuf;
	
	cw->window = g_object_new(
				GTK_TYPE_WINDOW,
				"title", _("Contact details"),
				"default-width", CONTACT_WINDOW_WIDTH,
				"default-height", CONTACT_WINDOW_HEIGHT,
				"window-position", GTK_WIN_POS_CENTER_ON_PARENT,
				"transient-for", cw->main->window,
				"destroy-with-parent", TRUE,
				NULL);
	g_signal_connect(
        cw->window, "delete_event", G_CALLBACK(delete_contact_window), cw);
	accel = g_object_new(GTK_TYPE_ACCEL_GROUP, NULL);
	gtk_window_add_accel_group(GTK_WINDOW(cw->window), accel);

	
	cw->tabs = gtk_notebook_new();

	/* tab 1 begin */
	vbox = gtk_vbox_new(FALSE, 5);
	
	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 5);

	frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
	gtk_widget_set_size_request(frame, PHOTO_WIDTH, PHOTO_HEIGHT);
	GtkWidget* label_photo = gtk_menu_item_new_with_label("Image");
	gtk_widget_add_accelerator(label_photo, "activate", accel,
            GDK_I, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	g_signal_connect(label_photo, "activate",
		G_CALLBACK(image_button_activate_cb), cw);
	gtk_frame_set_label_widget(GTK_FRAME(frame), label_photo);
	
	if (cw->contact->data) {
		if (debug_get_mode())
			contact_dump(cw->contact, stderr);
		AttribDef* attr = g_hash_table_lookup(cw->contact->data, "image");
		if (attr) {
			photo = g_strdup(attr->value.string);
			if (photo) {
				pixbuf = contact_load_image(cw->main->window, photo);
				if (! pixbuf) {
					g_free(photo);
					photo = NULL;
				}
			}
		}
	}
	
	if (! photo) {
		photo = g_strdup_printf("%s/%s", PIXDIR, "anonymous.xpm");
		pixbuf = gdk_pixbuf_new_from_file_at_scale(
			photo, PHOTO_WIDTH, PHOTO_HEIGHT, TRUE, &err);
	}
	g_free(photo);
	
	pix_event_box = gtk_event_box_new();
	gtk_widget_set_tooltip_text(pix_event_box,
	_("Mouse-left-click on photo, or Ctrl+I will activate edit mode"));
	g_signal_connect(pix_event_box, "button-press-event",
		G_CALLBACK(image_button_pressed_cb), cw);

	if (pixbuf) {
		cw->image = gtk_image_new_from_pixbuf(pixbuf);
		gtk_container_add(GTK_CONTAINER(pix_event_box), cw->image);	
		g_object_unref(pixbuf);
	}
	
	gtk_container_add(GTK_CONTAINER(frame), pix_event_box);
	
	gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, TRUE, 5);

	GtkWidget* entries = standard_attributes(cw);
	gtk_box_pack_start(GTK_BOX(hbox), entries, TRUE, TRUE, 5);
	
	frame = gtk_frame_new("Email addresses");
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
	GtkWidget* email_frame = gtk_vbox_new(FALSE, 5);
	gtk_container_add(GTK_CONTAINER(frame), email_frame);
	
    list = gtk_list_store_new(WINDOW_N_COLUMNS, G_TYPE_STRING,
		G_TYPE_STRING, G_TYPE_STRING, -1);
	/* reorder email_list when user changes order in view */
	g_signal_connect(GTK_TREE_MODEL(list), "row-deleted",
			G_CALLBACK(row_deleted_cb), cw);
	cw->email_list = gtk_tree_view_new_with_model(GTK_TREE_MODEL(list));
    g_object_unref(list);
    gtk_widget_set_tooltip_text(GTK_WIDGET(cw->email_list),
        _("Double-click, enter, or space on cell will activate edit mode\n"
          "Mouse-Left-click to drag and drop for reordering email list\n"
          "First email in list is set as default email"));
    gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(cw->email_list), TRUE);
    gtk_tree_view_set_reorderable(GTK_TREE_VIEW(cw->email_list), TRUE);

    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited",
            G_CALLBACK(email_cell_edited_cb), cw);
    column = gtk_tree_view_column_new_with_attributes(
            _("Alias"), renderer, "text", WINDOW_ALIAS, NULL);
    gtk_tree_view_column_set_alignment(column, 0.5);
    gtk_tree_view_column_set_min_width(column, 100);
    gtk_tree_view_column_set_resizable(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(cw->email_list), column);

    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited",
            G_CALLBACK(email_cell_edited_cb), cw);
    column = gtk_tree_view_column_new_with_attributes(
            _("Email"), renderer, "text", WINDOW_EMAIL, NULL);
    //gtk_tree_view_column_set_sort_column_id(column, 0);
    gtk_tree_view_column_set_alignment(column, 0.5);
    gtk_tree_view_column_set_min_width(column, 100);
    gtk_tree_view_column_set_resizable(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(cw->email_list), column);

    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer, "editable", TRUE, NULL);
    g_signal_connect(renderer, "edited",
            G_CALLBACK(email_cell_edited_cb), cw);
    column = gtk_tree_view_column_new_with_attributes(
            _("Remarks"), renderer, "text", WINDOW_REMARKS, NULL);
    gtk_tree_view_column_set_alignment(column, 0.5);
    gtk_tree_view_column_set_min_width(column, 100);
    gtk_tree_view_column_set_resizable(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(cw->email_list), column);
	email_list_add(GTK_TREE_VIEW(cw->email_list), cw);
	
	GtkWidget* scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll),
            GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);    
	gtk_container_add(GTK_CONTAINER(scroll), cw->email_list);
	gtk_box_pack_start(GTK_BOX(email_frame), scroll, FALSE, TRUE, 5);

	add_btn = gtk_button_new_with_mnemonic(_("_Add"));
	g_signal_connect(
        add_btn, "clicked", G_CALLBACK(email_add_cb), cw);

	delete_btn = gtk_button_new_with_mnemonic(_("_Delete"));
	g_signal_connect(
        delete_btn, "clicked", G_CALLBACK(email_delete_cb), cw);

	hbox = gtk_hbutton_box_new();
	gtk_button_box_set_layout(GTK_BUTTON_BOX(hbox), GTK_BUTTONBOX_CENTER);
	gtk_box_pack_start(GTK_BOX(hbox), add_btn, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), delete_btn, FALSE, FALSE, 0);

	gtk_box_pack_start(GTK_BOX(email_frame), hbox, FALSE, TRUE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 5);

	win1 = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(win1),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_add_with_viewport(
			GTK_SCROLLED_WINDOW(win1), vbox);

	label = gtk_label_new_with_mnemonic(_("_Standard attributes"));
	gtk_notebook_append_page(GTK_NOTEBOOK(cw->tabs), win1, label);
	/* tab 1 end */
	
	/* tab 2 begin */
	vbox = gtk_vbox_new(FALSE, 5);
	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 5);
	
	GtkWidget* extended = extended_attributes(cw);
	gtk_box_pack_start(GTK_BOX(hbox), extended, TRUE, TRUE, 5);

	win2 = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(win2),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_add_with_viewport(
			GTK_SCROLLED_WINDOW(win2), vbox);
	label = gtk_label_new_with_mnemonic(_("_Extended attributes"));
	
	gtk_notebook_append_page(GTK_NOTEBOOK(cw->tabs), win2, label);
	/* tab 2 end */
	
	cancel_btn = gtk_button_new_from_stock(GTK_STOCK_CANCEL);
	g_signal_connect(
        cancel_btn, "clicked", G_CALLBACK(quit_cb), cw);

	ok_btn = gtk_button_new_from_stock(GTK_STOCK_OK);
	gtk_widget_set_name(ok_btn, GTK_STOCK_OK);
	g_signal_connect(ok_btn, "clicked", G_CALLBACK(contact_save_cb), cw);

	apply_btn = gtk_button_new_from_stock(GTK_STOCK_APPLY);
	/* mnemonic A assigned to add */
	gtk_button_set_label(GTK_BUTTON(apply_btn), _("A_pply"));
	gtk_widget_set_name(apply_btn, GTK_STOCK_APPLY);
	g_signal_connect(
        apply_btn, "clicked", G_CALLBACK(contact_save_cb), cw);

	hbox = gtk_hbutton_box_new();
	gtk_button_box_set_layout(GTK_BUTTON_BOX(hbox), GTK_BUTTONBOX_END);
	gtk_box_pack_start(GTK_BOX(hbox), apply_btn, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), ok_btn, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), cancel_btn, FALSE, FALSE, 0);
		
	vbox = gtk_vbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), cw->tabs, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(cw->window), vbox);
	
    g_signal_connect(cw->window, "key-press-event",
        G_CALLBACK(contactwindow_key_press_event_cb), cw);

	gtk_widget_show_all(vbox);
}

static void contact_search_get(ContactEntryData** data, Contact* contact) {
	gint i;
	const gchar* value;
	const gchar* term;
	Email* email = NULL;
	
	for (i = 0; i < contact_entry_data_size; i++) {
		if (data[i]) {
			value = gtk_entry_get_text(GTK_ENTRY(data[i]->entry));
			if (value && strlen(value) > 0) {
				if (strcmp(data[i]->key, "alias") == 0 ||
					strcmp(data[i]->key, "email") == 0 ||
					strcmp(data[i]->key, "remarks") == 0) {
					if (!email)
						email = g_new0(Email, 1);
					if (strcmp(data[i]->key, "alias") == 0)
						email->alias = g_strdup(value);
					else if (strcmp(data[i]->key, "email") == 0)
						email->email = g_strdup(value);
					else
						email->remarks = g_strdup(value);
				}
				else {
					if (strcmp("Display name", data[i]->key) == 0)
						term = "cn";
					else
						term = data[i]->key;
					swap_data(contact->data, term, (gpointer) value);
				}
			}
		}
	}
	if (email)
		contact->emails = g_slist_prepend(contact->emails, email);
}

gboolean contact_show(MainWindow* main, Contact* contact, gchar** error) {
	GtkTreeView* view;
	GtkTreeIter* iter;
	ContactWindow* dialog;
	gint i;

	dialog = g_new0(ContactWindow, 1);
	dialog->main = main;
	dialog->old = contact;
	//dialog->changed_emails = NULL;
	dialog->contact = contact_copy(contact);
	dialog->new_row = FALSE;
	
	view = GTK_TREE_VIEW(main->abook_list);
	dialog->abook = get_selected_address_book(view);
	if (dialog->abook == NULL) {
		iter = set_selection_combobox(main->window,
						_("[New Contact] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return FALSE;
		GtkTreeSelection* row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		dialog->abook = get_selected_address_book(view);
	}
	dialog->plugin = get_selected_plugin(view);
	if (dialog->abook && dialog->plugin) {
		GSList* attr = dialog->plugin->attrib_list();
		contact_entry_data_size = g_slist_length(attr);
		dialog->data = g_new0(ContactEntryData *, contact_entry_data_size);
		for (i = 0; i < contact_entry_data_size; i++) {
			dialog->data[i] = g_new0(ContactEntryData, 1);
			AttribDef* attrdef = g_slist_nth_data(attr, i);
			dialog->data[i]->key = g_strdup(attrdef->attrib_name);
		}				
		gslist_free(&attr, attrib_def_free);

		if (! dialog->contact) {
			contact_is_new = TRUE;
			dialog->contact = contact_new();
		}
		else
			contact_is_new = FALSE;
			
		if (! dialog->contact->data) {
			dialog->contact->data = hash_table_new();
		}
			
		contact_widget(dialog);
		gtk_widget_show(dialog->window);
	}
	else {
		show_message(main->window, GTK_UTIL_MESSAGE_INFO, _("Please highlight desired"
			" address book holding contacts"));
	}
	
	return FALSE;
}

Contact* contact_search_show(MainWindow* main, gboolean* is_and, gchar** error) {
	Contact* contact = NULL;
	GtkTreeView* view;
	GtkTreeIter* iter;
	AddressBook* abook;
	Plugin* plugin;
	GtkWidget *dialog, *row, *vbox, *viewport;
	ContactEntryData** data;
	gint i, pos;
	gchar* key;
	const gchar tooltip[] = 
			N_("Seach using term for contact in current address book.\n"
            "Search is done in all the listed fields if it contains a term.\n"
            "Empty fields are disregarded in the search.\n"
            "Use * or ? for wildcard. Search is case sensitive.");
	
	view = GTK_TREE_VIEW(main->abook_list);
	abook = get_selected_address_book(view);
	if (abook == NULL) {
		iter = set_selection_combobox(main->window,
						_("[Locate Contact] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return contact;
		GtkTreeSelection* row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		abook = get_selected_address_book(view);
	}
	plugin = get_selected_plugin(view);
	if (abook && plugin) {
		contact = contact_new();
		dialog = gtk_dialog_new_with_buttons(
									_("Locate contact"),
									GTK_WINDOW(main->window),
									GTK_DIALOG_DESTROY_WITH_PARENT,
									GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
									GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
									NULL);
		gtk_widget_set_size_request(dialog, 500, 600);
		
		viewport = gtk_scrolled_window_new(NULL, NULL);
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(viewport),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), viewport);
		
		vbox = gtk_vbox_new(TRUE, 5);

		GSList* attr = plugin->attrib_list();
		contact_entry_data_size = g_slist_length(attr) + 3;
		data = g_new0(ContactEntryData *, contact_entry_data_size);
		for (i = pos = 0; i < contact_entry_data_size - 3; i++) {
			AttribDef* attrdef = g_slist_nth_data(attr, i);
			if (strcmp(attrdef->attrib_name, "image") == 0 ||
				strcmp(attrdef->attrib_name, "email") == 0)
				continue;
			if (strcmp(attrdef->attrib_name, "cn") == 0)
				key = "Display name";
			else
				key = attrdef->attrib_name;

			data[pos] = g_new0(ContactEntryData, 1);
			data[pos]->key = g_strdup(key);
			row = create_entry_row(NULL, data[pos++], key, 20);
			gtk_widget_set_tooltip_text(row, tooltip);
			gtk_box_pack_start(GTK_BOX(vbox), row, FALSE, TRUE, 0);
		}				
		gslist_free(&attr, attrib_def_free);

		data[pos] = g_new0(ContactEntryData, 1);
		data[pos]->key = g_strdup("alias");
		row = create_entry_row(NULL, data[pos++], "email alias", 20);
		gtk_box_pack_start(GTK_BOX(vbox), row, FALSE, TRUE, 0);
		
		data[pos] = g_new0(ContactEntryData, 1);
		data[pos]->key = g_strdup("email");
		row = create_entry_row(NULL, data[pos++], "email address", 20);
		gtk_box_pack_start(GTK_BOX(vbox), row, FALSE, TRUE, 0);
		
		data[pos] = g_new0(ContactEntryData, 1);
		data[pos]->key = g_strdup("remarks");
		row = create_entry_row(NULL, data[pos], "email remarks", 20);
		gtk_box_pack_start(GTK_BOX(vbox), row, FALSE, TRUE, 0);
		
		GtkWidget* align = gtk_alignment_new(0.5, 0.5, 0, 0);
		GtkWidget* rbox = gtk_hbox_new(TRUE, 2);
		gtk_container_add(GTK_CONTAINER(align), rbox);
		
		GtkWidget* radio_or = 
			gtk_radio_button_new_with_mnemonic(NULL, _("O_r"));
		GtkWidget* radio_and =
			gtk_radio_button_new_with_mnemonic_from_widget(
				GTK_RADIO_BUTTON(radio_or), _("_And"));
		gtk_box_pack_start(GTK_BOX(rbox), radio_or, FALSE, TRUE, 2);
   		gtk_box_pack_start(GTK_BOX(rbox), radio_and, FALSE, TRUE, 2);
   		gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);
		
		dialog_set_focus(GTK_DIALOG(dialog), GTK_STOCK_CANCEL);	

		gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(viewport), vbox);

		gtk_widget_show_all(dialog);
		
		gint r = gtk_dialog_run(GTK_DIALOG(dialog));
		switch (r) {
			case GTK_RESPONSE_ACCEPT:
				contact_search_get(data, contact);
				if (debug_get_mode())
					contact_dump(contact, stderr);
				*is_and = gtk_toggle_button_get_active(
					GTK_TOGGLE_BUTTON(radio_and));
				break;
			default:
				contact_free(contact);
				g_free(contact);
				contact = NULL;
				break;
		}
		gtk_widget_destroy(dialog);
		contact_entry_data_free(data);
	}
	else {
		show_message(main->window, GTK_UTIL_MESSAGE_INFO, _("Please highlight desired"
			" address book holding contacts"));
	}
	return contact;
}
