/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <gtk/gtk.h>
#include "extension.h"
#include "utils.h"
#include "gtk-utils.h"
#include "plugin.h"
#include "plugin-loader.h"
#include "callbacks.h"
#include "wizard.h"

#define NAME "LDIF Export extension"

static guint my_id;
static const gchar* ObjectClass[] = {
	"objectClass: person",
	"objectClass: organizationalPerson",
	"objectClass: inetOrgPerson",
	NULL
};
static const gchar* LDIF_Version = "version: 1";
static Plugin* plugin;
static AddressBook* abook;
static GtkTreeView* view;

static gchar* get_dn(Contact* contact, ExportData* data) {
	gchar *tmp = NULL, *dn = NULL;
	
	extract_data(contact->data, "dn", (gpointer) &tmp);
	if (! tmp) {
		extract_data(contact->data, "DN", (gpointer) &tmp);
		if (tmp)
			g_hash_table_remove(contact->data, "DN");
	}
	else
		g_hash_table_remove(contact->data, "dn");

	if (tmp && data->use_dn)
		dn = g_strdup_printf("dn: %s\n", tmp);
	else {
		g_free(tmp);
		tmp = NULL;
		if (strcmp("display name", data->rdn) == 0) {
			extract_data(contact->data, "nick-name", (gpointer) &tmp);
			if (tmp) {
				dn = g_strconcat("dn: displayName=", tmp, ",", data->suffix, "\n", NULL);
				g_free(tmp);
				tmp = NULL;
			}
			else {
				return dn;
			}
		}
		else if (strcmp("cn", data->rdn) == 0) {
			extract_data(contact->data, "cn", (gpointer) &tmp);
			if (tmp) {
				dn = g_strconcat("dn: cn=", tmp, ",", data->suffix, "\n", NULL);
				g_free(tmp);
				tmp = NULL;
			}
			else {
				return dn;
			}
		}
		else {
			if (contact->emails && contact->emails->data) {
				Email* e = (Email *) contact->emails->data;
				if (e->email) {
					dn = g_strconcat("dn: mail=", e->email, ",",
							data->suffix, "\n", NULL);
				}
			}
		}
	}
	g_free(tmp);
	
	return dn;
}

static gchar* contact_as_ldif(Contact* contact, ExportData* data) {
	gchar* ldif = NULL;
	GString* text;
	GHashTableIter iter;
	gpointer key, value;
	gchar* attrib;
	Contact* tmp;
	GSList* cur;
	const gchar** class = ObjectClass;

	tmp = contact_copy(contact);
	text = g_string_new("");

	while (*class)
		g_string_append_printf(text, "%s\n", *class++);
	
	ldif = get_dn(tmp, data);
	if (ldif) {
		text = g_string_append(text, ldif);
		g_free(ldif);
	}
	else {
		contact_free(tmp);
		g_string_free(text, TRUE);
		return NULL;
	}
	/* remove internal key (uid) */
	g_hash_table_remove(tmp->data, "uid");

	g_hash_table_iter_init(&iter, tmp->data);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		get_data((AttribDef *) value, (gpointer) &attrib);
		if (attrib && strlen(attrib) > 0) {
			debug_print("%s -> %s\n", native2ldap(key), attrib);
			if (strcasecmp("image", key) == 0)
				g_string_append_printf(text, "%s:: %s\n",
					native2ldap(key), attrib);
			else
				g_string_append_printf(text, "%s: %s\n",
					native2ldap(key), attrib);
		}
		g_free(attrib);
	}
	cur = tmp->emails;
	while (cur) {
		Email* e = (Email *) cur->data;
		if (e->email && strlen(e->email) > 0)
			g_string_append_printf(text, "mail: %s\n", e->email);
		cur = cur->next;
	}
	
	contact_free(tmp);
	
	return g_string_free(text, FALSE);
}

static gchar* get_contacts_as_ldif(MenuItem* item, ExportData* data) {
	GString* text = g_string_new("");
	gchar* ldif;
	GList* list;

	view = GTK_TREE_VIEW(item->mainwindow->abook_list);
	plugin = get_selected_plugin(view);
	abook = get_selected_address_book(view);

	for (list = abook->contacts; list; list = g_list_next(list)) {
		Contact* contact = (Contact *) list->data;
		ldif = contact_as_ldif(contact, data);
		if (ldif) {
			text = g_string_append(text, ldif);
			g_free(ldif);
			text = g_string_append(text, "\n");
		}
	}
	
	return g_string_free(text, FALSE);
}

static void make_ldif(ExportData* data, gpointer user_data) {
	GString* export;
	gchar* text = NULL;
	GError* error = NULL;
	MenuItem* item = (MenuItem *) user_data;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;
	
	if (! data)
		return;
		
	debug_print("LDIF Export\n");

	/* get file name for write, get abook, get contact list,
	 * convert to ldif, save text to file*/
	if (data->filename) {
		if (item->parent && strcmp("tools", item->parent) == 0) {
			view = GTK_TREE_VIEW(item->mainwindow->abook_list);
			abook = get_selected_address_book(view);
			if (abook == NULL) {
				iter = set_selection_combobox(item->mainwindow->window,
								_("[New Contact] Choose address book"),
								gtk_tree_view_get_model(view),
								BOOK_NAME_COLUMN);
				if (! iter) {
					export_data_free(&data);
					return;
				}
				row = gtk_tree_view_get_selection(view);
				gtk_tree_selection_select_iter(row, iter);
				g_free(iter);
			}
		}

		text = get_contacts_as_ldif(item, data);
		export = g_string_new(LDIF_Version);
		export = g_string_append(export, "\n");
		export = g_string_append(export, text);
		g_free(text);
		
		text = g_string_free(export, FALSE);
		if (! g_file_set_contents(data->filename, text, -1, &error)) {
			show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_ERROR, error->message);
			g_clear_error(&error);
		}
		else {
			show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_INFO,
				_("Data successfully exported as LDIF to file\n%s"),
				data->filename);
		}
		export_data_free(&data);
		g_free(text);
	}
}

static void ldif_export(GtkWidget* widget, gpointer data) {
	MenuItem* item = (MenuItem *) data;
	
	debug_print("LDIF Export\n");
	show_export_wizard(item, make_ldif);
}

static void setup(const MainWindow* mainwindow, gpointer object) {
	GtkWidget *menu;
	MenuItem* menu_item;
	
	menu = gtk_image_menu_item_new_with_mnemonic("_LDIF");
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_MAIN_MENU;
    menu_item->parent = _("tools");
	menu_item->sublabel = _("_Export");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(ldif_export), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

	menu = gtk_image_menu_item_new_with_mnemonic(_("_LDIF"));
	gtk_widget_set_name(menu, "contextmenu");
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_ADDRESSBOOK_MENU;
    menu_item->sublabel = _("_Export");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(ldif_export), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);
}

/**
 * The main application will call this function after loading the
 * extension providing a uniq id for the extension which is to be
 * used for further references
 * @param id uniq id provided by main application
 * @return 0 if success 1 otherwise
 */
gint extension_init(guint id) {
	my_id = id;
	gchar* error = NULL;
	
	register_hook_function(my_id, EXTENSION_AFTER_INIT_HOOK, setup, &error);
	
	if (error) {
		show_message(NULL, GTK_UTIL_MESSAGE_ERROR, error);
		return 1;
	}
	
	return 0;
}

/**
 * Called by main application when the extension should be unloaded
 * @return TRUE if success FALSE otherwise
 */
gboolean extension_done(void) {
	gchar* error;
	
	unregister_hook_function(my_id, EXTENSION_BEFORE_OPEN_ABOOK_HOOK, &error);
	
	return TRUE;
}

/**
 * Called by main application to ensure extension license is compatible
 * @return license
 */
const gchar* extension_license(void) {
	return "GPL3+";
}

/**
 * Called by main application to get name of extension
 * @return name
 */
const gchar* extension_name(void) {
	return NAME;
}

/**
 * Called by main application to get extension's describtion
 * @return description
 */
const gchar* extension_describtion(void) {
	return _("Export an address book in LDIF format");
}
