AC_PREREQ(2.59)
AC_INIT(src/claws-contacts.c)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AC_COPYRIGHT([Copyright (c) 2011 Michael Rasmussen.])
PACKAGE=claws-contacts

dnl version number
MAJOR_VERSION=0
MINOR_VERSION=6
MICRO_VERSION=1
INTERFACE_AGE=0
BINARY_AGE=0
EXTRA_VERSION=0
EXTRA_RELEASE=
EXTRA_GTK2_VERSION=

if test \( $EXTRA_VERSION -eq 0 \) -o \( "x$EXTRA_RELEASE" != "x" \); then
    VERSION=${MAJOR_VERSION}.${MINOR_VERSION}.${MICRO_VERSION}${EXTRA_RELEASE}${EXTRA_GTK2_VERSION}
else
    VERSION=${MAJOR_VERSION}.${MINOR_VERSION}.${MICRO_VERSION}git${EXTRA_VERSION}${EXTRA_GTK2_VERSION}
fi

dnl set $target
dnl AC_CANONICAL_HOST

AM_INIT_AUTOMAKE($PACKAGE, $VERSION, [NO-DEFINE])
AC_DEFINE_UNQUOTED(PACKAGE, "$PACKAGE", [Name of package])
AC_DEFINE_UNQUOTED(VERSION, "$VERSION", [Version number of package])
AC_SUBST(PACKAGE)
AC_SUBST(VERSION)
AC_SUBST(MAJOR_VERSION)
AC_SUBST(MINOR_VERSION)
AC_SUBST(MICRO_VERSION)
AC_SUBST(EXTRA_VERSION)

AC_CONFIG_HEADER([config.h])
AM_MAINTAINER_MODE

if test "x$prefix" = "xNONE"; then
	prefix=$ac_default_prefix
fi
if test "x$enable-maintainer-mode" = "x"; then
    CFLAGS="-g -Wall -O -O2 -Wno-unused-variable -Wno-uninitialized"
else
    CFLAGS="-g -Wall"
fi
BINDIR="${prefix}/bin"
AC_DEFINE_UNQUOTED(BINDIR, "$BINDIR", [Where is binary installed])
AC_DEFINE_UNQUOTED(PIXDIR, "${prefix}/share/claws-contacts/pixmaps", [Directory with various pixmaps])
PLUGINDIR="${prefix}/lib/claws-contacts/plugins"
CLAWS_CONTACTS_EXTENSIONDIR="${prefix}/lib/claws-contacts/extensions"
AC_DEFINE_UNQUOTED(CLAWS_CONTACTS_EXTENSIONDIR, "$CLAWS_CONTACTS_EXTENSIONDIR", [Where is extensions located])
AC_DEFINE_UNQUOTED(EXTENSIONSUBDIR, 0, [is extensions located in subdirs])
CLAWS_CONTACTS_CFLAGS="-I${ac_pwd}/src -I${ac_pwd}/src/dbus"

AC_SUBST(CLAWS_CONTACTS_CFLAGS)
AC_SUBST(PLUGINDIR)
AC_SUBST(CLAWS_CONTACTS_EXTENSIONDIR)
AC_SUBST(BINDIR)

GLIB_REQUIRED=2.16.0
GOBJECT_REQUIRED=2.16.0
GTK_REQUIRED=2.16.0

AC_SUBST(GLIB_REQUIRED)
AC_SUBST(GOBJECT_REQUIRED)
AC_SUBST(GTK_REQUIRED)

# Checks for programs.
AC_PROG_CC
AC_PROG_LIBTOOL
PKG_PROG_PKG_CONFIG

# Checks for libraries.
PKG_CHECK_MODULES([GLIB],
                  [glib-2.0 >= $GLIB_REQUIRED \
                   gobject-2.0 >= $GOBJECT_REQUIRED \
                   gmodule-2.0 \
                   dbus-1 \
                   dbus-glib-1]
)
AC_SUBST(GLIB_CFLAGS)
AC_SUBST(GLIB_LIBS)

PKG_CHECK_MODULES([GTK], gtk+-2.0 >= $GTK_REQUIRED)
AC_SUBST(GTK_CFLAGS)
AC_SUBST(GTK_LIBS)

PKG_CHECK_MODULES([LIBXML], libxml-2.0)
AC_SUBST(LIBXML_CFLAGS)
AC_SUBST(LIBXML_LIBS)

AC_CHECK_LIB([m],[cos])

dnl for gettext
ALL_LINGUAS=""
GETTEXT_PACKAGE=claws-contacts
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Define text domain.])

AM_GNU_GETTEXT_VERSION([0.18])
AM_GNU_GETTEXT([external])

# Checks for header files.
AC_CHECK_HEADERS([cxxabi.h execinfo.h],
		 [AC_DEFINE(HAVE_BACKTRACE, 1,
		 [Defines if your system have the cxxabi.h and execinfo.h header files for backtrace()])]
		 ,)

AC_CHECK_HEADER(gcrypt.h,
		[AC_CHECK_LIB(gcrypt, gcry_control,,
		 AC_MSG_ERROR([The found libgcrypt is not functional.
			       Get latest version here:
			       http://www.gnupg.org/download]))],
		[AC_MSG_ERROR([libgcrypt not found.
			       Get latest version here:
			       http://www.gnupg.org/download])])
AM_PATH_LIBGCRYPT(1.4.5,,
    AC_MSG_ERROR([[
***
*** libgcrypt was not found. You may want to get it from
*** ftp://ftp.gnupg.org/pub/gcrypt/alpha/libgcrypt/
***
]]))

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST

dnl check if compiler pre-processor defines __FUNCTION__
AC_MSG_CHECKING([if __FUNCTION__ is defined])
AC_PREPROC_IFELSE([AC_LANG_PROGRAM(
[],
[char* cp =  __FUNCTION__])],
[
 AC_MSG_RESULT([yes])
 ac_cv_HAVE_FUNCTION_DEFINED=yes
],
[
 AC_MSG_RESULT([no])
 ac_cv_HAVE_FUNCTION_DEFINED=no
])

if test "x$ac_cv_HAVE_FUNCTION_DEFINED" = "xyes"; then
    AC_DEFINE(HAVE_FUNCTION_DEFINED, [1], [Does current compiler support __FUNCTION__])
fi

# Checks for library functions.

dnl --- Enable debug mode ---
AC_MSG_CHECKING([whether to enable debug mode])
AC_ARG_ENABLE(debug,
	[AC_HELP_STRING([--enable-debug],[enable debug mode [default=no]])],
	[ac_cv_enable_debug=$enableval], [ac_cv_enable_debug=no])
if test x"$ac_cv_enable_debug" = xyes -o test $DEBUG_MODE = 1; then
	AC_MSG_RESULT(yes)
	DEBUG_MODE=TRUE
else
	AC_MSG_RESULT(no)
	DEBUG_MODE=FALSE
fi
AC_DEFINE_UNQUOTED(DEBUG, $DEBUG_MODE, [Activate debug mode in source])

EXTENSIONS="ldifexport ldifimport vcard"
dnl --- Example extension ---
AC_MSG_CHECKING([whether to build example extension])
AC_ARG_ENABLE(example-extension,
	[AC_HELP_STRING([--enable-example-extension],[build example extension [default=no]])],
	[ac_cv_enable_example_extension=$enableval], [ac_cv_enable_example_extension=no])
if test x"$ac_cv_enable_example_extension" = xyes; then
	AC_MSG_RESULT(yes)
	AC_CONFIG_SUBDIRS(extensions/example)
else
	AC_MSG_RESULT(no)
fi
AM_CONDITIONAL(BUILD_EXAMPLE_EXTENSION, test x"$ac_cv_enable_example_extension" = xyes)

PLUGINS=""
dnl --- Example plugin ---
AC_MSG_CHECKING([whether to build example plugin])
AC_ARG_ENABLE(example-plugin,
	[AC_HELP_STRING([--enable-example-plugin],[build example plugin [default=no]])],
	[ac_cv_enable_example_plugin=$enableval], [ac_cv_enable_example_plugin=no])
if test x"$ac_cv_enable_example_plugin" = xyes; then
	AC_MSG_RESULT(yes)
	AC_CONFIG_SUBDIRS(plugins/example)
else
	AC_MSG_RESULT(no)
fi
AM_CONDITIONAL(BUILD_EXAMPLE_PLUGIN, test x"$ac_cv_enable_example_plugin" = xyes)

dnl --- XML plugin ---
AC_MSG_CHECKING([whether to build xml plugin])
AC_ARG_ENABLE(xml-plugin,
	[AC_HELP_STRING([--disable-xml-plugin],[build default claws-mail addressbook plugin [default=yes]])],
	[ac_cv_enable_xml_plugin=$enableval], [ac_cv_enable_xml_plugin=yes])
if test x"$ac_cv_enable_xml_plugin" = xyes; then
	AC_MSG_RESULT(yes)
	AC_CHECK_PROG(ac_cv_have_yacc, yacc, yes, no)
	if test x"$ac_cv_have_yacc" = xyes; then
	    PLUGINS="xml $PLUGINS"
	    AC_DEFINE(BUILD_XML_PLUGIN, 1, [Is the xml plugin built])
	else
	    ac_cv_enable_xml_plugin="no"
	    AC_MSG_ERROR("Executable 'yacc' was not found. Install package byacc or disable building of the XML plugin")
	fi
else
	AC_MSG_RESULT(no)
fi
AM_CONDITIONAL(BUILD_XML_PLUGIN, test x"$ac_cv_enable_xml_plugin" = xyes)

dnl --- LDAP plugin ---
AC_MSG_CHECKING([whether to build ldap plugin])
AC_ARG_ENABLE(ldap-plugin,
	[AC_HELP_STRING([--enable-ldap-plugin],[build ldap addressbook plugin [default=yes]])],
	[ac_cv_enable_ldap_plugin=$enableval], [ac_cv_enable_ldap_plugin=yes])
if test x"$ac_cv_enable_ldap_plugin" = xyes; then
	AC_MSG_RESULT(yes)
	AC_CHECK_LIB(resolv, res_query, LDAP_LIBS="$LDAP_LIBS -lresolv")
	AC_CHECK_LIB(socket, bind, LDAP_LIBS="$LDAP_LIBS -lsocket")
	AC_CHECK_LIB(nsl, gethostbyaddr, LDAP_LIBS="$LDAP_LIBS -lnsl")
	AC_CHECK_LIB(lber, ber_get_tag, LDAP_LIBS="$LDAP_LIBS -llber",,
				 $LDAP_LIBS)

	AC_CHECK_HEADERS(ldap.h lber.h,
					 [ ac_cv_enable_ldap=yes ],
					 [ ac_cv_enable_ldap=no ])

	if test "$ac_cv_enable_ldap" = yes; then
			AC_CHECK_LIB(ldap, ldap_open,
						 [ ac_cv_enable_ldap=yes ],
						 [ ac_cv_enable_ldap=no ],
						 $LDAP_LIBS)

			AC_CHECK_LIB(ldap, ldap_start_tls_s,
						 [ ac_cv_have_tls=yes ],
						 [ ac_cv_have_tls=no ])
	fi

	if test "$ac_cv_enable_ldap" = yes; then
		LDAP_LIBS="$LDAP_LIBS -lldap"
		PLUGINS="ldap $PLUGINS"
		AC_DEFINE(BUILD_LDAP_PLUGIN, 1, [Is the ldap plugin built])
	else
		AC_MSG_ERROR([Open LDAP is required. See http://www.openldap.org/])
	fi

	if test "$ac_cv_have_tls" = yes; then
		AC_MSG_CHECKING([Whether GNUTLS is present])
		PKG_CHECK_MODULES([GNUTLS], gnutls,
						  AC_DEFINE(LDAP_TLS, [],
						  [Is GNUTLS present on the system]))
		AC_SUBST(GNUTLS_LIBS)
		AC_SUBST(GNUTLS_CFLAGS)
	fi

	AC_SUBST(LDAP_LIBS)
else
	AC_MSG_RESULT(no)
fi
AM_CONDITIONAL(BUILD_LDAP_PLUGIN, test x"$ac_cv_enable_ldap_plugin" = xyes)

DBUS_SERVICE_DIR="${datadir}/dbus-1/services"
AC_SUBST(DBUS_SERVICE_DIR)
dnl AC_DEFINE_UNQUOTED(DBUS_SERVICE_DIR, $DBUS_SERVICE_DIR, [Service dir for DBUS])

dnl --- DBus test client ---
AC_MSG_CHECKING([whether to build DBus test client])
AC_ARG_ENABLE(dbus-client,
	[AC_HELP_STRING([--enable-dbus-client],[build DBus test client [default=no]])],
	[ac_cv_enable_dbus_client=$enableval], [ac_cv_enable_dbus_client=no])
if test x"$ac_cv_enable_dbus_client" = xyes; then
	AC_MSG_RESULT(yes)
else
	AC_MSG_RESULT(no)
fi
AM_CONDITIONAL(BUILD_DBUS_CLIENT, test x"$ac_cv_enable_dbus_client" = xyes)

AC_CONFIG_FILES([
    Makefile
    claws-contacts.pc
    claws-contacts.desktop
    m4/Makefile
    po/Makefile.in
    config/Makefile
    pixmaps/Makefile
    src/Makefile
    src/dbus/Makefile
    dbus-client/Makefile
    xmllib/Makefile
    extensions/Makefile
    extensions/export/Makefile
    extensions/import/Makefile
    extensions/example/Makefile
    extensions/example/src/Makefile
    extensions/vcard/Makefile
    extensions/vcard/src/Makefile
    libversit/Makefile
    plugins/Makefile
    plugins/xml/Makefile
    plugins/ldap/Makefile
    plugins/example/Makefile
    plugins/example/src/Makefile
])

AC_OUTPUT
echo -e "
$PACKAGE $VERSION

    Configuration:

        Source code:\t${srcdir}
        Compiler:\t${CC} $($CC -dumpversion)
        CFLAGS:\t\t${CFLAGS}
        Plugins:\t${PLUGINS}
        Plugin dir:\t${PLUGINDIR}
        Extensions:\t${EXTENSIONS}
	Extension dir:\t${CLAWS_CONTACTS_EXTENSIONDIR}
        Debug mode:\t${DEBUG_MODE}
        DBus test client:\t${ac_cv_enable_dbus_client}

        The binary will be installed in ${BINDIR}

        Configure finished, type 'make' to build.
"
