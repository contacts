2013-01-28 [mir]	0.6.0cvs93

	* libversit/vcard-utils.c
	    Fix some compile time warnings

2013-01-28 [mir]	0.6.0cvs92

	* extensions/vcard/src/vcard-extension.c
	    Fix some memory allocation errors.

2013-01-27 [mir]	0.6.0cvs91

	* extensions/vcard/src/vcard-extension.c
	    Improve importer to be able to handle
	    files with lines wrapped after 72 chars.

2013-01-27 [mir]	0.6.0cvs90

	* extensions/vcard/src/vcard-extension.c
	* libversit/vcard-utils.c
	* libversit/vcard-utils.h
	* src/extension-loader.h
	    Fix some compile time warnings
	    Remove result popup from import if
	    vcf file cannot be parsed

2012-10-03 [mir]	0.6.0cvs89

	* libversit/vcard-utils.c
	    Support for email alias when importing
	    and exporting contacts.

2012-08-07 [mir]	0.6.0cvs88

	* libversit/vcard-utils.c
	    Fix parsing of attribute N (VCNameProp)


2012-07-03 [mir]	0.6.0cvs87

	* libversit/vcard-utils.c
	    - Fix bug. Do not use properties 'G' and 'F'
	    since they are not defined in the vCard 2.1
	    specification. Use 'N' instead.
	    - Added details 'WORK,HOME,INTERNET' to email property.
	    Details are taken from email alias -> work, home and
	    everything else is labeled 'INTERNET'.
	    - Fix some compile time warnings.

2012-07-03 [mir]	0.6.0cvs86

	* libversit/vobject.c
	    Fix bug to prevent extra ':' when writing
	    property N to file.

2012-06-15 [ticho]	0.6.0cvs85

	* src/contactwindow.c
	* src/mainwindow.c
		Allow resizing of columns in both mainwindow and contact
		window.

2012-04-07 [mir]	0.6.0cvs84

	* plugins/xml/xml-plugin.c
	    If no old address books are found stop
	    import process immediately.

2012-04-01 [colin]	0.6.0cvs83

	* configure.ac
	* src/callbacks.c
		Make things easier for new users migrating from old 
		Claws Mail's addressbook : build (and load by default)
		both XML and LDAP plugins

2012-03-31 [mir]	0.6.0cvs82

	* src/dbus/dbus-service.c
	    Don't freopen in dbus service fork since this will
	    brake the print preview functionality in
	    gtk_print_operation_run.

2012-03-29 [mir]	0.6.0cvs81

	* README
	* TODO
	    - Update TODO
	    - Added CVS info and project home to README

2012-03-25 [mir]	0.6.0cvs80

	* extensions/vcard/src/vcard-extension.c
	* libversit/Makefile.am
	* libversit/vcard-utils.c
	* libversit/vcard-utils.h
	* src/utils.c
	* src/utils.h
	* src/dbus/Makefile.am
	* src/dbus/claws-contacts.xml
	* src/dbus/server-object.c
	* src/dbus/server-object.h
	* xmllib/parser.c
	A bunch of changes and enhancements. Also added feature
	to allow users to create a vCard for each account which
	is to be used for optionally added a vCard to every
	send email.

2012-03-25 [mir]	0.6.0cvs79

	* src/callbacks.c
	* src/contactwindow.c
	Fix key-event handler so default handling is applied
	if search entry has focus.
	Refactored advanced search dialog.

2012-01-05 [mir]	0.6.0cvs78

	* xmllib/Makefile.am
	    Fix typo.

2012-01-05 [mir]	0.6.0cvs77

	* Makefile.am
	* extensions/vcard/src/Makefile.am
	* extensions/vcard/src/vcard-utils.c
	* extensions/vcard/src/vcard-utils.h
	* libversit/Makefile.am
	* libversit/vcard-utils.c
	* libversit/vcard-utils.h
	    move libversit and vcard-utils to project
	    wide library to be able to add vcard features
	    to claws-mail.

2012-01-05 [mir]	0.6.0cvs76

	* extensions/vcard/src/vcard-extension.c
	    Fix likely dereferencing of NULL pointer

2012-01-05 [mir]	0.6.0cvs75

	* libversit/.cvsignore
	    Forgot this

2012-01-05 [mir]	0.6.0cvs74

	* libversit/Makefile.am
	* libversit/README.TXT
	* libversit/port.h
	* libversit/vcc.y
	* libversit/vobject.c
	* libversit/vobject.h
	    Project wide lib now.

2012-01-05 [mir]	0.6.0cvs73

	* extensions/vcard/src/libversit/.cvsignore
	* extensions/vcard/src/libversit/Makefile.am
	* extensions/vcard/src/libversit/README.TXT
	* extensions/vcard/src/libversit/port.h
	* extensions/vcard/src/libversit/vcc.y
	* extensions/vcard/src/libversit/vobject.c
	* extensions/vcard/src/libversit/vobject.h
	    Moving to a project wide library

2012-01-03 [mir]	0.6.0cvs72

	* extensions/vcard/src/vcard-extension.c
	* extensions/vcard/src/vcard-utils.c
	    Avoid very unlikely dereferencing a null-pointer.

2012-01-01 [mir]	0.6.0cvs71

	* extensions/vcard/src/vcard-utils.c
	    Added debug info.
	    Remove dead code.

2012-01-01 [mir]	0.6.0cvs70

	* extensions/vcard/src/vcard-extension.c
	* extensions/vcard/src/vcard-utils.c
	* extensions/vcard/src/vcard-utils.h
	    Finished implementation of vCard import.
	    Completed extension vCard.

2012-01-01 [mir]	0.6.0cvs69

	* extensions/import/ldifimport_extension.c
	    Fix a potential memory leak.
	    Added error widget.

2012-01-01 [mir]	0.6.0cvs68

	* extensions/vcard/src/vcard-utils.c
	    Fix memory leak
	    silence extension when not i debug mode

2012-01-01 [mir]	0.6.0cvs67

	* extensions/vcard/src/vcard-extension.c
	* extensions/vcard/src/vcard-utils.c
	* extensions/vcard/src/libversit/.cvsignore
	    Finish vCard import extension.
	    Fix bug when importing binary encoded data.

2012-01-01 [mir]	0.6.0cvs66

	* src/extension-loader.c
	    Fix bug when inserting menu_items in
	    main menu.

2011-12-31 [mir]	0.6.0cvs65

	* extensions/Makefile.am
	* extensions/vcard/src/Makefile.am
	    Fix minor bug in the build system which
	    prevented proper build.

2011-12-31 [mir]	0.6.0cvs64

	* extensions/vcard/.cvsignore
	* extensions/vcard/AUTHORS
	* extensions/vcard/ChangeLog
	* extensions/vcard/Makefile.am
	* extensions/vcard/NEWS
	* extensions/vcard/README
	* extensions/vcard/TODO
	* extensions/vcard/configure.ac
	* extensions/vcard/src/.cvsignore
	* extensions/vcard/src/Makefile.am
	* extensions/vcard/src/vcard-extension.c
	* extensions/vcard/src/vcard-utils.c
	* extensions/vcard/src/vcard-utils.h
	* extensions/vcard/src/libversit/.cvsignore
	* extensions/vcard/src/libversit/Makefile.am
	* extensions/vcard/src/libversit/README.TXT
	* extensions/vcard/src/libversit/port.h
	* extensions/vcard/src/libversit/vcc.y
	* extensions/vcard/src/libversit/vobject.c
	* extensions/vcard/src/libversit/vobject.h
	    Initial upload of extension to support
	    import and export for contacts in vCard format.

2011-12-31 [mir]	0.6.0cvs63

	* extensions/example/src/example-extension.c
	* extensions/export/ldifexport_extension.c
	* extensions/export/wizard.c
	* extensions/export/wizard.h
	* extensions/import/ldifimport_extension.c
	* src/extension.h
	    Necessary changes to be able to support more
	    possibilities for making extensions.

2011-12-31 [mir]	0.6.0cvs62

	* src/callbacks.c
	* src/callbacks.h
	* src/gtk-utils.c
	* src/gtk-utils.h
	    Moved some convinience functions from callbacks
	    to gtk-utils.

2011-12-31 [mir]	0.6.0cvs61

	* src/mainwindow.c
	    Notify user about load problems with extensions

2011-12-31 [mir]	0.6.0cvs60

	* plugins/xml/xml-plugin.c
	    Fix bug which prevented saving more than
	    one email address for each contact.

2011-12-28 [mir]	0.6.0cvs59

	* src/about.c
	* src/extension-loader.c
	* src/extension-loader.h
	    Fix bug when inserting a submenu into a context menu
	    A cosmetic change in about.c

2011-12-23 [mir]	0.6.0cvs58

	* plugins/ldap/ldap-plugin.c
	    Fix similar bug as 0.6.0cvs57-1.

2011-12-23 [mir]	0.6.0cvs57

	* plugins/ldap/ldap-plugin.c
	    Fixed two bugs.
	    1) Missing the default dn if connection was reopened
	    2) Possible dereferencing a NULL pointer if 'error'
	       parameter was NULL.

2011-12-21 [mir]	0.6.0cvs56

	* src/callbacks.c
	* src/contactwindow.c
	* src/extension-loader.c
	* src/gtk-utils.c
	* src/plugin-loader.c
	* src/printing.c
	* src/utils.c
	* xmllib/parser.c
	    Improve public functions error resistens.

2011-12-18 [mir]	0.6.0cvs55

	* src/contactwindow.c
	    Implemented GTK standard shortcut key Esc for
	    closing GtkDialog in contactwindow. 

2011-12-18 [mir]	0.6.0cvs54

	* src/callbacks.c
	* src/callbacks.h
	* src/mainwindow.c
	    Implemented keyboard shortcuts on the mainwindon
	    for deleting and displaying/editing contacts.
	    (s: show/edit contact. Delete: delete contact)
	    Fix a minor memory leak.

2011-12-17 [mir]	0.6.0cvs53

	* extensions/import/ldifimport_parser.c
	* extensions/import/ldifimport_parser.h
	    If file to read from is given as filename
	    then the extension must open file for read
	    in which case the extension will also be
	    responsible for closing the file handle.

2011-12-17 [mir]	0.6.0cvs52

	* extensions/import/ldifimport_extension.c
	    Forgot to implement file chooser dialog
	    to get name of file to import contacts from.

2011-12-17 [mir]	0.6.0cvs51

	* plugins/xml/xml-plugin.c
	    Fix memory leak and NULL pointer dereferencing

2011-12-17 [mir]	0.6.0cvs50

	* extensions/import/Makefile.am
	* extensions/import/ldifimport_extension.c
	* extensions/import/ldifimport_parser.c
	* extensions/import/ldifimport_parser.h
	    Completed the LDIF import extension.

2011-12-17 [mir]	0.6.0cvs49

	* extensions/export/ldifexport_extension.c
	    Removed function since it is now present in utils.

2011-12-17 [mir]	0.6.0cvs48

	* plugins/ldap/ldap-plugin.c
	* src/utils.c
	* src/utils.h
	    More user friendly error message when adding
	    and updating contacts.
	    Move some useful methods to utils.{c,h}.

2011-12-14 [mir]	0.6.0cvs47

	* extensions/example/src/example-extension.c
	* src/callbacks.c
	* src/extension-loader.c
	* src/extension-loader.h
	    Added feature that enables to insert new
	    items into submenus in the context menu.
	    See example-extension.c for details.

2011-12-13 [mir]	0.6.0cvs46

	* src/extension-loader.c
	    Fix bug. Forgot to build path to extensions.

2011-12-13 [mir]	0.6.0cvs45

	* extensions/export/ldifexport_extension.c
	* extensions/export/wizard.c
	* src/extension-loader.c
	    Making some type casting to shutup lame
	    compiler warnings.

2011-12-13 [mir]	0.6.0cvs44

	* extensions/export/Makefile.am
	* extensions/export/ldifexport_extension.c
	* extensions/export/wizard.c
	* extensions/export/wizard.h
	* src/gtk-utils.c
	    Completed implementation of LDIF export.
	    Compatibility tested and found working
	    100% against Jxplorer-3.2.2rc1.

2011-12-13 [mir]	0.6.0cvs43

	* src/utils.c
	* src/utils.h
	    Remove commented out code which is
	    no longer needed.

2011-12-13 [mir]	0.6.0cvs42

	* extensions/example/src/example-extension.c
	    Improve example for adding new menu items.

2011-12-13 [mir]	0.6.0cvs41

	* src/extension-loader.c
	    Fix minor bug.

2011-12-13 [mir]	0.6.0cvs40

	* plugins/ldap/ldap-plugin.c
	* src/utils.c
	* src/utils.h
	    Fix error when saving images as binary data.
	    RFC 4510 was somewhat vague in defining the
	    storage format for binary image data which
	    I wrongly interpreted in that way that binary
	    image data was to be stored base64 encoded.
	    Infact LDAP expects the raw image data and
	    will internally do the base64 encoding when
	    appropriate.

2011-12-09 [mir]	0.6.0cvs39

	* src/mainwindow.c
	    Forgot to comment out debug info

2011-12-09 [mir]	0.6.0cvs38

	* claws-contacts.pc.in
	* configure.ac
	* extensions/Makefile.am
	* extensions/example/Makefile.am
	* extensions/example/configure.ac
	* extensions/example/config/Makefile.am
	* extensions/example/src/Makefile.am
	* extensions/example/src/example-extension.c
	* extensions/export/ldifexport_extension.c
	* plugins/Makefile.am
	* plugins/example/Makefile.am
	* plugins/example/configure.ac
	* src/Makefile.am
	* src/callbacks.c
	* src/contactwindow.c
	* src/extension-loader.c
	* src/extension-loader.h
	* src/extension.c
	* src/extension.h
	* src/gtk-utils.c
	* src/mainwindow.c
	* src/mainwindow.h
	    Fix a number of bugs
	    Refined the build system
	    Added a plugable extension framework


2011-12-03 [mir]	0.6.0cvs37

	* plugins/example/.cvsignore
	* plugins/example/src/.cvsignore
	* extensions/.cvsignore
	* extensions/example/.cvsignore
	* extensions/example/config/.cvsignore
	* extensions/example/src/.cvsignore
	    required cvsignore files

2011-12-03 [mir]	0.6.0cvs36

	* plugins/example/AUTHORS
	* plugins/example/ChangeLog
	* plugins/example/Makefile.am
	* plugins/example/NEWS
	* plugins/example/README
	* plugins/example/TODO
	* plugins/example/configure.ac
	* plugins/example/example-plugin.c
	* plugins/example/src/Makefile.am
	* plugins/example/src/example-plugin.c
	* extensions/Makefile.am
	* extensions/example/AUTHORS
	* extensions/example/ChangeLog
	* extensions/example/Makefile.am
	* extensions/example/NEWS
	* extensions/example/README
	* extensions/example/TODO
	* extensions/example/configure.ac
	* extensions/example/config/Makefile.am
	* extensions/example/src/Makefile.am
	* extensions/example/src/example-extension.c
	* extensions/export/Makefile.am
	* extensions/export/ldifexport_extension.c
	* extensions/import/Makefile.am
	    Refactored extensions and plugins.

2011-12-03 [mir]	0.6.0cvs35

	* commitHelper
	    Due to changed configure.ac

2011-11-30 [mir]	0.6.0cvs34

	* Makefile.am
	* TODO
	* configure.ac
	* extensions/.cvsignore
	* extensions/Makefile.am
	* extensions/export/.cvsignore
	* extensions/export/Makefile.am
	* extensions/export/ldifexport_extension.c
	* extensions/import/.cvsignore
	* extensions/import/Makefile.am
	* extensions/import/ldifimport_extension.c
	* src/Makefile.am
	* src/claws-contacts.c
	* src/extension-loader.c
	* src/extension-loader.h
	* src/extension.c
	* src/extension.h
	* src/mainwindow.c
	* src/mainwindow.h
	    Begin implementing plugable extension feature.

2011-11-30 [mir]	0.6.0cvs33

	* src/contactwindow.c
	    remove an unneeded g_object_unref.

2011-11-30 [mir]	0.6.0cvs32

	* plugins/ldap/ldap-plugin.c
	* src/dbus/dbus-service.c
	    Ensure LDAP connection is always closed after
	    every call since concurrent connections can
	    give TLS problems.

2011-11-29 [mir]	0.6.0cvs31

	* src/utils.c
	* src/dbus/dbus-contact.h
	* src/dbus/server-object.c
	    Upgrade dbus part to handle attrib_def structure.

2011-11-28 [mir]	0.6.0cvs30

	* configure.ac
	* plugins/ldap/ldap-plugin.c
	* plugins/xml/xml-plugin.c
	* src/callbacks.c
	* src/contactwindow.c
	* src/printing.c
	* src/utils.c
	* src/utils.h
	* xmllib/parser.c
	    - code cleaning to loose compile time
	      warnings and optimizer errors.

2011-11-28 [mir]	0.6.0cvs29

	* configure.ac
	* plugins/ldap/ldap-plugin.c
	* src/about.c
	* src/callbacks.c
	* src/contactwindow.c
	    Mostly code cleaning.
	    - Fix one memory leak.
	    - Fix building on systems with GTK+2 < 2.24

2011-11-28 [mir]	0.6.0cvs28

	* plugins/ldap/ldap-plugin.c
	* src/callbacks.c
	* src/contactwindow.c
	* src/printing.c
	* src/utils.c
	* src/utils.h
	    -Fix lots of bugs
	    -Improfements
	    -Refactoring
	    -And last: Complete LDAP plugin with full support
	     for write/update. Testing is adviceable.

2011-11-26 [mir]	0.6.0cvs27

	* src/contactwindow.c
	    - Refactored implementation so that updates
	      are only committed when user requests to
	      save changes. Eliminates server round trips
	      and avoid saving incomplete contact information.

2011-11-26 [mir]	0.6.0cvs26

	* src/callbacks.c
	    - Fix memory leak
	    - Remove commented out code


2011-11-22 [mir]	0.6.0cvs25

	* plugins/ldap/ldap-plugin.c
	* plugins/xml/xml-plugin.c
	* src/callbacks.c
	* src/contactwindow.c
	    Fixing small bug in xml-plugin.
	    Fixing small bug in callbacks and contactwindow.
	    Preparing ldap-plugin for update support.

2011-11-20 [mir]	0.6.0cvs24

	* plugins/ldap/ldap-plugin.c
	    Forgot to free LDAPMessage in case of en error.
	    More clever handling of AbookConnection structure
	    when writing new configuration.

2011-11-20 [mir]	0.6.0cvs23

	* src/callbacks.c
	    Fix bug: Used a copy instead of a reference to address
		book which should be edited.
	    Fix bug: Forgot to clear list of contacts in copy of
		address book which is used for editing - edit config
		is likely to change the cached list of contacts.


2011-11-20 [mir]	0.6.0cvs22

	* plugins/example/example-plugin.c
	* plugins/ldap/ldap-plugin.c
	* plugins/xml/xml-plugin.c
	* src/callbacks.c
	* src/callbacks.h
	* src/contactwindow.c
	* src/mainwindow.h
	* src/plugin-loader.c
	* src/plugin-loader.h
	* src/plugin.h
	* src/utils.c
	* src/utils.h
	    Lots of bug fixes, some feature enhancements and complete
	    read-only support for LDAP with advanced search
	    capabilities.

2011-11-14 [mir]	0.6.0cvs21

	* plugins/ldap/ldap-plugin.c
	    Forgot to use commitHelper for privous commit:-)

2011-10-04 [mir]	0.6.0cvs20

	* dbus-client/client.c
	* plugins/example/example-plugin.c
	* plugins/ldap/ldap-plugin.c
	* plugins/xml/plugin-init.c
	* plugins/xml/xml-plugin.c
	* src/about.c
	* src/callbacks.c
	* src/claws-contacts.c
	* src/contactwindow.c
	* src/gtk-utils.c
	* src/mainwindow.c
	* src/plugin-loader.c
	* src/plugin-loader.h
	* src/plugin.h
	* src/printing.c
	* src/utils.c
	* src/utils.h
	* src/dbus/dbus-service.c
	* src/dbus/server-object.c
	* xmllib/parser.c
	* xmllib/parser.h
	    - Implemented forgotten functionality to reactivate closed
	    address books.
	    - Fix a ton of bugs.
	    - Extended plugin API so that it is possible to get a list
	      of closed address books as well as splitting attributes
	      in mandatory and optional.
	    - Provide plugins a way to have a fixed set of attributes.
	      Eg. LDAP has a finite set of attributes while XML supports
	      an infinite set of attributes.

2011-09-27 [mir]	0.6.0cvs19

	* plugins/example/example-plugin.c
	* plugins/ldap/ldap-plugin.c
	* plugins/xml/plugin-init.c
	* plugins/xml/xml-plugin.c
	* src/callbacks.c
	* src/gtk-utils.c
	* src/gtk-utils.h
	* src/plugin-loader.c
	* src/plugin.h
	* src/utils.c
	* src/utils.h
	    Made necessary changes to support plugins
	    which only support a limited set of attributes.
	    Eg. LDAP attributes are defined in schemas while
	    the XML plugin by nature has an infinite set
	    of supported attributes.

2011-09-20 [mir]	0.6.0cvs18

	* configure.ac
	    Fix small bug.

2011-09-20 [mir]	0.6.0cvs17

	* dbus-client/.cvsignore
	* dbus-client/client.c
	* plugins/xml/plugin-init.c
	* plugins/xml/xml-plugin.c
	* src/callbacks.c
	* src/contactwindow.c
	* src/gtk-utils.c
	* src/gtk-utils.h
	* src/plugin-loader.c
	* src/plugin-loader.h
	* src/plugin.h
	* src/printing.c
	* src/settings.c
	* src/utils.c
	* src/utils.h
	* src/dbus/server-object.c
	* xmllib/parser.c
	    Extended attribute support to use all available
	    supported attributes defined in AttribType.
	    Fix a lot of bugs and potential memory leaks.


2011-09-17 [mir]	0.6.0cvs16

	* xmllib/parser.c
	    Fix a segfault.
	    Added feature to fetch name for address book
	    from file provided the user has not entered
	    on in the widget.

2011-09-15 [mir]	0.6.0cvs15

	* plugins/ldap/.cvsignore
	    Missing this i previous commit.

2011-09-15 [mir]	0.6.0cvs14

	* plugins/Makefile.am
	* plugins/ldap/Makefile.am
	* plugins/ldap/ldap-plugin.c
	    Added framework for ldap plugin.

2011-09-15 [mir]	0.6.0cvs13

	* xmllib/parser.c
	    Fix a potential memory leak.
	    Adjusted gslist_free to use revised function.

2011-09-11 [mir]	0.6.0cvs12

	* plugins/xml/xml-plugin.c
	* src/plugin-loader.c
	* src/plugin-loader.h
	* src/plugin.h
	    Fix typo.

2011-09-11 [mir]	0.6.0cvs11

	* plugins/example/example-plugin.c
	    Added documentation for functions and variables.

2011-09-10 [mir]	0.6.0cvs10

	* src/plugin-loader.c
	    Fix memory leak

2011-09-08 [mir]	0.6.0cvs9

	* src/callbacks.c
	* src/mainwindow.c
	    Implement clear button in advanced search mode
	    too.
	    Added tooltip to clear button.

2011-09-08 [mir]	0.6.0cvs8

	* .cvsignore
	    Added one more ignore file

2011-09-08 [mir]	0.6.0cvs7

	* autogen.sh
	* configure.ac
	    Fix type.
	    Ensure config dir exists

2011-09-07 [mir]	0.6.0cvs6

	* .cvsignore
	    One more file to ignore

2011-09-07 [mir]	0.6.0cvs5

	* .cvsignore
	* dbus-client/.cvsignore
	* pixmaps/.cvsignore
	* plugins/.cvsignore
	* plugins/example/.cvsignore
	* plugins/xml/.cvsignore
	* src/.cvsignore
	* src/dbus/.cvsignore
	* xmllib/.cvsignore
	    Added cvsignore files to reduce noise

2011-09-07 [mir]	0.6.0cvs4

	* Makefile.am
	    Applied extra automake options

2011-09-07 [mir]	0.6.0cvs3

	* commitHelper
	    Line number changed for VERSION string

2011-09-07 [mir]	0.6.0cvs2

	* AUTHORS
	* ChangeLog
	* Makefile.am
	* NEWS
	* TODO
	* autogen.sh
	* claws-contacts.desktop.in
	* claws-contacts.pc.in
	* configure.ac
	* org.clawsmail.ClawsContact.service.in
	* dbus-client/Makefile.am
	* dbus-client/client.c
	* pixmaps/Makefile.am
	* pixmaps/anonymous.xpm
	* pixmaps/claws-contacts_128x128.png
	* pixmaps/claws-contacts_16x16.png
	* pixmaps/claws-contacts_32x32.png
	* pixmaps/claws-contacts_48x48.png
	* pixmaps/claws-contacts_64x64.png
	* pixmaps/icon-copyright
	* plugins/Makefile.am
	* plugins/example/Makefile.am
	* plugins/example/example-plugin.c
	* plugins/xml/Makefile.am
	* plugins/xml/plugin-init.c
	* plugins/xml/plugin-init.h
	* plugins/xml/xml-plugin.c
	* src/Makefile.am
	* src/about.c
	* src/about.h
	* src/callbacks.c
	* src/callbacks.h
	* src/claws-contacts.c
	* src/contactwindow.c
	* src/contactwindow.h
	* src/gtk-utils.c
	* src/gtk-utils.h
	* src/mainwindow.c
	* src/mainwindow.h
	* src/plugin-loader.c
	* src/plugin-loader.h
	* src/plugin.h
	* src/printing.c
	* src/printing.h
	* src/settings.c
	* src/settings.h
	* src/utils.c
	* src/utils.h
	* src/dbus/Makefile.am
	* src/dbus/claws-contacts.xml
	* src/dbus/dbus-contact.h
	* src/dbus/dbus-service.c
	* src/dbus/dbus-service.h
	* src/dbus/server-object.c
	* src/dbus/server-object.h
	* xmllib/Makefile.am
	* xmllib/parser.c
	* xmllib/parser.h
	    Initial upload of the new address book

2011-09-07 [colin]	0.0.0cvs1

	* README
	* configure.ac
	* commitHelper
		Contacts module init.

# $Id$
claws-contacts (0.1.6)    
	* Patch provided by Colin to fix some build
	  problems
	  
-- Michael Rasmussen <mir@datanom.net> Wed, 31 Aug 2011 18:21:21 +0100

claws-contacts (0.1.5)
        * version.sh
	    Added version system
	    Integration with claws-mail

-- Michael Rasmussen <mir@datanom.net> Sun, 03 Jul 2011 19:44:21 +010