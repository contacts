/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib-bindings.h>
#include "server-object.h"
#include "client-bindings.h"
#include "utils.h"
#include "dbus-contact.h"

typedef struct {
    gchar* alias;
    gchar* email;
    gchar* name;
    gchar* comment;
} ContactInfo;

static gchar* search = NULL;
static gchar* addrbook = NULL;
static gchar* emailaddr = NULL;
static gchar* bookname = NULL;
static gchar* comment = NULL;
static gchar* alias = NULL;
static gboolean compose_mode = FALSE;

static GOptionEntry entries[] = {
    {"search", 's', 0, G_OPTION_ARG_STRING, &search, "Search param", "param"},
    {"book", 'b', 0, G_OPTION_ARG_STRING, &addrbook, "Address Book", "book"},
    {"email", 'e', 0, G_OPTION_ARG_STRING, &emailaddr, "Email address", "email"},
    {"name", 'n', 0, G_OPTION_ARG_STRING, &bookname, "First and second name", "\"firstname secondname\""},
    {"comment", 'c', 0, G_OPTION_ARG_STRING, &comment, "Comment", "comment"},
    {"alias", 'a', 0, G_OPTION_ARG_STRING, &alias, "Alias", "alias"},
    {"signal", 'g', 0, G_OPTION_ARG_NONE, &compose_mode, "Compose mode", NULL},
    {NULL}
};

static gboolean parse_cmdline(int* argc, char*** argv) {
    GError* error = NULL;
    GOptionContext* context;
    gboolean res = FALSE;

    context = g_option_context_new("<ping|commit|find|insert|show|list>");
    g_option_context_add_main_entries(context, entries, PACKAGE_STRING);
    g_option_context_set_help_enabled(context, TRUE);
    if (! g_option_context_parse(context, argc, argv, &error)) {
        g_print("Parsing options failed: %s\n", error->message);
        g_clear_error(&error);
        res = TRUE;
    }
    g_option_context_free(context);
    return res;
}

static void contact_info_free(ContactInfo* c) {
    g_free(c->alias);
    g_free(c->email);
    g_free(c->name);
    g_free(c->comment);
}

static gchar* convert_2_utf8(gchar* locale) {
    gsize read, write;
    GError* error = NULL;
    gchar *current, *utf8;
    const gchar* charset;

    if (g_get_charset(&charset) || g_utf8_validate(locale, -1, 0))
        return g_strdup(locale);

    if (strcmp("ANSI_X3.4-1968", charset) == 0)
        current = g_strdup("ISO-8859-1");
    else
        current = g_strdup(charset);

    utf8 = g_convert(locale, -1, "UTF-8", current, &read, &write, &error);
    if (error) {
        g_printerr("Fail to convert [%s]: %s\n", charset, error->message);
        g_free(current);
        g_clear_error(&error);
        exit(EXIT_FAILURE);
    }
    g_free(current);

    return utf8;
}

static void format_contact(DBusContact* contact, ContactInfo* c) {
    gchar* firstname;
    gchar* lastname;
    GValueArray* email = NULL;
    GValue email_member = {0};
    gchar* str;   
    
    contact->data = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
    contact->emails = g_value_email_new();
    firstname = lastname = NULL;
    
    if (c->name) {
        gchar* pos = strchr(c->name, ' ');
        if (pos) {
            firstname = g_strndup(c->name, pos - c->name);
            lastname = g_strdup(++pos);
            g_hash_table_replace(contact->data,
                g_strdup("first-name"), convert_2_utf8(firstname));
            g_hash_table_replace(contact->data,
                g_strdup("last-name"), convert_2_utf8(lastname));
        }
        else {
            lastname = g_strdup(c->name);
            g_hash_table_replace(contact->data,
                g_strdup("last-name"), convert_2_utf8(lastname));
        }
        g_free(firstname);
        g_free(lastname);
    }

    email = g_value_array_new(0);

    if (c->alias)
        str = convert_2_utf8(c->alias);
    else
        str = g_strdup("");

    g_value_init(&email_member, G_TYPE_STRING);
    g_value_set_string(&email_member, str);
    g_value_array_append(email, &email_member);
    g_value_unset(&email_member);
    g_free(str);

    if (c->email)
        str = convert_2_utf8(c->email);
    else
        str = g_strdup("");

    g_value_init(&email_member, G_TYPE_STRING);
    g_value_set_string(&email_member, str);
    g_value_array_append(email, &email_member);
    g_value_unset(&email_member);
    g_free(str);

    if (c->comment)
        str = convert_2_utf8(c->comment);
    else
        str = g_strdup("");
    
    g_value_init(&email_member, G_TYPE_STRING);
    g_value_set_string(&email_member, str);
    g_value_array_append(email, &email_member);
    g_value_unset(&email_member);
    g_free(str);

    g_ptr_array_add(contact->emails, email);
    dbus_contact_print(contact, stderr);
}

static void free_ptr_array(gchar** array) {
    g_strfreev(array);
}

static void dbus_contact_free(const DBusContact* contact) {
    g_hash_table_destroy(contact->data);
    g_ptr_array_free(contact->emails, TRUE);
}

int main(int argc, char** argv) {
    DBusGProxy *proxy;
    DBusGConnection *connection;
    GError *error = NULL;
    ContactInfo* contact_info;
    gchar **msg, **test = NULL;
    DBusContact contact;
        
    gchar* reply;
    gboolean succes;
    guint count;

    if (parse_cmdline(&argc, &argv))
        return 1;

    if (argc < 2) {
        g_printerr("Missing action parameter\n");
        return 2;
    }

    if (argc > 2) {
        g_printerr("Can only do one action at the time\n");
        return 3;
    }

    g_type_init ();

    connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
    if (connection == NULL || error) {
        g_warning("Unable to connect to dbus: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    }
    
    proxy = dbus_g_proxy_new_for_name (connection,
            "org.clawsmail.Contacts",
            "/org/clawsmail/contacts",
            "org.clawsmail.Contacts");
    if (proxy == NULL) {
        g_warning("Could not get a proxy object\n");
        return 4;
    }

    if (strcmp(argv[1], "show") == 0) {
        if (!org_clawsmail_Contacts_show_addressbook(proxy, compose_mode, &error)) {
            g_warning("Woops remote method failed: %s", error->message);
            g_clear_error(&error);
            return 12;
        }
    }
    else if (strcmp(argv[1], "ping") == 0) {
        if (!org_clawsmail_Contacts_ping(proxy, &reply, &error)) {
            g_warning("Woops remote method failed: %s", error->message);
            g_clear_error(&error);
            free_ptr_array(test);
            return 5;
        }
        g_print("%s\n", reply);
        g_free(reply);
    }
    else if (strcmp(argv[1], "commit") == 0) {
        if (! addrbook) {
            g_printerr("Missing required parameter [book]\n");
            return 6;
        }
        if (!org_clawsmail_Contacts_commit(proxy, addrbook, &succes, &error)) {
            g_warning("Woops remote method failed: %s", error->message);
            g_clear_error(&error);
            free_ptr_array(test);
            return 7;
        }
        if (succes)
            g_print("Changes committed\n");
        else
            g_print("Changes not committed\n");
    }
    else if (strcmp(argv[1], "find") == 0) {
        if (! search) {
            g_printerr("Missing required parameter [search]\n");
            return 8;
        }
        if (!org_clawsmail_Contacts_search_addressbook(
                proxy, search, addrbook, &test, &error)) {
            g_warning("Woops remote method failed: %s", error->message);
            g_clear_error(&error);
            free_ptr_array(test);
            return 9;
        }
        for (msg = test, count = 0; *msg; msg += 1, count++) {
            g_print("%s\n", *msg);
        }
        g_print("Found %d contacts matching %s\n", count, search);
        free_ptr_array(test);
    }
    else if (strcmp(argv[1], "insert") == 0) {
        contact_info = g_new0(ContactInfo, 1);
        contact_info->email = emailaddr;
        contact_info->name = bookname;
        contact_info->comment = comment;
        contact_info->alias = alias;
        format_contact(&contact, contact_info);
        contact_info_free(contact_info);
        g_free(contact_info);
        if (!org_clawsmail_Contacts_add_contact(
            proxy, addrbook, contact.data, contact.emails, &error)) {
            g_warning("Woops remote method failed: %s", error->message);
            g_clear_error(&error);
            dbus_contact_free(&contact);
            return 10;
        }
        dbus_contact_free(&contact);
    }
    else if (strcmp(argv[1], "list") == 0) {
        if (!org_clawsmail_Contacts_book_list(proxy, &test, &error)) {
            g_warning("Woops remote method failed: %s", error->message);
            g_clear_error(&error);
            free_ptr_array(test);
            return 13;
        }
        for (msg = test, count = 0; *msg; msg += 1, count++) {
            g_print("%s\n", *msg);
        }
        g_print("Found %d address books\n", count);
        free_ptr_array(test);
    }
    else {
        g_printerr("%s: Unknown action\n", argv[1]);
        return 11;
    }

    g_object_unref(proxy);
    g_free(search);
    g_free(addrbook);
    g_free(emailaddr);
    g_free(bookname);
    g_free(comment);
    g_free(alias);
    return 0;
}
