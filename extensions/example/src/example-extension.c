/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <gtk/gtk.h>
#include "extension.h"
#include "utils.h"
#include "gtk-utils.h"

#define NAME "Example extension"

static guint my_id;

static void my_cb(GtkWidget* widget, gpointer data) {
	MenuItem* item = (MenuItem *) data;
	
	show_message(item->mainwindow->window, GTK_UTIL_MESSAGE_INFO, _("test"));
}

static void setup(const MainWindow* mainwindow, gpointer object) {
	GtkWidget *menu;
	MenuItem* menu_item;
	
	// Add a menu item into the file menu
	menu = gtk_image_menu_item_new_with_mnemonic(_("_Test2"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_MAIN_MENU;
    menu_item->parent = _("file");
    menu_item->submenu = FALSE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(my_cb), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

	// Add a menu item into the context menu of contact
	menu = gtk_image_menu_item_new_with_mnemonic(_("_Test3"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_CONTACT_MENU;
    menu_item->submenu = FALSE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(my_cb), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

	// Add a menu item into the context menu of address book
	// Will create another submenu since ldifimport also has
	// a menu item under '_Import'
	menu = gtk_image_menu_item_new_with_mnemonic(_("_Test"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_ADDRESSBOOK_MENU;
    menu_item->sublabel = _("_Import");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(my_cb), (gpointer) menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

	// Add a menu item into the menu bar
	menu = gtk_image_menu_item_new_with_mnemonic(_("_Zest2"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_MAIN_MENU;
    menu_item->parent = _("_Menu");
    menu_item->submenu = FALSE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(my_cb), menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);

	// Add another menu item into the menu bar. This time as a submenu
	menu = gtk_image_menu_item_new_with_mnemonic(_("_Yest1"));
    menu_item = menu_item_new();
    menu_item->menu = CONTACTS_MAIN_MENU;
    menu_item->parent = _("_Menu");
    menu_item->sublabel = _("_Test1");
    menu_item->submenu = TRUE;
	menu_item->mainwindow = mainwindow;
    g_signal_connect(menu, "activate",
            G_CALLBACK(my_cb), menu_item);
	add_menu_item(GTK_IMAGE_MENU_ITEM(menu), menu_item);
}

/**
 * The main application will call this function after loading the
 * extension providing a uniq id for the extension which is to be
 * used for further references
 * @param id uniq id provided by main application
 * @return 0 if success 1 otherwise
 */
gint extension_init(guint id) {
	my_id = id;
	gchar* error = NULL;
	
	register_hook_function(my_id, EXTENSION_AFTER_INIT_HOOK, setup, &error);
	return 0;
}

/**
 * Called by main application when the extension should be unloaded
 * @return TRUE if success FALSE otherwise
 */
gboolean extension_done(void) {
	return TRUE;
}

/**
 * Called by main application to ensure extension license is compatible
 * @return license
 */
const gchar* extension_license(void) {
	return "GPL3+";
}

/**
 * Called by main application to get name of extension
 * @return name
 */
const gchar* extension_name(void) {
	return NAME;
}

/**
 * Called by main application to get extension's describtion
 * @return description
 */
const gchar* extension_describtion(void) {
	return _("Example Claws-contacts extension");
}
