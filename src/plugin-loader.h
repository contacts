/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __PLUGIN_LOADER_H__
#define __PLUGIN_LOADER_H__

#include <glib.h>

G_BEGIN_DECLS

#include <gtk/gtk.h>
#include "plugin.h"
#include "mainwindow.h"

typedef struct {
	gchar* filename;
	gchar* id;
	//gchar* addrbook_name;
	GModule	*module;
	gboolean (*init) (gpointer self, gchar** error);
	void (*reset) (gchar** error);
	const gchar* (*name) (void);
	const gchar* (*desc) (void);
	const gchar* (*version) (void);
	PluginType (*type) (void);
	const gchar* (*license) (void);
	const gchar* (*file_filter) (void);
	gboolean (*need_credentials) (void);
	GSList* (*extra_config) (void);
	gchar* (*default_url) (const gchar* name);
	GSList* (*remaining_attribs) (void);
	GSList* (*inactive_attribs) (void);
	PluginFeature* (*provides) (void);
	GSList* (*attrib_list) (void);
	void (*attribs_set) (GHashTable* attributes);
	GSList* (*addrbook_all_get) (void);
	GList* (*closed_books_get) (void);
	GSList* (*addrbook_names_all) (void);
	gboolean (*abook_open) (AddressBook* abook, gchar** error);
	gboolean (*abook_close) (AddressBook* abook, gchar** error);
	gboolean (*abook_delete) (AddressBook* abook, gchar** error);
	gboolean (*abook_set_config) (AddressBook* old, AddressBook* new, gchar** error);
	gboolean (*abooks_commit_all) (gchar** error);

	/* If plugin is read-only */
	GSList* (*get_contact) (AddressBook* abook,
							const gchar* search_token,
							gchar** error);
	
	/* If plugin is read-write */
	gboolean (*set_contact) (AddressBook* abook,
							 const Contact* contact,
							 gchar** error);
	gboolean (*delete_contact) (AddressBook* abook,
								const Contact* contact,
								gchar** error);
	gboolean (*update_contact) (AddressBook* abook,
								const Contact* contact,
								gchar** error);
	
	/* If plugin supports advanced search */
	GSList* (*search_contact) (AddressBook* abook,
							   const Contact* search_tokens,
							   gboolean is_and,
							   gchar** error);
	
	gboolean readonly;
	gboolean can_adv_search;
	/*GSList* rdeps;*/
	gchar* error;
} Plugin;

typedef struct {
	Plugin* plugin;
	GSList* abooks;
} Reference;

void plugin_dialog(MainWindow* mainwindow);
Plugin* plugin_load(const gchar* filename, gchar** error);
void plugin_unload(Plugin* plugin);
void plugin_load_all(GtkWidget* parent, GSList* list);
void plugin_unload_all();
Plugin* plugin_get_plugin(const gchar* name);
GSList* plugin_get_path_all();
GSList* plugin_get_name_all();
GSList* address_books_get();
Reference* address_book_reference_get(const gchar* name);

G_END_DECLS

#endif
