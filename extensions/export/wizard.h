/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef __WIZARD_H__
#define __WIZARD_H__

#include <glib.h>

G_BEGIN_DECLS

#include <glib/gi18n.h>
#include <gtk/gtk.h>

typedef struct {
	gchar*		filename;
	gchar*		suffix;
	gchar*		rdn;
	gboolean	use_dn;
} ExportData;

extern const gchar* RDN[];

typedef void (*WIZARD_CALLBACK) (ExportData* data, gpointer user_data);

void show_export_wizard(gpointer menu_item, WIZARD_CALLBACK callback);
void export_data_free(ExportData** data);

G_END_DECLS

#endif
