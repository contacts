/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "plugin-init.h"
#include "plugin.h"
#include "gtk-utils.h"
#include "utils.h"

static const gchar addr_file[] =  "addrbook--index.xml";
static const gchar addr_dir[] =  ".claws-mail/addrbook";
static gchar* path = NULL;

static GSList* standard_attr_list() {
	GSList* list = NULL;
	AttribDef* attr;
	gchar** standard = (gchar **) standard_attribs;

	while (*standard) {
		/*g_ptr_array_add(array, g_strdup(*standard++));*/
		attr = g_new0(AttribDef, 1);
		attr->type = ATTRIB_TYPE_STRING;
		attr->attrib_name = g_strdup(*standard++);
		list = g_slist_prepend(list, attr);
	}
	
	return list;
}

static GSList* scan_existing_addrbooks(const gchar* path, gchar** error) {
/*	GPtrArray* array;*/
	GSList* list = NULL;
	AttribDef* attr;
	gchar* attrib_file;
	gchar* contents = NULL;
	GError* err = NULL; 
	
	attrib_file = g_strconcat(path, G_DIR_SEPARATOR_S, "attributesrc", NULL);
	/*array = g_ptr_array_new_with_free_func(g_free);*/
	
	g_file_get_contents(attrib_file, &contents, NULL, &err);
	if (err) {
		*error = g_strdup(err->message);
		g_clear_error(&err);
	}
	else {
		if (strlen(contents) > 0) {
			gchar** standard = g_strsplit(contents, "\n", 0);
			gchar** tmp = standard;
			while(*tmp) {
				gchar* s = *tmp++;
				if (strlen(s) > 0) {
					/*g_ptr_array_add(array, g_strdup(s));*/
					attr = g_new0(AttribDef, 1);
					attr->type = ATTRIB_TYPE_STRING;
					attr->attrib_name = g_strdup(s);
					list = g_slist_prepend(list, attr);
				}
			}
			g_strfreev(standard);
		}
		g_free(contents);
	}
	/*g_ptr_array_add(array, NULL);*/
	
	g_free(attrib_file);
	
	return list /*(gchar **) g_ptr_array_free(array, FALSE)*/;
}

static gboolean has_old_claws_native_abook(gchar** path, gchar** error) {
	const gchar* homedir = get_home();
	gchar* tmp = NULL;
	gboolean result = FALSE;
	
	if (! homedir)
		return result;
		
	*path = g_strconcat(homedir, G_DIR_SEPARATOR_S, addr_dir, NULL);
	tmp = g_strconcat(*path, G_DIR_SEPARATOR_S, addr_file, NULL);
	if (g_file_test(tmp, G_FILE_TEST_EXISTS))
		result = TRUE;
	else {
		g_free(*path);
		*path = NULL;
	}
	g_free(tmp);
	
	return result;
}

#define native_header "Displayed attributes are found\nby scanning existing address books"
#define standard_header "Displayed attributes are\ndefaults for address books"
static GHashTable* get_attributes(
		const gchar* path, gboolean old_abook, gchar** error) {
	const gchar* header;
	AttribContainer* attribs;
	GHashTable* result;

	attribs = g_new0(AttribContainer, 1);
	if (old_abook) {
		header = native_header;
		attribs->existing_attribs = standard_attr_list();
		attribs->existing_attribs = g_slist_concat(
			attribs->existing_attribs,
			scan_existing_addrbooks(path, error));
		/*attribs->existing_attribs = (const gchar**) scanned_attribs;*/
	}
	else {
		header = standard_header;
		attribs->existing_attribs = standard_attr_list();
	}
	
	result = get_attrib_list(NULL, attribs, header, TRUE, error, add_attrib_btn_cb);
	gslist_free(&attribs->existing_attribs, attrib_def_free);
	g_free(attribs);
	
	return result;
}

GHashTable* plugin_get_attribs(gboolean* old_abook, gchar** error) {
	GHashTable* hash_table;
	
	if (*old_abook == TRUE) {
		*old_abook = has_old_claws_native_abook(&path, error);
		if (*error) {
			return NULL;
		}
	}
	
	hash_table = get_attributes(path, *old_abook, error);
	
	return hash_table;
}

const gchar* old_abook_index_file() {
	return addr_file;
}

const gchar* old_abook_addr_dir() {
	return (const gchar *) path;
}

void old_abook_free_dir() {
	g_free(path);
}
	
