#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ(2.59)
AC_INIT(src/vcard-extension.c)
AC_CONFIG_AUX_DIR(config)
AM_MAINTAINER_MODE

PACKAGE=vcard_extension

dnl extension version

MAJOR_VERSION=0
MINOR_VERSION=1
MICRO_VERSION=1
EXTRA_VERSION=0

if test \( $EXTRA_VERSION -eq 0 \); then
    if test \( $MICRO_VERSION -eq 0 \); then
        VERSION=${MAJOR_VERSION}.${MINOR_VERSION} 
    else
        VERSION=${MAJOR_VERSION}.${MINOR_VERSION}.${MICRO_VERSION}
    fi
else
    if test \( $MICRO_VERSION -eq 0 \); then
        VERSION=${MAJOR_VERSION}.${MINOR_VERSION}cvs${EXTRA_VERSION}
    else
        VERSION=${MAJOR_VERSION}.${MINOR_VERSION}.${MICRO_VERSION}cvs${EXTRA_VERSION}
    fi
fi

AC_CONFIG_MACRO_DIR([m4])
AM_INIT_AUTOMAKE($PACKAGE, $VERSION, no-define)
AC_DEFINE_UNQUOTED(EXTENSIONVERSION, "$VERSION", [extension version])

AC_CONFIG_HEADERS([config.h])

# Checks for programs.
AC_PROG_CC
AM_PROG_CC_STDC
AC_LANG_C
AC_ISC_POSIX
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_PROG_CPP
AC_PROG_INSTALL
AC_PROG_LIBTOOL

# Checks for libraries.
dnl check if compiler pre-processor defines __FUNCTION__
AC_MSG_CHECKING([if __FUNCTION__ is defined])
AC_PREPROC_IFELSE([AC_LANG_PROGRAM(
[],
[char* cp =  __FUNCTION__])],
[
 AC_MSG_RESULT([yes])
 ac_cv_HAVE_FUNCTION_DEFINED=yes
],
[
 AC_MSG_RESULT([no])
 ac_cv_HAVE_FUNCTION_DEFINED=no
])

if test "x$ac_cv_HAVE_FUNCTION_DEFINED" = "xyes"; then
    AC_DEFINE(HAVE_FUNCTION_DEFINED, [1], [Does current compiler support __FUNCTION__])
fi

# Find pkg-config
AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
if test x$PKG_CONFIG = xno ; then
  AC_MSG_ERROR([*** pkg-config not found. See http://www.freedesktop.org/software/pkgconfig/])
fi

# Check for claws-contacts
if test "x$USE_MAINTAINER_MODE" = "xyes"; then
    CLAWS_CONTACTS_CFLAGS="-I ../../../src -I ../../../src/dbus"
    CLAWS_CONTACTS_LIBS=""
else
    PKG_CHECK_MODULES(CLAWS_CONTACTS, claws-contacts >= 0.6.0)
fi
AC_SUBST(CLAWS_CONTACTS_CFLAGS)
AC_SUBST(CLAWS_CONTACTS_LIBS)
if test -z $prefix || test "${prefix}" = "NONE" ; then
  prefix=$( $PKG_CONFIG --variable=prefix claws-contacts )
  CLAWS_CONTACTS_EXTENSIONDIR=$( $PKG_CONFIG --variable=extensiondir claws-contacts )
else
  CLAWS_CONTACTS_EXTENSIONDIR='${libdir}/claws-contacts/extensions'
fi
AC_SUBST(CLAWS_CONTACTS_EXTENSIONDIR)

if test $USE_MAINTAINER_MODE = yes; then
    CFLAGS="-g -Wall"
else
    CFLAGS="-g -O2 -Wall"
fi

#ALL_LINGUAS="ca cs es fi fr hu id it ja lt nl pt_BR ru sk uk"
#AC_DEFINE_UNQUOTED(TEXTDOMAIN, "$PACKAGE", [Gettext textdomain])
#AM_GNU_GETTEXT_VERSION([0.15])
#AM_GNU_GETTEXT([external])

PKG_CHECK_MODULES(GLIB, [glib-2.0 >= 2.16])
AC_SUBST(GLIB_CFLAGS)
AC_SUBST(GLIB_LIBS)

PKG_CHECK_MODULES(GTK, gtk+-2.0 >= 2.16)
AC_SUBST(GTK_CFLAGS)
AC_SUBST(GTK_LIBS)

# Checks for header files.
AC_HEADER_STDC

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_SIZE_T

# Checks for library functions.

# check the version of libarchive
AC_SUBST(VERSION)
AC_SUBST(EXTENSIONVERSION)
AC_SUBST(MAJOR_VERSION)
AC_SUBST(MINOR_VERSION)
AC_SUBST(MICRO_VERSION)
AC_SUBST(EXTRA_VERSION)

AC_CONFIG_COMMANDS(
    [summary],
    [[echo ""
    echo "${PACKAGE} will be compiled with these settings:"
    echo ""
    echo -e "CFLAGS: ${CFLAGS}"
    echo -e "Extension version: ${EXTENSION_VER}"
    echo ""
    echo -e "Now run make to build ${PACKAGE}"
    echo ""
    echo -e "Please send bugs or feature requests to the maintainer(s)."
    echo -e "Email addresses can be found in the AUTHORS file."
    echo ""]],
    [
     CFLAGS="$CFLAGS"
     PACKAGE="$PACKAGE"
     EXTENSION_VER="$VERSION"
    ]
)

AC_OUTPUT([
    Makefile
    src/Makefile
    src/libversit/Makefile
])

