/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdlib.h>
#include "mainwindow.h"
#include "callbacks.h"
#include "utils.h"
#include "gtk-utils.h"
#include "plugin-loader.h"
#include "parser.h"
#include "about.h"
#include "contactwindow.h"
#include "printing.h"
#include "dbus-service.h"
#include "settings.h"
#include "extension-loader.h"
#include "extension.h"

#define PLUGINGROUP "loaded plugins"

static ConfigFile* config = NULL;

struct DataContainer {
	Plugin* plugin;
	AddressBook** addressbook;
	GtkWidget* widget;
};

static gboolean update_progress_bar(gpointer data) {
	GtkWidget* progress_bar = (GtkWidget *) data;
	
	debug_print("Pulse progress bar\n");
	gtk_progress_bar_pulse(GTK_PROGRESS_BAR(progress_bar));
	while (gtk_events_pending())
		gtk_main_iteration();

	return TRUE;
}

static gboolean progress_map_event_cb(
			GtkWidget *widget, GdkEvent *event, gpointer data) {
	MainWindow* win = (MainWindow *) data;

	win->source_id = g_timeout_add(
			100, update_progress_bar, win->progress_bar);
	update_progress_bar(win->progress_bar);

	read_config(win);
    update_abook_list(win);
	
	return FALSE;
}

static void write_config() {
	GSList* plugins;
	gchar *configrc, *data;
	gchar** list;
	int num;
	GError* err = NULL;
	const gchar comment[] = "Configuration file for Claws-mail address book.\n"
							"Do not change unless you KNOW what your are doing.";
	FILE* fp;
	gsize len;
	
	gchar* home = get_self_home();
	configrc = g_strdup_printf("%s%s%src", home, G_DIR_SEPARATOR_S, PACKAGE);
	g_free(home);
	
	plugins = plugin_get_path_all();
	
	if (! config) {
		config = g_new0(ConfigFile, 1);
		config->path = g_strdup(configrc);
	}
	if (! config->key_file) {
		config->key_file = g_key_file_new();
		if (g_file_test(configrc, G_FILE_TEST_EXISTS)) {
			g_key_file_load_from_file(
				config->key_file, configrc, G_KEY_FILE_KEEP_COMMENTS, &err);
			if (err) {
				g_clear_error(&err);
				return;
			}
		}
	}
	
	g_key_file_set_comment(config->key_file, NULL, NULL, comment, NULL);

	list = gslist_to_array(plugins, &num);
	g_key_file_set_string_list(config->key_file, PLUGINGROUP,
			"plugins", (const gchar**) list, num);
	g_strfreev(list);
	gslist_free(&plugins, g_free);
	
	data = g_key_file_to_data(config->key_file, &len, &err);
	if (! err) {
		gchar* old = g_strconcat(configrc, ".bak", NULL);
		g_rename(configrc, old);
		g_free(old);
		fp = g_fopen(configrc, "w");
		fwrite(data, len, 1, fp);
		fclose(fp);
	}
	g_free(data);
	g_free(configrc);
	config_main_free(config);
	g_free(config);
}

void shutdown_application(gpointer data) {
	MainWindow* win = (MainWindow *) data;
	
	/* iterate over all address books determine whether the have unsaved data */
	debug_print("Persisting unsaved data\n");
	//save_unsaved_data(win->abook_list);
	
	debug_print("Updating configuration\n");
	write_config();
	
    debug_print("Unloading all plugins\n");
	plugin_unload_all();
	unload_extensions();
	
	if (win) {
    	debug_print("Closing application\n");
		gtk_widget_destroy(win->progress_dialog);
		g_free(win->default_book);
		g_free(win);
		gtk_main_quit();
	}
	if (mainloop) {
		if (g_main_loop_is_running(mainloop))
			g_main_loop_unref(mainloop);
	}
	if (DAEMON) {
		g_log_remove_handler(G_LOG_DOMAIN, log_handler_id);
		close_log_file();
		debug_print("DBusProxy [%p]\n", DBusProxy);
		g_object_unref(DBusProxy);
	}
}

static void select_addr_book_cb(GtkButton *button, gpointer data) {
	GtkWidget *dialog;
	gchar* file;
	struct DataContainer cur = *(struct DataContainer *) data;
	const gchar* file_filter;	
	GtkFileFilter* filter;
	gchar* home;
	
	file_filter = cur.plugin->file_filter();
	if (file_filter) {
		gchar* f = g_strconcat("*.", file_filter, NULL);
		filter = gtk_file_filter_new();
		gtk_file_filter_set_name(filter, f);
		gtk_file_filter_add_pattern(filter, f);
		g_free(f);
	}
	
	dialog = gtk_file_chooser_dialog_new("Select File",
					      NULL,
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					      NULL);
	if (file_filter)
		gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
	gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), FALSE);
	home = cur.plugin->default_url(NULL);
	if (home) {
		gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), home);
		g_free(home);
	}
	
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		if (file) {
			gtk_entry_set_text(GTK_ENTRY(cur.widget), file);
			g_free(file);
		}
	}

	gtk_widget_destroy(dialog);
}

static void show_popup_menu(GtkWidget* widget, GdkEventButton* event,
        gpointer data, gboolean new_row) {
    MainWindow* win = (MainWindow *) data;
    GtkTreeView* view;
    int button, event_time;
    GtkWidget *menu,
              *abook_new, *abook_open, 
              *abook_delete, *abook_close, *abook_edit,
              *contact_new, *contact_edit,
              *contact_print, *contact_delete;
    GtkAccelGroup* accel;
    const gchar* name = widget->name;
	GSList* context;
	
    debug_print("Popup called from %s\n", name);
    if (! (strcmp(name, "abook_list") == 0 ||
		strcmp(name, "contact_list") == 0))
        return;

    if (event) {
        button = event->button;
        event_time = event->time;
    }
    else {
        button = 0;
        event_time = gtk_get_current_event_time();
    }

    menu = gtk_menu_new();

    accel = g_object_new(GTK_TYPE_ACCEL_GROUP, NULL);
    gtk_window_add_accel_group(GTK_WINDOW(win->window), accel);

    if (strcmp(name, "abook_list") == 0) {
		if (new_row) {
			abook_new = gtk_image_menu_item_new_with_mnemonic("_New");
    		gtk_image_menu_item_set_accel_group(
					GTK_IMAGE_MENU_ITEM(abook_new), accel);
    		gtk_widget_add_accelerator(abook_new, "activate", accel,
            		GDK_N, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
			g_signal_connect(abook_new, "activate",
					G_CALLBACK(abook_new_cb), win);
			gtk_menu_shell_append(GTK_MENU_SHELL(menu), abook_new);

            abook_open = gtk_image_menu_item_new_with_mnemonic("_Open");
    		gtk_image_menu_item_set_accel_group(
					GTK_IMAGE_MENU_ITEM(abook_open), accel);
    		gtk_widget_add_accelerator(abook_open, "activate", accel,
            		GDK_O, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
           	g_signal_connect(abook_open, "activate",
                G_CALLBACK(abook_open_cb), win);
            gtk_menu_shell_append(GTK_MENU_SHELL(menu), abook_open);
		}
        else {
			view = GTK_TREE_VIEW(win->abook_list);
			win->selected_abook = get_selected_address_book(view);
			if (win->selected_abook == NULL) {
				g_object_unref(accel);
				gtk_widget_destroy(menu);
				return;
			}

    		abook_edit = gtk_image_menu_item_new_from_stock(
					GTK_STOCK_EDIT, accel);
    		gtk_widget_add_accelerator(abook_edit, "activate", accel,
            		GDK_E, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
            g_signal_connect(abook_edit, "activate",
                G_CALLBACK(abook_edit_cb), win);
            gtk_menu_shell_append(GTK_MENU_SHELL(menu), abook_edit);

            abook_close = gtk_image_menu_item_new_with_mnemonic("_Close");
    		gtk_image_menu_item_set_accel_group(
					GTK_IMAGE_MENU_ITEM(abook_close), accel);
    		gtk_widget_add_accelerator(abook_close, "activate", accel,
            		GDK_C, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
            g_signal_connect(abook_close, "activate",
                G_CALLBACK(abook_close_cb), win);
            gtk_menu_shell_append(GTK_MENU_SHELL(menu), abook_close);

            abook_delete = gtk_image_menu_item_new_from_stock(
					GTK_STOCK_DELETE, accel);
    		gtk_widget_add_accelerator(abook_delete, "activate", accel,
            		GDK_D, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
            g_signal_connect(abook_delete, "activate",
                G_CALLBACK(abook_delete_cb), win);
            gtk_menu_shell_append(GTK_MENU_SHELL(menu), abook_delete);
        }
		/* Insert requested menu_items from extensions */
		context = get_menu_items(CONTACTS_ADDRESSBOOK_MENU);
	    if (context) {
			GSList* cur = context;
			while (cur) {
				GtkWidget* widget = (GtkWidget *) cur->data;
				gtk_widget_unparent(widget);
				gtk_menu_shell_append(GTK_MENU_SHELL(menu),	g_object_ref(widget));
				cur = cur->next;
			}
		}
    }
    else if (strcmp(name, "contact_list") == 0) {
		if (new_row) {
	        contact_new = gtk_image_menu_item_new_with_mnemonic("_New");
    		gtk_image_menu_item_set_accel_group(
					GTK_IMAGE_MENU_ITEM(contact_new), accel);
    		gtk_widget_add_accelerator(contact_new, "activate", accel,
            		GDK_N, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
			g_signal_connect(contact_new, "activate",
					G_CALLBACK(contact_new_cb), win);
			gtk_menu_shell_append(GTK_MENU_SHELL(menu), contact_new);
		}
		else {
			view = GTK_TREE_VIEW(win->contact_list);
			win->selected_contact = get_selected_contact(view);
			if (win->selected_contact == NULL) {
				g_object_unref(accel);
				gtk_widget_destroy(menu);
				return;
			}

            contact_edit = gtk_image_menu_item_new_from_stock(
                GTK_STOCK_EDIT, accel);
    		gtk_widget_add_accelerator(contact_edit, "activate", accel,
            		GDK_E, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
            g_signal_connect(contact_edit, "activate",
                G_CALLBACK(contact_edit_cb), win);
            gtk_menu_shell_append(GTK_MENU_SHELL(menu), contact_edit);

            contact_print = gtk_image_menu_item_new_from_stock(
                GTK_STOCK_PRINT, accel);
    		gtk_widget_add_accelerator(contact_print, "activate", accel,
            		GDK_P, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
            g_signal_connect(contact_print, "activate",
                G_CALLBACK(contact_print_cb), win);
            gtk_menu_shell_append(GTK_MENU_SHELL(menu), contact_print);

            contact_delete = gtk_image_menu_item_new_from_stock(
                GTK_STOCK_DELETE, accel);
    		gtk_widget_add_accelerator(contact_delete, "activate", accel,
            		GDK_D, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
            g_signal_connect(contact_delete, "activate",
                G_CALLBACK(contact_delete_cb), win);
            gtk_menu_shell_append(GTK_MENU_SHELL(menu), contact_delete);
        }
		/* Insert requested menu_items from extensions */
		context = get_menu_items(CONTACTS_CONTACT_MENU);
	    if (context) {
			GSList* cur = context;
			while (cur) {
				GtkWidget* widget = (GtkWidget *) cur->data;
				gtk_widget_unparent(widget);
				gtk_menu_shell_append(GTK_MENU_SHELL(menu),	g_object_ref(widget));
				cur = cur->next;
			}
		}
    }
	else {
		return;
	}

    gtk_widget_show_all(menu);
    gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, button, event_time);
}

static void contact_set_attr(Contact* contact,
							 ContactColumn column,
							 const gchar* value) {
	Email* mail;
	AttribDef* attr = g_new0(AttribDef, 1);
	
	attr->type = ATTRIB_TYPE_STRING;
	attr->value.string = g_strdup(value);
	switch (column) {
		case CONTACT_DISPLAYNAME_COLUMN:
			attr->attrib_name = g_strdup("cn");
			g_hash_table_replace(contact->data,
					g_strdup("cn"), attr);
			break;
    	case CONTACT_FIRSTNAME_COLUMN:
    		attr->attrib_name = g_strdup("first-name");
			g_hash_table_replace(contact->data,
					g_strdup("first-name"), attr);
			break;
		case CONTACT_LASTNAME_COLUMN:
			attr->attrib_name = g_strdup("last-name");
			g_hash_table_replace(contact->data,
					g_strdup("last-name"), attr);
			break;
		case CONTACT_EMAIL_COLUMN:
			/* In default view only the first email will be used */
			if (contact->emails) {
				mail = (Email *) contact->emails->data;
				g_free(mail->email);
			}
			else {
				mail = g_new0(Email, 1);
				contact->emails = g_slist_prepend(contact->emails, mail);
			}
			mail->email = g_strdup(value);
			break;
		default:
			attrib_def_free(attr);
			break;
	}
}

void read_config(MainWindow* mainwindow) {
	gchar* configrc;
	GError* err = NULL;
	GSList *plugins = NULL, *book = NULL;
	GtkWidget* window = NULL;
	
	gchar* home = get_self_home();
	configrc = g_strdup_printf("%s%s%src", home, G_DIR_SEPARATOR_S, PACKAGE);
	g_free(home);

	config = g_new0(ConfigFile, 1);
	config->key_file = g_key_file_new();
	config->path = g_strdup(configrc);
	if (g_file_test(configrc, G_FILE_TEST_EXISTS)) {
		g_key_file_load_from_file(
			config->key_file, configrc, G_KEY_FILE_KEEP_COMMENTS, &err);
		if (err) {
			g_clear_error(&err);
		}
	}

	
	config_get_value(config, PLUGINGROUP, "plugins", &plugins);
	if (plugins == NULL) {
#ifdef BUILD_XML_PLUGIN
		plugins = g_slist_prepend(plugins,
				g_strconcat(PLUGINDIR, G_DIR_SEPARATOR_S,
					"xml-plugin.so", NULL));
#endif
#ifdef BUILD_LDAP_PLUGIN
		plugins = g_slist_prepend(plugins,
				g_strconcat(PLUGINDIR, G_DIR_SEPARATOR_S,
					"ldap-plugin.so", NULL));
#endif
	}

	if (mainwindow) {
		window = mainwindow->window; 
		config_get_value(config, "Settings", "default book", &book);
		if (book) {
			mainwindow->default_book = g_strdup((gchar *) book->data);
			if (mainwindow->default_book[strlen(mainwindow->default_book) - 1] == ';') {
				gchar* tmp = g_strndup(mainwindow->default_book,
						strlen(mainwindow->default_book) - 1);
				g_free(mainwindow->default_book);
				mainwindow->default_book = g_strdup(tmp);
				g_free(tmp);
			}
		}
		if (mainwindow->use_extensions) {
			debug_print("BEFORE_INIT_HOOK\n");
			run_hook_callbacks(EXTENSION_BEFORE_INIT_HOOK, NULL);
		}
	}	

	plugin_load_all(window, plugins);
	gslist_free(&plugins, g_free);
	gslist_free(&book, g_free);
	
	g_free(configrc);
	if (mainwindow) {
		destroy_progress_dialog(mainwindow);
		if (mainwindow->use_extensions) {
			debug_print("AFTER_INIT_HOOK\n");
			run_hook_callbacks(EXTENSION_AFTER_INIT_HOOK, NULL);
		}
	}
}

gchar* contact_write_to_backend(Plugin* plugin,
								AddressBook* abook,
								Contact* old,
								Contact* new) {
	gchar* error = NULL;
	
	cm_return_val_if_fail(plugin != NULL, _("Missing plugin"));
	cm_return_val_if_fail(abook != NULL, _("Missing address book"));

	if (! plugin->readonly) {
		if (! plugin->update_contact(abook, new, &error)) {
			abook->contacts = g_list_remove(abook->contacts, old);
			abook->contacts = g_list_prepend(abook->contacts, new);
		}
	}
	else {
		error = g_strdup(_("Plugin does not support updates"));
	}
	
	return error;
}

#define MISSING "Missing name"
void book_cell_data_format(GtkTreeViewColumn *col,
                           GtkCellRenderer   *renderer,
                           GtkTreeModel      *model,
                           GtkTreeIter       *iter,
                           gpointer           data) {
	gchar* name;

	cm_return_if_fail(renderer != NULL);
	
	gtk_tree_model_get(model, iter, BOOK_NAME_COLUMN, &name, -1);
	if (! name || strcmp(name, MISSING) == 0) {
		gchar* text = g_strconcat("<i>", MISSING, "</i>", NULL);
		g_object_set(renderer, "markup", text, NULL);
		g_object_set(renderer, "weight", PANGO_WEIGHT_NORMAL, NULL);
		g_free(text);
	}
	else {
		g_object_set(renderer, "weight", PANGO_WEIGHT_MEDIUM, NULL);
	}
	
	g_free(name);
}

gint delete_event(GtkWidget* widget, GdkEvent* event, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	shutdown_application(win);

    return TRUE;
}

void menu_quit_cb(GtkMenuItem *menuitem, gpointer data) {
	shutdown_application(data);
	while (gtk_events_pending())
  		gtk_main_iteration ();
  	gtk_main_quit();
  	exit(EXIT_SUCCESS);
}

void toolbar_quit_cb(GtkToolButton *toolbutton, gpointer data) {
	shutdown_application(data);
	while (gtk_events_pending())
  		gtk_main_iteration ();
  	gtk_main_quit();
  	exit(EXIT_SUCCESS);
}

void file_plugin_cb(GtkMenuItem *menuitem, gpointer data) {
    MainWindow* win = (MainWindow *) data;

    debug_print("Creating plugin window\n");

    plugin_dialog(win);
}

void abook_edit_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	AddressBook *abook = NULL, *new;
	Plugin* plugin = NULL;
	gchar *error = NULL;
	GSList *plugins;
	GList *closed = NULL, *cur_closed;
	GSList *open = NULL, *cur_open; 
	gboolean use_closed = FALSE;
	GSList* names = NULL;
	gchar* plugin_name = NULL;
	GtkTreeView* view;
	GtkTreeIter* iter;

	if (win->selected_abook) {
		abook = (AddressBook *) win->selected_abook;
		win->selected_abook = NULL;
		view = GTK_TREE_VIEW(win->abook_list);
		plugin = get_selected_plugin(view);
	}
	else {
		plugins = plugin_get_name_all();
		gboolean response = show_choice_list(win->window,
			"[Open book] Select plugin to use",
			plugins, &plugin_name);
		gslist_free(&plugins, NULL);

		if (response) {
			plugin = plugin_get_plugin(plugin_name);
			if (plugin) {
				closed = plugin->closed_books_get();
				if (closed) {
					use_closed = show_question(win->window,
						"Choose from list of previously closed books?");
				}
				if (use_closed) {
					for (cur_closed = closed; cur_closed; cur_closed = g_list_next(cur_closed)) {
						AddressBook* abook = (AddressBook *) cur_closed->data;
						names = g_slist_append(names, g_strdup(abook->abook_name));
					}
					if (names) {
						g_free(plugin_name);
						plugin_name = NULL;
						if (show_choice_list(win->window, "Choose address book",
								names, &plugin_name)) {
							gint index = gslist_get_index(
								names, plugin_name, gslist_compare_gchar);
							abook = address_book_copy(g_list_nth_data(closed, index), TRUE);
						}
						gslist_free(&names, g_free);
					}
					g_free(plugin_name);
				}
				else {
					open = plugin->addrbook_all_get();
					if (open) {
						for (cur_open = open; cur_open; cur_open = g_slist_next(cur_open)) {
							AddressBook* abook = (AddressBook *) cur_open->data;
							names = g_slist_append(names, g_strdup(abook->abook_name));
						}
						if (names) {
							g_free(plugin_name);
							plugin_name = NULL;
							if (show_choice_list(win->window, "Choose address book",
									names, &plugin_name)) {
								gint index = gslist_get_index(
									names, plugin_name, gslist_compare_gchar);
								abook = g_slist_nth_data(open, index);
							}
							gslist_free(&names, g_free);
						}
						g_free(plugin_name);
						gslist_free(&open, NULL);
					}
					else {
						show_message(win->window, GTK_UTIL_MESSAGE_INFO,
							_("No address book to edit"));
					}
				}
				if (closed) {
					for (cur_closed = closed; cur_closed; cur_closed = g_list_next(cur_closed)) {
						AddressBook* a = (AddressBook *) cur_closed->data;
						address_book_free(&a);
					}
					glist_free(&closed, NULL);
				}
			}
		}
		else {
			view = GTK_TREE_VIEW(win->abook_list);
			plugin = get_selected_plugin(view);
			iter = set_selection_combobox(win->window,
							"[Edit book] Choose address book",
							gtk_tree_view_get_model(view),
							BOOK_NAME_COLUMN);
			if (! iter)
				return;
			GtkTreeSelection* row = gtk_tree_view_get_selection(view);
			gtk_tree_selection_select_iter(row, iter);
			g_free(iter);
			abook = get_selected_address_book(view);
		}
	}
	if (abook && plugin) {
		if (win->use_extensions) {
			debug_print("BEFORE_EDIT_ABOOK_HOOK\n");
			run_hook_callbacks(EXTENSION_BEFORE_EDIT_ABOOK_HOOK, abook);
		}
		new = address_book_copy(abook, TRUE);
		address_book_contacts_free(new);
		if (address_book_edit(win->window, plugin, &new)) {
			if (! use_closed)
				plugin->abook_close(abook, &error);
			if (error) {
				show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
				g_free(error);
				address_book_free(&new);
				return;
			}
			plugin->abook_set_config(abook, new, &error);
			if (error) {
				show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
				g_free(error);
				address_book_free(&new);
				return;
			}
			plugin->abook_open(new, &error);
			if (error) {
				show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
				g_free(error);
				address_book_free(&new);
			}
			win->selected_abook = abook;
			update_abook_list(win);
			win->selected_abook = NULL;
			address_book_free(&abook);
			if (win->use_extensions) {
				debug_print("AFTER_EDIT_ABOOK_HOOK\n");
				run_hook_callbacks(EXTENSION_AFTER_EDIT_ABOOK_HOOK, abook);
			}
		}
		else {
			address_book_free(&new);
		}
	}
	else {
		show_message(win->window, GTK_UTIL_MESSAGE_INFO,
		_("Please highlight desired address book to edit"));
	}
}

void abook_delete_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	AddressBook* abook;
	Plugin* plugin;
	gchar *error = NULL;
	GtkTreeView* view;
	GtkTreeIter* iter;

	/* TODO: be able to also delete closed books */
	if (win->selected_abook) {
		abook = (AddressBook *) win->selected_abook;
		win->selected_abook = NULL;
	}
	else {
		view = GTK_TREE_VIEW(win->abook_list);
		iter = set_selection_combobox(win->window,
						"[Delete book] Choose address book",
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return;
		GtkTreeSelection* row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		abook = get_selected_address_book(view);
	}
	plugin = get_selected_plugin(GTK_TREE_VIEW(win->abook_list));
	if (abook && plugin) {
		if (win->use_extensions) {
			debug_print("BEFORE_DELETE_ABOOK_HOOK\n");
			run_hook_callbacks(EXTENSION_BEFORE_DELETE_ABOOK_HOOK, abook);
		}
		if (show_question(win->window, _("Remove '%s'?"), abook->abook_name)) {
			plugin->abook_delete(abook, &error);
			if (error) {
				show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
				g_free(error);
				return;
			}
			address_book_free(&abook);
			update_abook_list(win);
		}
		if (win->use_extensions) {
			debug_print("AFTER_DELETE_ABOOK_HOOK\n");
			run_hook_callbacks(EXTENSION_AFTER_DELETE_ABOOK_HOOK, abook);
		}
	}
	else {
		show_message(win->window, GTK_UTIL_MESSAGE_INFO,
		_("Please highlight desired address book for deletion"));
	}
}

void abook_open_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	GSList *plugins;
	Plugin* plugin = NULL;
	AddressBook *book = NULL, *tmp;
	gchar* plugin_name = NULL;
	gchar* error = NULL;
	gboolean use_closed = FALSE;
	GList* cur;
	GSList* names = NULL;
	GList* closed = NULL;

    debug_print("Creating address book window\n");
	
	plugins = plugin_get_name_all();
	gboolean response = show_choice_list(win->window,
			"[Open book] Select plugin to use",
			plugins, &plugin_name);
	gslist_free(&plugins, NULL);

	if (response) {
		plugin = plugin_get_plugin(plugin_name);
		if (plugin) {
			closed = plugin->closed_books_get();
			if (closed) {
				use_closed = show_question(win->window,
					"Choose from list of previously closed books?");
			}
			if (use_closed) {
				for (cur = closed; cur; cur = g_list_next(cur)) {
					AddressBook* abook = (AddressBook *) cur->data;
					names = g_slist_append(names, g_strdup(abook->abook_name));
				}
				if (names) {
					g_free(plugin_name);
					plugin_name = NULL;
					if (show_choice_list(win->window, "Choose address book",
							names, &plugin_name)) {
						gint index = gslist_get_index(
							names, plugin_name, gslist_compare_gchar);
						book = address_book_copy(g_list_nth_data(closed, index), TRUE);
					}
					gslist_free(&names, g_free);
				}
			}
			else {
				book = address_book_new();
				if (address_book_edit(win->window, plugin, &book)) {
					/* Empty name ensures the chosen name is used as name */
					tmp = address_book_new();
					tmp->URL = g_strdup(book->URL);
					plugin->abook_set_config(tmp, book, &error);
					if (error) {
						show_message(win->window, GTK_UTIL_MESSAGE_WARNING, "%s", error);
						g_free(error);
						address_book_free(&book);
					}
				}
				else
					address_book_free(&book);
			}
			if (book) {
				if (win->use_extensions) {
					debug_print("BEFORE_OPEN_ABOOK_HOOK\n");
	            	run_hook_callbacks(EXTENSION_BEFORE_OPEN_ABOOK_HOOK, book);
				}
				if (plugin->abook_open(book, &error)) 
					update_abook_list(win);
				if (error) {
					show_message(win->window, GTK_UTIL_MESSAGE_WARNING, "%s", error);
					g_free(error);
					address_book_free(&book);
				}
				if (win->use_extensions) {
					debug_print("AFTER_OPEN_ABOOK_HOOK\n");
	            	run_hook_callbacks(EXTENSION_AFTER_OPEN_ABOOK_HOOK, book);
				}
			}
			if (closed) {
				for (cur = closed; cur; cur = g_list_next(cur)) {
					AddressBook* a = (AddressBook *) cur->data;
					address_book_free(&a);
				}
				glist_free(&closed, NULL);
			}
		}
	}
	g_free(plugin_name);
}

void abook_close_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	AddressBook* abook;
	Plugin* plugin;
	gchar *error = NULL;
	GtkTreeView* view;
	GtkTreeIter* iter;

	if (win->selected_abook) {
		abook = (AddressBook *) win->selected_abook;
		win->selected_abook = NULL;
	}
	else {
		view = GTK_TREE_VIEW(win->abook_list);
		iter = set_selection_combobox(win->window,
						"[Close book] Choose address book",
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return;
		GtkTreeSelection* row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		abook = get_selected_address_book(view);
	}
	plugin = get_selected_plugin(GTK_TREE_VIEW(win->abook_list));
	if (abook && plugin) {
		if (win->use_extensions) {
			debug_print("BEFORE_CLOSE_ABOOK_HOOK\n");
			run_hook_callbacks(EXTENSION_BEFORE_CLOSE_ABOOK_HOOK, abook);
		}
		if (show_question(win->window, _("Close '%s' ?"), abook->abook_name)) {
			plugin->abook_close(abook, &error);
			update_abook_list(win);
		}
		if (win->use_extensions) {
			debug_print("AFTER_CLOSE_ABOOK_HOOK\n");
			run_hook_callbacks(EXTENSION_AFTER_CLOSE_ABOOK_HOOK, abook);
		}
	}
	else {
		show_message(win->window, GTK_UTIL_MESSAGE_INFO,
		_("Please highlight desired address book for deletion"));
	}
}

void abook_new_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	GSList *plugins;
	Plugin* plugin = NULL;
	AddressBook* book = NULL;
	gchar* plugin_name = NULL;
	gchar* error = NULL;

    debug_print("Creating address book window\n");
	
	plugins = plugin_get_name_all();
	gboolean response = show_choice_list(win->window,
			"[New book] Select plugin to use",
			plugins, &plugin_name);
	gslist_free(&plugins, NULL);

	if (response) {
		plugin = plugin_get_plugin(plugin_name);
		if (plugin) {
			if (win->use_extensions) {
				debug_print("BEFORE_INIT_ABOOK_HOOK\n");
				run_hook_callbacks(EXTENSION_BEFORE_INIT_ABOOK_HOOK, NULL);
			}
			if (address_book_edit(win->window, plugin, &book)) {
				if (address_book_name_exists(plugin, book)) {
					error = g_strdup_printf("%s: Name is in use!",
						(book->abook_name) ? book->abook_name : "Missing name");
					address_book_free(&book);
					goto error;
				}
				if (win->use_extensions) {
					debug_print("AFTER_INIT_ABOOK_HOOK\n");
					run_hook_callbacks(EXTENSION_AFTER_INIT_ABOOK_HOOK, book);
				}
				if (plugin->file_filter() && strcmp("xml", plugin->file_filter()) == 0 &&
						g_file_test(book->URL, G_FILE_TEST_EXISTS)) {
					AddressBook* abook =
							address_book_get(plugin, book->abook_name);
					if (abook) {
						if (show_question(win->window,
							_("%s exists. Remove ?"), book->abook_name)) {
							plugin->abook_close(abook, &error);
							g_free(error);
							error = NULL;
						}
						else {
							address_book_free(&book);
							return; 
						}
					}
					else {
						if (! show_question(win->window,
							_("%s exists. Remove ?"), book->abook_name)) {
							address_book_free(&book);
							return;
						}
					}
				}
				if (win->use_extensions) {
					debug_print("BEFORE_ADD_ABOOK_HOOK\n");
					run_hook_callbacks(EXTENSION_BEFORE_ADD_ABOOK_HOOK, book);
				}
				if (! plugin->abook_set_config(book, NULL, &error)) {
					if (plugin->abook_open(book, &error)) 
						update_abook_list(win);
				}
				else
					address_book_free(&book);
				if (win->use_extensions) {
					debug_print("AFTER_ADD_ABOOK_HOOK\n");
					run_hook_callbacks(EXTENSION_AFTER_ADD_ABOOK_HOOK, book);
				}
	error:
				if (error) {
					show_message(win->window, GTK_UTIL_MESSAGE_WARNING, "%s", error);
					g_free(error);
				}
			}
		}
	}
	g_free(plugin_name);
}

void show_progress_dialog(MainWindow* mainwindow) {
	GtkWidget *vbox, *logo;
	GdkPixbuf* pic;
	
	vbox = gtk_vbox_new(FALSE, 5);
	
	pic = gdk_pixbuf_new_from_file(PIXMAPS[PIXMAP_128], NULL);
	if (pic) {
		logo = gtk_image_new_from_pixbuf(pic);
		gtk_box_pack_start(GTK_BOX(vbox), logo, FALSE, TRUE, 5);
	}
	mainwindow->progress_bar = gtk_progress_bar_new();
	gtk_progress_bar_set_bar_style(GTK_PROGRESS_BAR(
			mainwindow->progress_bar), GTK_PROGRESS_DISCRETE);
	gtk_progress_bar_set_bar_style(GTK_PROGRESS_BAR(
			mainwindow->progress_bar), GTK_PROGRESS_CONTINUOUS);
	gtk_box_pack_start(GTK_BOX(vbox), mainwindow->progress_bar, FALSE, TRUE, 5);

	mainwindow->progress_dialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(mainwindow->progress_dialog),
			N_("Loading address books"));
	gtk_window_set_position(GTK_WINDOW(mainwindow->progress_dialog),
			GTK_WIN_POS_CENTER);
	g_signal_connect(mainwindow->progress_dialog, "map-event",
            G_CALLBACK(progress_map_event_cb), mainwindow);
	gtk_window_set_default_size(
			GTK_WINDOW(mainwindow->progress_dialog), 320, 20);
	
	gtk_container_add(GTK_CONTAINER(mainwindow->progress_dialog), vbox);
	
	gtk_widget_show_all(mainwindow->progress_dialog);
}

void destroy_progress_dialog(MainWindow* mainwindow) {
	g_source_remove(mainwindow->source_id);
	gtk_widget_hide(mainwindow->progress_dialog);
	gtk_widget_show_all(mainwindow->window);
	if (! mainwindow->compose_mode) {
		gtk_widget_hide(mainwindow->to_btn);
		gtk_widget_hide(mainwindow->cc_btn);
		gtk_widget_hide(mainwindow->bcc_btn);
	}
	update_abook_list(mainwindow);
}

void update_abook_list(MainWindow* mainwindow) {
    GtkTreeIter   iter, *selected = NULL;
    GtkListStore* store;
    GtkTreeModel* model;
    GtkTreeSelection* row;
    GSList *list, *ref, *cur;

    model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainwindow->abook_list));
    store = GTK_LIST_STORE(model);

	list_view_clear(GTK_TREE_VIEW(mainwindow->abook_list), mainwindow);
	list_view_clear(GTK_TREE_VIEW(mainwindow->contact_list), mainwindow);
	ref = address_books_get();
	for (list = ref; list; list = g_slist_next(list)) {
		Reference* reference = (Reference *) list->data;
		for (cur = reference->abooks; cur; cur = g_slist_next(cur)) {
			AddressBook* book = (AddressBook *) cur->data;
			gtk_list_store_append(store, &iter);
			gtk_list_store_set(store, &iter,
					BOOK_NAME_COLUMN, book->abook_name,
					BOOK_DATA_COLUMN, book,
					BOOK_PLUGIN_COLUMN, reference->plugin, -1);
			if (mainwindow->selected_abook) {
				if (address_book_compare(book, mainwindow->selected_abook) == 0) {
					selected = gtk_tree_iter_copy(&iter);
				}
			}
		}
		gslist_free(&reference->abooks, NULL);
	}
	if (! ref) {
		gtk_widget_set_sensitive(mainwindow->search_btn, FALSE);
		gtk_widget_set_sensitive(mainwindow->adv_search_btn, FALSE);
		gtk_widget_set_sensitive(mainwindow->new_btn, FALSE);
	}
	if (selected) {
		row = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainwindow->abook_list));
		gtk_tree_selection_select_iter(row, selected);
		abook_list_cursor_changed_cb(GTK_TREE_VIEW(mainwindow->abook_list), mainwindow);
		gtk_tree_iter_free(selected);
	}
	gslist_free(&ref, NULL);
}

void abook_list_cursor_changed_cb(GtkTreeView* tree_view, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	AddressBook* abook;
	GList *cur;

	list_view_clear(GTK_TREE_VIEW(win->contact_list), win);
	gtk_entry_set_text(GTK_ENTRY(win->search_text), "");
	abook = get_selected_address_book(tree_view);
	if (abook) {
		for (cur = abook->contacts; cur; cur = g_list_next(cur)) {
			Contact* contact = (Contact *) cur->data;
			list_view_append_contact(
					GTK_TREE_VIEW(win->contact_list), contact);
		}
		gtk_tree_view_columns_autosize(GTK_TREE_VIEW(win->contact_list));
		gchar* msg = g_strdup_printf("%s [%d Contact(s)]",
				(abook->abook_name)?abook->abook_name: "?",
				g_list_length(abook->contacts));
		set_status_msg(win, STATUS_MSG_CONNECTED, msg);
		g_free(msg);
		gtk_widget_set_sensitive(win->search_btn, TRUE);
		gtk_widget_set_sensitive(win->adv_search_btn, TRUE);
		gtk_widget_set_sensitive(win->new_btn, TRUE);
	}
}

static void compare_extra_config_value(ExtraConfig* ec) {
	if (ec) {
		switch (ec->type) {
			case PLUGIN_CONFIG_EXTRA_CHECKBOX:
			case PLUGIN_CONFIG_EXTRA_SPINBUTTON:
				break;
			case PLUGIN_CONFIG_EXTRA_ENTRY:
				if (! ec->current_value.entry)
					ec->current_value.entry = g_strdup(ec->preset_value.entry);
				break;
		}
	}
}

static GSList* make_extra_config(GSList* preset, GSList* current) {
	GSList* merged = NULL;
	GSList *head, *p_list, *c_list;
	
	head = g_slist_copy(preset);
	c_list = current;
	while (c_list) {
		p_list = preset;
		while(p_list) {
			ExtraConfig* c = (ExtraConfig *) c_list->data;
			ExtraConfig* p = (ExtraConfig *) p_list->data;
			if (c && p && c->type == p->type &&
					utf8_collate(c->label, p->label) == 0) {
				compare_extra_config_value(c);
				merged = g_slist_prepend(merged, c);
				head = g_slist_remove(head, p);
				break;
			}
			p_list = p_list->next;
		}
		c_list = c_list->next;
	}
	p_list = head;
	while (p_list) {
		merged = g_slist_prepend(merged, extra_config_copy(p_list->data));
		p_list = p_list->next;
	}
	g_slist_free(head);
	
	return merged;
}

static void add_advanced_page(GtkNotebook* notebook,
							  Plugin* plugin,
							  AddressBook* address_book) {
	GSList *cur, *extra;
	GtkWidget *widget = NULL, *hbox, *vbox, *label = NULL;
	gboolean new;
	
	if (address_book->extra_config) {
		GSList* preset = plugin->extra_config();
		extra = make_extra_config(preset, address_book->extra_config);
		gslist_free(&preset, extra_config_free);
		new = FALSE;
	}
	else {
		extra = plugin->extra_config();
		new = TRUE;
	}

	if (extra) {
		vbox = gtk_vbox_new(FALSE, 0);
		for (cur = extra; cur; cur = g_slist_next(cur)) {
			ExtraConfig* ec = (ExtraConfig *) cur->data;
			hbox = gtk_hbox_new(FALSE, 5);
			switch (ec->type) {
				case PLUGIN_CONFIG_EXTRA_CHECKBOX:
					widget = gtk_check_button_new();
					if (ec->label)
						gtk_button_set_label(GTK_BUTTON(widget), ec->label);
					if (ec->tooltip)
						gtk_widget_set_tooltip_text(widget, ec->tooltip);
					if (new)
						gtk_toggle_button_set_active(
							GTK_TOGGLE_BUTTON(widget), ec->preset_value.check_btn);
					else
						gtk_toggle_button_set_active(
							GTK_TOGGLE_BUTTON(widget), ec->current_value.check_btn);
					label = gtk_label_new("");
					break;
				case PLUGIN_CONFIG_EXTRA_ENTRY:
					widget = gtk_entry_new();
					if (ec->label)
						label = gtk_label_new(ec->label);
					else
						label = gtk_label_new("");
					if (ec->tooltip)
						gtk_widget_set_tooltip_text(widget, ec->tooltip);
					if (ec->current_value.entry)
						gtk_entry_set_text(GTK_ENTRY(widget), ec->current_value.entry);
					else if (ec->preset_value.entry)
						gtk_entry_set_text(GTK_ENTRY(widget), ec->preset_value.entry);
					break;
				case PLUGIN_CONFIG_EXTRA_SPINBUTTON:
					widget = gtk_spin_button_new_with_range(0.0, 100.0, 1.0);
					if (ec->label)
						label = gtk_label_new(ec->label);
					else
						label = gtk_label_new("");
					if (ec->tooltip)
						gtk_widget_set_tooltip_text(widget, ec->tooltip);
					if (new)
						gtk_spin_button_set_value(
							GTK_SPIN_BUTTON(widget), ec->preset_value.spin_btn);
					else		
						gtk_spin_button_set_value(
							GTK_SPIN_BUTTON(widget), ec->current_value.spin_btn);		
					break;
			}
			gtk_widget_set_name(widget, ec->label);
			gtk_widget_set_size_request(label, 100, -1);
			gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
			gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
			if (ec->type == PLUGIN_CONFIG_EXTRA_SPINBUTTON)
				gtk_box_pack_start(GTK_BOX(hbox), widget, FALSE, FALSE, 2);
			else
				gtk_box_pack_start(GTK_BOX(hbox), widget, TRUE, TRUE, 2);
			gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
		}
		label = gtk_label_new(_("Advanced settings"));
		gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vbox, label);
		if (new)
			gslist_free(&extra, extra_config_free);
		else
			gslist_free(&extra, NULL);
	}
}

static void set_advanced_config(Plugin* plugin,
								AddressBook* address_book,
								GtkNotebook* notebook) {
	GtkWidget *page, *widget;
	GSList *config, *cur, *widgets = NULL, *ptr;
	gboolean found;
	
	config = plugin->extra_config();
	page = gtk_notebook_get_nth_page(notebook, 1);
	
	if (config && page) {
		for (cur = config; cur; cur = g_slist_next(cur)) {
			ExtraConfig* ec = (ExtraConfig *) cur->data;
			gslist_free(&widgets, NULL);
			widgets = find_name(GTK_CONTAINER(notebook), ec->label);
			if (! widgets)
				continue;
			
			switch (ec->type) {
				case PLUGIN_CONFIG_EXTRA_CHECKBOX:
					found = FALSE;
					for (ptr = widgets; ptr && !found; ptr = g_slist_next(ptr)) {
						widget = (GtkWidget *) widgets->data;
						if (debug_get_mode()) {
							gchar* text = gtk_widget_get_tooltip_text(widget);
							debug_print("%s\n", (text) ? text: "(NULL)");
							g_free(text);
						}
						if (GTK_IS_CHECK_BUTTON(widget))
							found = TRUE;
						else
							widget = NULL;
					}
					if (widget) {
						ec->current_value.check_btn = 
							gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));
					}
					break;
				case PLUGIN_CONFIG_EXTRA_ENTRY:
					found = FALSE;
					for (ptr = widgets; ptr && !found; ptr = g_slist_next(ptr)) {
						widget = (GtkWidget *) widgets->data;
						if (debug_get_mode()) {
							gchar* text = gtk_widget_get_tooltip_text(widget);
							debug_print("%s\n", (text) ? text: "(NULL)");
							g_free(text);
						}
						if (GTK_IS_ENTRY(widget))
							found = TRUE;
						else
							widget = NULL;
					}
					if (widget) {
						g_free(ec->current_value.entry);
						ec->current_value.entry = 
							gtk_editable_get_chars(GTK_EDITABLE(widget), 0, -1);
					}
					break;
				case PLUGIN_CONFIG_EXTRA_SPINBUTTON:
					found = FALSE;
					for (ptr = widgets; ptr && !found; ptr = g_slist_next(ptr)) {
						widget = (GtkWidget *) widgets->data;
						if (debug_get_mode()) {
							gchar* text = gtk_widget_get_tooltip_text(widget);
							debug_print("%s\n", (text) ? text: "(NULL)");
							g_free(text);
						}
						if (GTK_IS_SPIN_BUTTON(widget))
							found = TRUE;
						else
							widget = NULL;
					}
					if (widget) {
						ec->current_value.spin_btn = 
							gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widget));
					}
					break;
			}
		}
		address_book->extra_config = config;
	}
	else	
		gslist_free(&config, extra_config_free);
}

#define GTK_ENTRIES 4
gboolean address_book_edit(GtkWidget* parent,
						   Plugin* plugin,
						   AddressBook** address_book) {
	GtkWidget* dialog;
	GtkWidget* file_btn;
	AddressBook* book;
	GtkWidget* input[GTK_ENTRIES] = {NULL, NULL, NULL, NULL};
	GtkWidget *hbox, *vbox, *label = NULL, *frame, *notebook = NULL;
	int i;
	gboolean response = FALSE;
	gboolean use_button = FALSE;
	struct DataContainer data;
	gboolean show_url = TRUE;
	gchar* title = NULL;
	
	for (i = 0; i < GTK_ENTRIES; i++) {
		input[i] = gtk_entry_new();
	}
	
	if (*address_book) {
		book = *address_book;
		if (book->abook_name)
			gtk_entry_set_text(GTK_ENTRY(input[0]), book->abook_name);
		if (book->URL)
			gtk_entry_set_text(GTK_ENTRY(input[1]), book->URL);
		if (book->username)
			gtk_entry_set_text(GTK_ENTRY(input[2]), book->username);
		if (book->password)
			gtk_entry_set_text(GTK_ENTRY(input[3]), book->password);
		gtk_entry_set_visibility(GTK_ENTRY(input[3]), FALSE);
		title = g_strdup(_("Edit address book"));
	}
	else {
		book = *address_book = address_book_new();
		title = g_strdup(_("New address book"));
	}
	
	dialog = gtk_dialog_new_with_buttons(
                title, 
                GTK_WINDOW(parent),
                GTK_DIALOG_DESTROY_WITH_PARENT,
                GTK_STOCK_OK, GTK_RESPONSE_OK,
                GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                NULL);
    g_free(title);
    gtk_widget_set_size_request(dialog, 360, -1);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_CANCEL);
	
	vbox = gtk_vbox_new(FALSE, 0);
	for (i = 0; i < GTK_ENTRIES; i++) {
		hbox = gtk_hbox_new(FALSE, 5);
		switch (i) {
			case 0: 
				label = gtk_label_new(_("Name"));
				gtk_widget_set_tooltip_text(
					input[i], _("The name for this address book"));
				break;
			case 1:
				if (show_url) { 
					label = gtk_label_new(_("URL"));
					gtk_widget_set_tooltip_text(
						input[i], _("URL or path to this address book\n"
									"Must conform to plugin requirements."));
					if (plugin->file_filter()) {
						file_btn = gtk_button_new_from_stock(GTK_STOCK_OPEN);
						gtk_widget_set_tooltip_text(
							file_btn, _("Open file dialog"));
						data.plugin = plugin;
						data.addressbook = address_book;
						data.widget = input[i];
						g_signal_connect(file_btn, "clicked",
							G_CALLBACK(select_addr_book_cb), &data);
						use_button = TRUE;
					}
				}
				else {
					gtk_widget_destroy(input[i]);
					input[i] = NULL;
				}
				break;
			case 2:
				label = gtk_label_new(_("Username"));
				gtk_widget_set_tooltip_text(
					input[i], _("And optional username"));
				if (!plugin->need_credentials())
					gtk_widget_set_sensitive(input[i], FALSE);
				break;
			case 3:
				label = gtk_label_new(_("Password"));
				gtk_widget_set_tooltip_text(
					input[i], _("And optional password"));
				gtk_entry_set_visibility(GTK_ENTRY(input[i]), FALSE);
				if (!plugin->need_credentials())
					gtk_widget_set_sensitive(input[i], FALSE);
				break;
		}
		if (input[i]) {
			gtk_widget_set_size_request(label, 100, -1);
			gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
			gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
			gtk_box_pack_start(GTK_BOX(hbox), input[i], TRUE, TRUE, 2);
			if (use_button) {
				gtk_box_pack_start(GTK_BOX(hbox), file_btn, FALSE, FALSE, 2);
				use_button = FALSE;
			}
			gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
		}
	}
	if (plugin->extra_config()) {
		notebook = gtk_notebook_new();
		label = gtk_label_new(_("Basic settings"));
		gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vbox, label);
		add_advanced_page(GTK_NOTEBOOK(notebook), plugin, book);
		gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), notebook);
		gtk_widget_show_all(notebook);
	}
	else {
		frame = gtk_frame_new(_("Address book settings"));
		gtk_container_add(GTK_CONTAINER(frame), vbox);
		gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), frame);
		gtk_widget_show_all(frame);
	}
	dialog_set_focus(GTK_DIALOG(dialog), GTK_STOCK_CANCEL);
	
	gint result = gtk_dialog_run(GTK_DIALOG(dialog));
	switch (result) {
		case GTK_RESPONSE_OK:
			g_free(book->abook_name);
			book->abook_name = 
				gtk_editable_get_chars(GTK_EDITABLE(input[0]), 0, -1);
			if (strlen(book->abook_name) < 1) {
				/* Empty string. Ensure tree_view displays 'Missing name' */
				g_free(book->abook_name);
				book->abook_name = NULL;
			}
			if (show_url) {
				g_free(book->URL);
				book->URL = 
					gtk_editable_get_chars(GTK_EDITABLE(input[1]), 0, -1);
			}
			else {
				book->URL = plugin->default_url(book->abook_name);
			}
			g_free(book->username);
			book->username = 
				gtk_editable_get_chars(GTK_EDITABLE(input[2]), 0, -1);
			g_free(book->password);
			book->password = 
				gtk_editable_get_chars(GTK_EDITABLE(input[3]), 0, -1);
			response = TRUE;
			if (plugin->extra_config()) {
				set_advanced_config(plugin, book, GTK_NOTEBOOK(notebook));
			}
			break;
		case GTK_RESPONSE_CANCEL:
			break;
	}
	gtk_widget_destroy(dialog);

	return response;
}

void abook_list_row_activated_cb(GtkTreeView *tree_view,
								 GtkTreePath *path,
								 GtkTreeViewColumn *column,
								 gpointer data) {
	MainWindow* win = (MainWindow *) data;
	AddressBook *abook, *new;
	Plugin* plugin;
	gchar* error = NULL;

	abook = get_selected_address_book(tree_view);
	new = address_book_copy(abook, TRUE);
	plugin = get_selected_plugin(tree_view);
	if (address_book_edit(win->window, plugin, &new)) {
		plugin->abook_close(abook, &error);
		if (error) {
			show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
			g_free(error);
			address_book_free(&new);
			return;
		}
		plugin->abook_set_config(abook, new, &error);
		if (error) {
			show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
			g_free(error);
			address_book_free(&new);
			return;
		}
		address_book_free(&abook);
		plugin->abook_open(new, &error);
		if (error) {
			show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
			g_free(error);
			address_book_free(&new);
		}
		update_abook_list(win);
	}
}

gboolean list_button_pressed_cb(GtkWidget *widget, GdkEventButton *event,
        gpointer data) {
    GtkTreeSelection* row;
    GtkTreePath* path;
    gboolean new_row;

    debug_print("Button pressed: %d\n", event->button);
    if (event->button == 3 && event->type == GDK_BUTTON_PRESS) {
        row = gtk_tree_view_get_selection(GTK_TREE_VIEW(widget));
        if (gtk_tree_selection_count_selected_rows(row)  <= 1) {
            if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(widget),
                (gint) event->x, (gint) event->y, &path, NULL, NULL, NULL)) {
                gtk_tree_selection_unselect_all(row);
                gtk_tree_selection_select_path(row, path);
                gtk_tree_path_free(path);
                new_row = FALSE;
            }
            else
                new_row = TRUE;
            show_popup_menu(widget, event, data, new_row);
            return TRUE;
        }
    }
    return FALSE;
}

gboolean list_popup_menu_cb(GtkWidget* widget, gpointer data) {
    GtkTreeSelection* row;
    gboolean new_row;

    row = gtk_tree_view_get_selection(GTK_TREE_VIEW(widget));
    if (gtk_tree_selection_get_selected(row, NULL, NULL))
        new_row = FALSE;
    else
        new_row = TRUE;
    show_popup_menu(widget, NULL, data, new_row);
    return TRUE;
}

void show_about(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	
	debug_print("Show about dialog\n");
	about_dialog_show(win->window);
}

void contact_list_cursor_changed_cb(GtkTreeView* tree_view, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	Contact* contact;

	contact = get_selected_contact(tree_view);
	if (contact && win->compose_mode) {
		gtk_widget_set_sensitive(win->to_btn, TRUE);
		gtk_widget_set_sensitive(win->cc_btn, TRUE);
		gtk_widget_set_sensitive(win->bcc_btn, TRUE);
	}
}

void contact_list_row_activated_cb(GtkTreeView *tree_view,
								   GtkTreePath *path,
								   GtkTreeViewColumn *column,
								   gpointer data) {
	MainWindow* win = (MainWindow *) data;
	Contact* contact;
	gchar* error = NULL;

	if (debug_get_mode()) {
		gchar* p = gtk_tree_path_to_string(path);
		debug_print("GtkTreePath: %s\n", p);
		g_free(p);
	}
	contact = get_selected_contact(tree_view);
	if (win->use_extensions) {
		debug_print("BEFORE_OPEN_ABOOK_HOOK\n");
		run_hook_callbacks(EXTENSION_BEFORE_OPEN_CONTACT_HOOK, contact);
	}
	contact_show(win, contact, &error);

	if (error) {
		show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
		g_free(error);
	}
	if (win->use_extensions) {
		debug_print("AFTER_OPEN_ABOOK_HOOK\n");
		run_hook_callbacks(EXTENSION_AFTER_OPEN_CONTACT_HOOK, contact);
	}
}

void contact_cell_edited_cb(GtkCellRendererText *cell,
							gchar *path_string,
							gchar *new_text,
							gpointer data) {
	MainWindow* win = (MainWindow *) data;
	AddressBook *abook;
	Plugin* plugin;
	Contact *old_contact = NULL, *new_contact = NULL;
	gchar* error = NULL;
	GtkTreeIter iter;
	GtkTreeModel* model;
	GtkTreeViewColumn* column;
	GtkTreePath* path;
	guint col;
	
	debug_print("Path: %s - New text: %s\n", path_string, new_text);
	model = gtk_tree_view_get_model(GTK_TREE_VIEW(win->contact_list));
	gtk_tree_model_get_iter_from_string(model, &iter, path_string);
	gtk_tree_view_get_cursor(GTK_TREE_VIEW(win->contact_list), &path, &column);

	if (! cell && ! cell->text && strlen(new_text) < 1) {
		contact_list_row_activated_cb(
				GTK_TREE_VIEW(win->contact_list), path, column, data);
		gtk_tree_path_free(path);
		return;
	}

	gtk_tree_path_free(path);

	if (cell->text && new_text && strcmp(cell->text, new_text) == 0)
		return;
	
	for (col = CONTACT_DISPLAYNAME_COLUMN; col < CONTACT_N_COLUMNS; col++) {
		if (column == gtk_tree_view_get_column(
				GTK_TREE_VIEW(win->contact_list),col))
			break;
	}

	plugin = get_selected_plugin(GTK_TREE_VIEW(win->abook_list));
	abook = get_selected_address_book(GTK_TREE_VIEW(win->abook_list));
	if (abook && plugin) {
		old_contact = get_selected_contact(GTK_TREE_VIEW(win->contact_list));
		if (old_contact) {
			new_contact = contact_copy(old_contact);
			if (new_contact) {			
				contact_set_attr(new_contact, col, new_text);
				error = contact_write_to_backend(
						plugin, abook, old_contact, new_contact);
			}
		}
	}
	else {
		show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "Unresolved error");
	}
	if (error) {
		show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
		g_free(error);
		contact_free(new_contact);
		g_free(new_contact);
	}
	else {
		gtk_list_store_set(GTK_LIST_STORE(model), &iter, col, new_text, -1);
		gtk_list_store_set(GTK_LIST_STORE(model), &iter,
				CONTACT_DATA_COLUMN, new_contact, -1);
		if (old_contact) {
			contact_free(old_contact);
			g_free(old_contact);
		}
	}
}

void contact_add_to_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	gchar* mail = NULL;
	GtkTreeModel* model = NULL;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;

	row = gtk_tree_view_get_selection(GTK_TREE_VIEW(win->contact_list));
	if (row) {
		iter = g_new0(GtkTreeIter, 1);
		if (gtk_tree_selection_get_selected(row, &model, iter)) {
			gtk_tree_model_get(model, iter, CONTACT_EMAIL_COLUMN, &mail, -1);
		}
		g_free(iter);
	}
	if (mail) {
		DBusProxy = g_object_new(OBJECT_TYPE_SERVER, NULL);
		notify_to_selected(DBusProxy, mail);
		g_object_unref(DBusProxy);
		g_free(mail);
	}
}

void contact_add_cc_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	gchar* mail = NULL;
	GtkTreeModel* model = NULL;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;

	row = gtk_tree_view_get_selection(GTK_TREE_VIEW(win->contact_list));
	if (row) {
		iter = g_new0(GtkTreeIter, 1);
		if (gtk_tree_selection_get_selected(row, &model, iter)) {
			gtk_tree_model_get(model, iter, CONTACT_EMAIL_COLUMN, &mail, -1);
		}
		g_free(iter);
	}
	if (mail) {
		DBusProxy = g_object_new(OBJECT_TYPE_SERVER, NULL);
		notify_cc_selected(DBusProxy, mail);
		g_object_unref(DBusProxy);
		g_free(mail);
	}
}

void contact_add_bcc_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	gchar* mail = NULL;
	GtkTreeModel* model = NULL;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;

	row = gtk_tree_view_get_selection(GTK_TREE_VIEW(win->contact_list));
	if (row) {
		iter = g_new0(GtkTreeIter, 1);
		if (gtk_tree_selection_get_selected(row, &model, iter)) {
			gtk_tree_model_get(model, iter, CONTACT_EMAIL_COLUMN, &mail, -1);
		}
		g_free(iter);
	}
	if (mail) {
		DBusProxy = g_object_new(OBJECT_TYPE_SERVER, NULL);
		notify_bcc_selected(DBusProxy, mail);
		g_object_unref(DBusProxy);
		g_free(mail);
	}
}

void contact_new_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	gchar* error = NULL;
	GtkTreeView* view;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;
	AddressBook* abook;
	Plugin* plugin;

	view = GTK_TREE_VIEW(win->abook_list);
	abook = get_selected_address_book(view);
	if (abook == NULL) {
		iter = set_selection_combobox(win->window,
						_("[New Contact] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return;
		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
	}
	plugin = get_selected_plugin(view);

	if (plugin && ! plugin->readonly) {
		if (win->use_extensions) {
			debug_print("BEFORE_INIT_CONTACT_HOOK\n");
			run_hook_callbacks(EXTENSION_BEFORE_INIT_CONTACT_HOOK, NULL);
		}
		contact_show(win, NULL, &error);
		/* EXTENSION_AFTER_INIT_CONTACT_HOOK -> contactwindow->contact_update */
	}
	else
		error = g_strdup(_("Plugin does not support updates"));

	if (error) {
		show_message(win->window, GTK_UTIL_MESSAGE_INFO, "%s", error);
		g_free(error);
	}
}

void contact_delete_cb(GtkWidget* widget, gpointer data) {
    MainWindow* win = (MainWindow *) data;
	GtkTreeModel* model = NULL;
	GtkTreeView* view;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;
	AddressBook* abook;
	Plugin* plugin;
	Contact* contact;
	gchar *mail, *cn, *first, *last, *con_id;
	gchar* error = NULL;

	mail = cn = first = last = con_id = NULL;
	view = GTK_TREE_VIEW(win->abook_list);
	abook = get_selected_address_book(view);
	if (abook == NULL) {
		iter = set_selection_combobox(win->window,
						_("[Delete Contact] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return;
		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		abook = get_selected_address_book(view);
	}

	plugin = get_selected_plugin(view);

	if (! plugin || (plugin && plugin->readonly)) {
		show_message(win->window, GTK_UTIL_MESSAGE_INFO,
				_("Plugin does not support updates"));
		return;
	}
	
	view = GTK_TREE_VIEW(win->contact_list);
	if (win->selected_contact) {
		contact = (Contact *) win->selected_contact;
		win->selected_contact = NULL;
	}
	else {
		iter = set_selection_combobox(win->window,
						_("[Delete Contact] Choose contact to delete"),
						gtk_tree_view_get_model(view),
						CONTACT_EMAIL_COLUMN);
		if (! iter)
			return;
		if (! iter->stamp) {
			show_message(win->window, GTK_UTIL_MESSAGE_INFO, "No available contact");
			return;
		}
		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		iter = NULL;
		contact = get_selected_contact(view);
	}
	
	if (abook && plugin && contact) {
		row = gtk_tree_view_get_selection(view);
		if (row) {
			iter = g_new0(GtkTreeIter, 1);
			if (gtk_tree_selection_get_selected(row, &model, iter)) {
				gtk_tree_model_get(model, iter, CONTACT_EMAIL_COLUMN, &mail, -1);
				gtk_tree_model_get(model, iter, CONTACT_DISPLAYNAME_COLUMN, &cn, -1);
				gtk_tree_model_get(model, iter, CONTACT_FIRSTNAME_COLUMN, &first, -1);
				gtk_tree_model_get(model, iter, CONTACT_LASTNAME_COLUMN, &last, -1);
			}
			if (mail)
				con_id = g_strdup(mail);
			else if (cn)
				con_id = g_strdup(cn);
			else if (first || last) {
				if (first && last)
					con_id = g_strconcat(first, " ", last, NULL);
				else if (last)
					con_id = g_strdup(last);					
				else
					con_id = g_strdup(first);					
			}
		}
		if (show_question(win->window,
			_("Should contact '%s' be deleted?"), con_id ? con_id : "Unknown")) {
			if (win->use_extensions) {
				debug_print("BEFORE_DELETE_CONTACT_HOOK\n");
				run_hook_callbacks(EXTENSION_BEFORE_DELETE_CONTACT_HOOK, contact);
			}
			plugin->delete_contact(abook, contact, &error);
			if (iter && !error) {
				gtk_list_store_remove(GTK_LIST_STORE(model), iter);
				abook->contacts = g_list_remove(abook->contacts, contact);
				if (win->use_extensions) {
					debug_print("BEFORE_DELETE_CONTACT_HOOK\n");
					run_hook_callbacks(EXTENSION_AFTER_DELETE_CONTACT_HOOK, contact);
				}
				contact_free(contact);
				g_free(contact);
			}
			g_free(iter);
		}
		if (error) {
			show_message(win->window, GTK_UTIL_MESSAGE_WARNING, "%s", error);
			g_free(error);
		}
		g_free(mail);
		g_free(cn);
		g_free(first);
		g_free(last);
		g_free(con_id);
	}
}

void contact_edit_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	GtkTreeView* view;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;
	AddressBook* abook;
	Plugin* plugin;
	Contact* contact;
	gchar* error = NULL;

	view = GTK_TREE_VIEW(win->abook_list);
	plugin = get_selected_plugin(view);

	if (plugin && plugin->readonly) {
		show_message(win->window, GTK_UTIL_MESSAGE_INFO,
				_("Plugin does not support updates"));
		return;
	}
	
	abook = get_selected_address_book(view);
	if (abook == NULL) {
		iter = set_selection_combobox(win->window,
						_("[Edit Contact] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return;
		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		abook = get_selected_address_book(view);
	}

	view = GTK_TREE_VIEW(win->contact_list);
	if (win->selected_contact) {
		contact = (Contact *) win->selected_contact;
		win->selected_contact = NULL;
	}
	else {
		iter = set_selection_combobox(win->window,
						_("[Edit Contact] Choose contact to edit"),
						gtk_tree_view_get_model(view),
						CONTACT_EMAIL_COLUMN);
		if (! iter)
			return;
		if (! iter->stamp) {
			show_message(win->window, GTK_UTIL_MESSAGE_INFO, "No available contact");
			return;
		}
		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		iter = NULL;
		contact = get_selected_contact(view);
	}

	if (abook && plugin && contact) {
		contact_show(win, contact, &error);
		if (error) {
			show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
			g_free(error);
		}
	}
}

void tools_attribs_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	Plugin* plugin;
	gchar* plugin_name;
	GSList *plugins;
	AttribContainer* attr_container;
	gchar* error = NULL;
	GHashTable* new_attribs = NULL;

	plugins = plugin_get_name_all();
	gboolean response = show_choice_list(win->window,
			"[Attributes] Select plugin to use",
			plugins, &plugin_name);
	gslist_free(&plugins, NULL);
	if (response) {
		plugin = plugin_get_plugin(plugin_name);

		if (plugin) {
			attr_container = g_new0(AttribContainer, 1);
			attr_container->existing_attribs = plugin->attrib_list();
			attr_container->inactive_attribs = plugin->inactive_attribs();
			attr_container->remaining_attribs = plugin->remaining_attribs();
			new_attribs = get_attrib_list(win->window, attr_container,
					"Supported Attributes",	TRUE, &error, add_attrib_btn_cb);
			gslist_free(&attr_container->existing_attribs, attrib_def_free);
			gslist_free(&attr_container->inactive_attribs, attrib_def_free);
			gslist_free(&attr_container->remaining_attribs, attrib_def_free);
			g_free(attr_container);
			if (error) {
				show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", error);
				g_free(error);
			}
			else {
				plugin->attribs_set(new_attribs);
			}
			hash_table_free(&new_attribs);
		}
	}
}

void contact_basic_search_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	gchar* search = NULL;
	GtkTreeView* view;
	AddressBook* abook;
	Plugin* plugin;
	Contact* contact;
	gchar* error = NULL;
	GSList *contacts, *cur;
	
	search = gtk_editable_get_chars(GTK_EDITABLE(win->search_text), 0, -1);
	if (!search || strlen(search) < 1) {
		show_message(win->window, GTK_UTIL_MESSAGE_INFO,
		"Missing search term");
		return;
	}
	if ((search[0] != '*' && search[0] != '?') &&
			(search[strlen(search) - 1] != '*' && 
			search[strlen(search) - 1] != '?')) {
		gchar* tmp = g_strdup(search);
		g_free(search);
		search = g_strconcat("*", tmp, "*", NULL);
		g_free(tmp);
	}
	
	view = GTK_TREE_VIEW(win->abook_list);
	abook = get_selected_address_book(view);
	plugin = get_selected_plugin(view);

	if (abook && plugin) {
		if (win->use_extensions) {
			debug_print("BEFORE_SEARCH_CONTACT_HOOK\n");
			run_hook_callbacks(EXTENSION_BEFORE_SEARCH_ABOOK_HOOK, abook);
		}
		contacts = plugin->get_contact(abook, search, &error);
		g_free(search);
		if (error) {
			show_message(win->window, GTK_UTIL_MESSAGE_WARNING, "%s", error);
			g_free(error);
		}
		else {
			if (win->use_extensions) {
				debug_print("AFTER_SEARCH_CONTACT_HOOK\n");
				run_hook_callbacks(EXTENSION_AFTER_SEARCH_ABOOK_HOOK, contacts);
			}
			view = GTK_TREE_VIEW(win->contact_list);
			list_view_clear(view, win);
			for (cur = contacts; cur; cur = g_slist_next(cur)) {
				contact = (Contact *) cur->data;
				list_view_append_contact(view, contact);
			}
		}		
		gslist_free(&contacts, NULL);
	}
}

void contact_clear_search_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	GtkTreeView *view = GTK_TREE_VIEW(win->abook_list);

	abook_list_cursor_changed_cb(view, data);
	gtk_widget_set_sensitive(win->clear_btn, FALSE);
}

void contact_clear_sensitivity_cb(GtkWidget* widget, GdkEvent* event, gpointer data) {
	MainWindow* win = (MainWindow *) data;

	gtk_widget_set_sensitive(win->clear_btn,
		(gtk_entry_get_text_length(GTK_ENTRY(widget)) > 0)? TRUE: FALSE);
}

void contact_advanced_search_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	GtkTreeView* view;
	AddressBook* abook;
	Plugin* plugin;
	gchar* error = NULL;
	Contact* contact;
	gboolean is_and;
	GSList *contacts, *cur;

	
	view = GTK_TREE_VIEW(win->abook_list);
	plugin = get_selected_plugin(view);

	if (plugin && ! plugin->can_adv_search) {
		show_message(win->window, GTK_UTIL_MESSAGE_INFO,
				_("Plugin does not support advanced search"));
		return;
	}
	
	abook = get_selected_address_book(view);

	if (abook && plugin) {
		contact = contact_search_show(win, &is_and, &error);
		if (error) {
			show_message(win->window, GTK_UTIL_MESSAGE_WARNING, "%s", error);
			g_free(error);
		}
		else if (contact) {
			if (win->use_extensions) {
				debug_print("BEFORE_SEARCH_CONTACT_HOOK (advanced)\n");
				run_hook_callbacks(EXTENSION_BEFORE_SEARCH_CONTACT_HOOK, contact);
			}
			contacts = plugin->search_contact(abook, contact, is_and, &error);
			if (error) {
				show_message(win->window, GTK_UTIL_MESSAGE_WARNING, "%s", error);
				g_free(error);
			}
			else {
				contact_free(contact);
				g_free(contact);
				if (win->use_extensions) {
					debug_print("AFTER_SEARCH_CONTACT_HOOK (advanced)\n");
					run_hook_callbacks(EXTENSION_AFTER_SEARCH_CONTACT_HOOK, contacts);
				}
				view = GTK_TREE_VIEW(win->contact_list);
				list_view_clear(view, win);
				for (cur = contacts; cur; cur = g_slist_next(cur)) {
					contact = (Contact *) cur->data;
					list_view_append_contact(view, contact);
				}
				gtk_widget_set_sensitive(win->clear_btn, TRUE);
			}		
			gslist_free(&contacts, NULL);
			
		}
		else
			return;

	}
}

void tools_prefs_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;

	show_settings(win, config);
}

void show_help(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;

	show_message(win->window, GTK_UTIL_MESSAGE_INFO,
		"Implementation awaits your contribution:-)");
}

void contact_print_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	AddressBook* abook;
	PrintInfo* print_data;
	GtkTreeView* view;
	GtkTreeIter* iter = NULL;
	GtkTreeSelection* row;
	
	view = GTK_TREE_VIEW(win->abook_list);
	abook = get_selected_address_book(view);
	print_data = g_new0(PrintInfo, 1);
	print_data->print_book = FALSE;

	if (abook == NULL) {
		iter = set_selection_combobox(win->window,
						_("[Print Contact] Choose address book"),
						gtk_tree_view_get_model(view),
						BOOK_NAME_COLUMN);
		if (! iter)
			return;
		row = gtk_tree_view_get_selection(view);
		gtk_tree_selection_select_iter(row, iter);
		g_free(iter);
		abook = get_selected_address_book(view);
		print_data->print_book = TRUE;
	}

	if (abook) {
		view = GTK_TREE_VIEW(win->contact_list);
		
		print_data->win = win;
		print_data->abook = abook;
		print_data->contact = get_selected_contact(view);
		
		show_print_dialog(print_data);
		
		if (print_data->error) {
			show_message(win->window, GTK_UTIL_MESSAGE_ERROR, "%s", print_data->error);
			g_free(print_data->error);
		}
	}

	g_free(print_data);
}

void contact_print_prepare_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	GtkTreeView* view;
	GtkTreeSelection* row = NULL;
    GtkTreeIter   iter;
    GtkTreeModel* model;
    GtkTreePath* selected;
    gboolean has_selection = FALSE;
	
	view = GTK_TREE_VIEW(win->contact_list);

	row = gtk_tree_view_get_selection(view);
	if (row) {
		if (gtk_tree_selection_get_selected(row, &model, &iter)) {
			gtk_tree_selection_unselect_all(row);
			has_selection = TRUE;
		}
	}
		
	contact_print_cb(widget, data);
	
	if (has_selection) {
		selected = gtk_tree_model_get_path(model, &iter);
		gtk_tree_view_set_cursor(view, selected, NULL, FALSE);
		gtk_tree_path_free(selected);
	}
}

void printer_page_setup_cb(GtkWidget* widget, gpointer data) {
	MainWindow* win = (MainWindow *) data;
	
	do_page_setup(win);
}

void markup_default_book(GtkTreeViewColumn* col,
						 GtkCellRenderer* renderer,
						 GtkTreeModel* model,
						 GtkTreeIter* iter,
						 gpointer data) {
    MainWindow* win = (MainWindow *) data;
    gchar* book;
    
    if (win && win->default_book) {
    	gtk_tree_model_get(model, iter, BOOK_NAME_COLUMN, &book, -1);
    	if (utf8_collate(book, win->default_book) == 0)
        	g_object_set(renderer, "weight", PANGO_WEIGHT_HEAVY, NULL);
    	else
        	g_object_set(renderer, "weight-set", FALSE, NULL);
    	g_object_set(renderer, "text", book, NULL);
    	g_free(book);
	}
}

void config_main_free(ConfigFile* config) {
	if (config) {
		g_free(config->path);
		g_key_file_free(config->key_file);
	}
}

gboolean mainwindow_key_press_event_cb(GtkWidget *widget,
									   GdkEventKey *event,
									   gpointer   user_data) {
	MainWindow* win = (MainWindow *) user_data;
	GtkTreeView* view;
    Contact* contact;
	
	if (!event) return FALSE;

	/* if search has focus use default action */	
	if (gtk_widget_is_focus(win->search_text)) {
		return FALSE;
	}

	switch (event->keyval) {
		case GDK_KEY_s:
			view = GTK_TREE_VIEW(win->contact_list);

			contact = get_selected_contact(view);
			if (contact) {
				win->selected_contact = contact;
				contact_edit_cb(widget, win);
			}
			return TRUE;
		case GDK_KEY_Delete:
			view = GTK_TREE_VIEW(win->contact_list);

			contact = get_selected_contact(view);
			if (contact) {
				win->selected_contact = contact;
				contact_delete_cb(widget, win);
			}
			return TRUE;
		default:
			break;
	}
	
	return FALSE;
}
