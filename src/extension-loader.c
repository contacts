/*
 * $Id$
 */
/* vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:��,trail\:�: */

/*
 * Claws-contacts is a proposed new design for the address book feature
 * in Claws Mail. The goal for this new design was to create a
 * solution more suitable for the term lightweight and to be more
 * maintainable than the present implementation.
 *
 * More lightweight is achieved by design, in that sence that the whole
 * structure is based on a plugable design.
 *
 * Claws Mail is Copyright (C) 1999-2011 by the Claws Mail Team and
 * Claws-contacts is Copyright (C) 2011 by Michael Rasmussen.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifdef HAVE_CONFIG_H
#       include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "mainwindow.h"
#include "extension.h"
#include "extension-loader.h"
#include "utils.h"
#include "gtk-utils.h"

static GSList* active_hooks[EXTENSION_HOOK_N];
static GSList* user_menu_items = NULL;
static GSList* abook_context = NULL;
static GSList* contact_context = NULL;
/* The number corresponts to the default number of GtkMenu - 1 */
static guint menu_count = 4;
/* The number corresponts to the default number of GtkMenu - separator and quit */
static guint file_menu_count = 2;
static GHashTable* extensions = NULL;
static const MainWindow* mainwindow = NULL;
static guint NEXT_ID = 1;
static const gchar* valid_license[] = {
	"gpl3",
	"gpl3+",
	"gpl2+",
	NULL
};

typedef struct {
	guint			id;
	HOOKFUNC		callback;
	ExtensionHook	type;
} Callback;
	
static guint next_id() {
	guint id = NEXT_ID;
	NEXT_ID++;
	
	return id;
}

static Extension* extension_new(const gchar* filename) {
	Extension* extension = g_new0(Extension, 1);
	extension->filename = g_strdup(filename);
	extension->id = next_id();
	
	return extension;
}

static gboolean compatible_license(Extension* extension) {
	gboolean res = TRUE;
	gchar* license;
	gchar** valid = (gchar **) valid_license;
	
	license = g_utf8_strdown(extension->license(), -1);
	while(*valid && res) {
		if (utf8_collate(license, *valid++) == 0)
			res = FALSE;
	}
	
	g_free(license);
	return res;
}

void extension_free(gpointer extension) {
	Extension* ext = (Extension *) extension;
	if (! ext)
		return;
	
	debug_print("%s: Unloading\n", ext->name ? ext->name() : ext->filename);
	g_free(ext->filename);
	if (ext->module) {
		if (ext->done)
			ext->done();
		g_module_close(ext->module);
	}
	g_free(ext);
}

gint register_hook_function(guint id,
						    ExtensionHook hook_type,
						    HOOKFUNC callback,
						    gchar** error) {
	gint result = 0;
	Callback* cb;

	if (! callback) {
		if (error)
			*error = g_strdup(_("Missing reference to callback function"));
		return TRUE;
	}
	
	debug_print("Registrering hook function [%d]\n", id); 
	cb = g_new0(Callback, 1);
	cb->id = id;
	cb->callback = callback;
	cb->type = hook_type;
	active_hooks[hook_type] = g_slist_prepend(active_hooks[hook_type], cb);
	
	return result;
}

void unregister_hook_function(guint id,
						      ExtensionHook hook_type,
						      gchar** error) {
	Callback* cb = NULL;
	GSList* cur;
	gboolean found = FALSE;
	
	cur = active_hooks[hook_type];
	while (cur && ! found) {
		cb = (Callback *) cur->data;
		if (cb && cb->id == id)
			found = TRUE;
		else {
			cb = NULL;
			cur = cur->next;
		}
	}
	
	if (cb) {
		debug_print("Unregistrering hook function [%d]\n", cb->id); 
		active_hooks[hook_type] = g_slist_remove(active_hooks[hook_type], cb);
		g_free(cb);
	}
	else {
		if (error)
			*error = g_strdup(_("%d: Extension not found"));
	}
}

void init_hooks(MainWindow* main) {
	ExtensionHook i;
	
	mainwindow = (const MainWindow *) main;
	for (i = EXTENSION_BEFORE_INIT_HOOK; i < EXTENSION_HOOK_N; i++)
		active_hooks[i] = NULL;
	extensions = g_hash_table_new_full(g_int_hash, g_int_equal, NULL, extension_free);
}

void done_hooks() {
	ExtensionHook i;
	GSList* cur;
	
	for (i = EXTENSION_BEFORE_INIT_HOOK; i < EXTENSION_HOOK_N; i++) {
		cur = active_hooks[i];
		while (cur) {
			g_free(cur->data);
			cur = cur->next;
		}
		g_slist_free(active_hooks[i]);
		active_hooks[i] = NULL;
	}
			
}	

void run_hook_callbacks(ExtensionHook hook, gpointer object) {
	GSList* cur;
	
	if (! (EXTENSION_BEFORE_INIT_HOOK || EXTENSION_AFTER_INIT_HOOK) && ! object)
		return;
	
	for (cur = active_hooks[hook]; cur; cur = g_slist_next(cur)) {
		Callback* cb = (Callback *) cur->data;
		if (cb && cb->callback)
			cb->callback(mainwindow, object);
	}
}

Extension* extension_load(const gchar* filename, gchar** error) {
	Extension* extension;
	gpointer extension_init, extension_done, extension_license,
			 extension_name, extension_describtion;
	
	if (! filename) {
		if (error)
			*error = g_strdup(_("Missing filename"));
		return NULL;
	}
	
	extension = extension_new(filename);

	debug_print("trying to load `%s'\n", filename);
	extension->module = g_module_open(filename, 0);
	if (extension->module == NULL) {
		if (error)
			*error = g_strdup(g_module_error());
		extension_free(extension);
		return NULL;
	}

	if (!g_module_symbol(extension->module, "extension_init", &extension_init) ||
		!g_module_symbol(extension->module, "extension_done", &extension_done) ||
		!g_module_symbol(extension->module, "extension_name", &extension_name) ||
		!g_module_symbol(extension->module, "extension_describtion", &extension_describtion) ||
		!g_module_symbol(extension->module, "extension_license", &extension_license)) {
			if (error)
				*error = g_strdup(g_module_error());
			extension_free(extension);
			return NULL;
	}
	
	extension->init = extension_init;
	extension->done = extension_done;
	extension->license = extension_license;
	extension->name = extension_name;
	extension->describtion = extension_describtion;
	
	if (compatible_license(extension)) {
		if (error)
			*error = g_strconcat(extension->license(), ": not compatible!", NULL);
		extension_free(extension);
		return NULL;
	}
			
	g_hash_table_replace(extensions, &extension->id, extension);
	
	return extension;
}

void extension_unload(Extension** extension, gchar** error) {
	
	cm_return_if_fail(extension != NULL);

	if (! g_hash_table_remove(extensions, &(*extension)->id)) {
		if (error)
			*error = g_strdup(_("Extension was not found"));
	}
	else
		*extension = NULL;
}

static void user_menu_items_free(gpointer item) {
	UserMenuItem* menu;
	
	if (! item)
		return;
		
	menu = (UserMenuItem *) item;
	g_free(menu->parent);
	g_free(menu->sublabel);
}

void extension_loader_done() {
	done_hooks();
	if (extensions) {
		g_hash_table_destroy(extensions);
		extensions = NULL;
	}
	mainwindow = NULL;
	NEXT_ID = 1;

	gslist_free(&user_menu_items, user_menu_items_free);
	gslist_free(&contact_context, user_menu_items_free);
	gslist_free(&abook_context, user_menu_items_free);
}

static GSList* get_extension_path(gchar** error) {
    GDir *ext_dir, *ext_sub_dir;
    GError *err = NULL;
    const gchar *filename, *ext_name;
    gchar *path, *ext, *ext_file = NULL;
    GSList* files = NULL;

	ext_dir = g_dir_open(CLAWS_CONTACTS_EXTENSIONDIR, 0, &err);
	if (err) {
		g_warning("g_dir_open() failed: %s\n", err->message);
		if (error)
			*error = g_strdup(err->message);
		g_clear_error(&err);
		return NULL;
	}
	debug_print("Extension dir: %s\n", CLAWS_CONTACTS_EXTENSIONDIR);
	if (EXTENSIONSUBDIR) {
		while ((filename = g_dir_read_name(ext_dir))) {
			g_free(ext_file);
			ext_file = NULL;
			path = g_build_filename(CLAWS_CONTACTS_EXTENSIONDIR, filename, NULL);
			debug_print("Extension dir: %s\n", path);
			if (g_file_test(path, G_FILE_TEST_IS_DIR)) {
				ext = g_build_filename(path, "src", ".libs", NULL);
				ext_sub_dir = g_dir_open(ext, 0, &err);
				if (err) {
					g_free(ext);
					g_clear_error(&err);
					ext = g_build_filename(path, ".libs", NULL);
					ext_sub_dir = g_dir_open(ext, 0, &err);
					if (err) {
						g_clear_error(&err);
						continue;
					}
				}
				while ((ext_name = g_dir_read_name(ext_sub_dir)) && ! ext_file) {
					debug_print("Extension dir: %s\n", ext_name);
					if (g_str_has_suffix(ext_name, G_MODULE_SUFFIX)) {
						ext_file = g_build_filename(ext, ext_name, NULL);
					}
				}
				g_dir_close(ext_sub_dir);
				g_free(ext);
				if (ext_file)
					files = g_slist_append(files, g_strdup(ext_file));
				g_free(ext_file);
				ext_file = NULL;
			}
			g_free(path);
		}
	}
	else {
		while ((filename = g_dir_read_name(ext_dir))) {
			if (g_str_has_suffix(filename, G_MODULE_SUFFIX)) {
				path = g_build_filename(CLAWS_CONTACTS_EXTENSIONDIR, filename, NULL);
				debug_print("Extension dir: %s\n", path);
				files = g_slist_append(files, g_strdup(path));
				g_free(path);
			}
		}
	}

	g_dir_close(ext_dir);
	
	return files;
}

void load_extensions(gchar** error) {
    Extension* extension;
    GSList *exts, *cur;

	exts = get_extension_path(error);
	for (cur = exts; cur; cur = g_slist_next(cur)) {
		gchar* ext = (gchar *) cur->data;
		extension = extension_load(ext, error);
		if (extension) {
			extension->init(extension->id);
			debug_print("%s: Loaded\n", extension->name());
		}
		else
			debug_print("%s: %s\n", ext, *error ? *error : "Unknown error");
	}
	gslist_free(&exts, g_free);
}

void unload_extensions() {
	GHashTableIter iter;
	gpointer key, value;
	gchar* error = NULL;
	GSList *exts = NULL, *cur;
	
	if (extensions) {
		g_hash_table_iter_init(&iter, extensions);
		while (g_hash_table_iter_next(&iter, &key, &value)) {
			exts = g_slist_prepend(exts, (Extension *) value);
		}
		cur = exts;
		while (cur) {
			Extension* extension = (Extension *) cur->data;
			extension_unload(&extension, &error);
			if (error) {
				debug_print("%s\n", error);
			}
			g_free(error);
			error = NULL;
			cur = cur->next;
		}
		gslist_free(&exts, NULL);
	}
	extension_loader_done();
}

GSList* get_menu_items(ContactsMenu menu) {
	GSList *widgets = NULL, *cur;
	UserMenuItem* item;
	
	switch (menu) {
		case CONTACTS_ADDRESSBOOK_MENU:
			cur = abook_context;
			while (cur) {
				item = (UserMenuItem *) cur->data;
				widgets = g_slist_prepend(widgets, item->widget);
				cur = cur->next;
			}
			break;
		case CONTACTS_CONTACT_MENU:
			cur = contact_context;
			while (cur) {
				item = (UserMenuItem *) cur->data;
				widgets = g_slist_prepend(widgets, item->widget);
				cur = cur->next;
			}
			break;
		default:
			break;
	}
	
	return widgets;
}

static GtkWidget* create_menu_item(GtkWidget** widget, const gchar* label) {
	GtkWidget* menu = NULL;

	if (! *widget) {
		menu = gtk_menu_new();
		gtk_widget_show(menu);
		gtk_menu_set_accel_group(GTK_MENU(menu), mainwindow->accel);
		*widget = gtk_image_menu_item_new_with_mnemonic(label);
		gtk_widget_show(*widget);
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(*widget), menu);
	}
		
	return menu;
}

static GtkWidget* insert_sub_menu(GtkImageMenuItem* image_menu,
								  MenuItem* menu_item,
								  UserMenuItem* user_menu) {
	GtkWidget *widget = NULL, *menu = NULL;
	GSList* cur;
	gboolean is_created = FALSE;

	if (menu_item->submenu) {
		for (cur = user_menu_items; cur && ! menu && ! is_created; cur = g_slist_next(cur)) {
			UserMenuItem* item = (UserMenuItem *) cur->data;
			if (utf8_collate((gchar *) item->parent, (gchar *) menu_item->parent) == 0) {
				if (utf8_collate((gchar *) item->sublabel, (gchar *) menu_item->sublabel) == 0 ||
					utf8_collate((gchar *) item->parent, (gchar *) item->sublabel) == 0) {
					menu = item->menu;
					is_created = TRUE;
					/*debug_print("parent: %s sublabel: %s\n", item->parent, item->sublabel);
					if (utf8_collate((gchar *) item->parent, (gchar *) item->sublabel) == 0) {
						force = TRUE;
						debug_print("force\n");
					}*/
				}
			}
		}
		if (! menu) {
/*			if (force) {
				tmp = create_menu_item(&widget, menu_item->sublabel);
				gtk_menu_shell_append(GTK_MENU_SHELL(tmp), GTK_WIDGET(image_menu));
			}
			else*/
				menu = create_menu_item(&widget, menu_item->sublabel);
			user_menu->menu = /*(force)? tmp :*/ menu;
			user_menu->sublabel = g_strdup(menu_item->sublabel);
		}
		if (menu) {
			if (is_created) {
				gtk_menu_shell_append(GTK_MENU_SHELL(menu), GTK_WIDGET(image_menu));
				widget = NULL;
			}
			else {
				gtk_menu_shell_append(GTK_MENU_SHELL(menu), GTK_WIDGET(image_menu));
				//widget = GTK_WIDGET(image_menu);
			}
		}
	}
	else
		widget = GTK_WIDGET(image_menu);
		
	return widget;
}

static gboolean insert_context_sub_menu(ContactsMenu menu,
										  UserMenuItem* menu_item) {
	GtkWidget* widget = NULL;
	GSList* cur;
	UserMenuItem* item;
	
	if (menu_item->sublabel) {
		switch (menu) {
			case CONTACTS_ADDRESSBOOK_MENU:
				cur = abook_context;
				break;
			case CONTACTS_CONTACT_MENU:
				cur = contact_context;
				break;
			default:
				cm_return_val_if_fail(TRUE, FALSE);
		}
		while (cur) {
			item = (UserMenuItem *) cur->data;
			if (item->sublabel) {
				if (strcmp(item->sublabel, menu_item->sublabel) == 0) {
					gtk_menu_shell_append(GTK_MENU_SHELL(item->menu),
						menu_item->widget);
					return FALSE;
				}
			}
			cur = cur->next;
		}
		menu_item->menu = create_menu_item(&widget, menu_item->sublabel);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_item->menu), menu_item->widget);
		menu_item->widget = widget;
	}

	return TRUE;
}

MenuItem* menu_item_new(void) {
	MenuItem* item;
	
	item = g_new0(MenuItem, 1);
	
	return item;
}

gboolean add_menu_item(GtkImageMenuItem* image_menu, MenuItem* menu_item) {
	gboolean added = TRUE;
	GtkWidget* menu_elem;
	UserMenuItem* menu;
	
	if (! mainwindow)
		return added;

	if (! menu_item || menu_item == CONTACTS_NONE ||
		(! menu_item->parent && menu_item->menu == CONTACTS_MAIN_MENU)) {
		g_critical("Missing menu_item or menu_item invalid");
		return added;
	}
	
	if (! GTK_IS_IMAGE_MENU_ITEM(image_menu)) {
		g_critical("menu is not GtkImageMenuItem");
		return added;
	}

	if (menu_item->submenu && ! menu_item->sublabel) {
		g_critical("Submenu is missing label for widget");
		return added;
	}

	debug_print("Inserting '%s' in '%s' menu_item\n",
		gtk_menu_item_get_label(GTK_MENU_ITEM(image_menu)), menu_item->parent);
	
	menu = g_new0(UserMenuItem, 1);
	gtk_widget_show(GTK_WIDGET(image_menu));
	if (menu_item->parent && strcasecmp("file", menu_item->parent) == 0) {
		// add to 'File'
		menu_elem = insert_sub_menu(image_menu, menu_item, menu);
		if (menu->sublabel && menu_item->parent)
			menu->parent = g_strdup(menu_item->parent);
		else {
			g_free(menu);
			menu = NULL;
		}
		if (menu_elem) {
			gtk_menu_shell_insert(GTK_MENU_SHELL(mainwindow->file_menu),
						  menu_elem, file_menu_count);
			file_menu_count++;
		}
	}	
	else if (menu_item->parent && strcasecmp("tools", menu_item->parent) == 0) {
		// add to 'Tools' 
		menu_elem = insert_sub_menu(image_menu, menu_item, menu);
		if (menu->sublabel && menu_item->parent)
			menu->parent = g_strdup(menu_item->parent);
		else {
			g_free(menu);
			menu = NULL;
		}
		if (menu_elem)
			gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->tools_menu), menu_elem);
	}
	else if (menu_item->parent && strcasecmp("help", menu_item->parent) == 0) {
		// add to 'Help' 
		menu_elem = insert_sub_menu(image_menu, menu_item, menu);
		if (menu->sublabel && menu_item->parent)
			menu->parent = g_strdup(menu_item->parent);
		else {
			g_free(menu);
			menu = NULL;
		}
		if (menu_elem)
			gtk_menu_shell_append(GTK_MENU_SHELL(mainwindow->help_menu), menu_elem);
	}
	else if (menu_item->menu == CONTACTS_MAIN_MENU) {
		if (! menu_item->sublabel) {
			menu_item->sublabel = g_strdup(menu_item->parent);
			menu_item->submenu = TRUE;
		}
		menu_elem = insert_sub_menu(image_menu, menu_item, menu);
		if (menu->sublabel && menu_item->parent)
			menu->parent = g_strdup(menu_item->parent);
		else {
			g_free(menu);
			menu = NULL;
		}
		if (menu_elem) {
			gtk_menu_shell_insert(GTK_MENU_SHELL(mainwindow->menu), menu_elem, menu_count);
			menu_count++;
		}
	}
	else {
		// context menu
		if (menu_item->menu == CONTACTS_CONTACT_MENU) {
			menu->widget = GTK_WIDGET(image_menu);
			menu->sublabel = g_strdup(menu_item->sublabel);
			if (insert_context_sub_menu(menu_item->menu, menu))
				contact_context = g_slist_append(contact_context, menu);
			else
				user_menu_items_free(menu);
		}
		else if (menu_item->menu == CONTACTS_ADDRESSBOOK_MENU) {
			menu->widget = GTK_WIDGET(image_menu);
			menu->sublabel = g_strdup(menu_item->sublabel);
			if (insert_context_sub_menu(menu_item->menu, menu))
				abook_context = g_slist_append(abook_context, menu);
			else
				user_menu_items_free(menu);
		}
		else {
			// error!!!!
		}
		menu = NULL;
	}

	if (menu)
		user_menu_items = g_slist_prepend(user_menu_items, menu);

	return added;
}
